\section{Non-functional components and interceptors}

In this section we discuss how the membrane of a composite component including components can be analysed and translated into implementation code. 
We do not explain here the graphical specification of a non-functional part as it was presented in details in Section~\ref{sec:vce-graphical-formalism}.
In Chapter~\ref{chap:formalisation} we also formalised the well-formedness conditions for the componentised membrane of primitives. In the current version of our framework we verify only the membrane of composite components, the aspect dealing with the componentised membrane of the primitives is in the scope of the future work.
 It should be mentioned that a membrane can include sub-components of two kinds: component-controllers and interceptors. We currently do not construct the pNets for the latter. 


\subsection{From application design to pNets}

\subsubsection{Illustrative example}

Figure~\ref{fig:nf-bindings} illustrates an example of a composite component \texttt{Composite} with a componentised membrane. Its content includes one sub-component \texttt{Prim1}, and its membrane includes two component-controllers (\texttt{Contr1, Contr2}) and one interceptor \texttt{Monitor}. The composite serves calls to two non-functional methods: \texttt{m1} and \texttt{m2}, and to one functional method \texttt{m5}. We include the functional method in the example in order compare the treatment of the functional and non-functional requests. The \texttt{m1} and \texttt{m5} method calls are processed by \texttt{Prim1} (in the content) while the invocations of \texttt{m2} are served by \texttt{Contr1} (in the membrane). The component-controllers communicate with each other: \texttt{Contr1} can invoke method \texttt{m4} on \texttt{Contr2}. Finally, the composite includes a non-functional internal interface \texttt{C2-controller} which is not visible from outside of the composite. It is used by \texttt{Contr1} which can invoke the method \texttt{m3} on it, and the invocation will be forwarded to \texttt{Prim1}.
%\TODO{say that svs are similar, and that they rely on table 5 etc}

\begin{figure}
     \centering
     \includegraphics[width=14cm]{drawings/nf-bindings.pdf}
     \caption{Bindings in a membrane}
     \label{fig:nf-bindings}
 \end{figure}

In order to include the specification of the non-functional elements in the model of a composite component behaviour, we extend the set of sub-nets encoding the behaviour of a composite with several other pNets and synchronisation vectors. In fact, as we will demonstrate in this section, the pNets modelling the non-functional elements of a composite are very similar to the ones for the functional part. In particular, we generate proxy, proxy manager, and delegate pLTSs processing the non-functional requests, and the pNets for the components in the membrane.

Figure~\ref{fig:nf-pnet} illustrates the pNet of the composite depicted in Figure~\ref{fig:nf-bindings}; for the sake of simplicity, we omit the parameters of the actions in the figure. The pNet includes three sub-pNets for the sub-components: \symb{Contr1}, \symb{Contr2},  and \symb{Prim1}. The \texttt{Monitor} component is not included in the structure because we have not formalised the pNets for the interceptors in the current version of the framework. Instead, we assume that the functional call goes directly to the plugged component. This assumption is coherent with the role of interceptors. 
One can notice that from the figure of the pNet it is not possible to understand which sub-net models a functional or a non-functional sub-component. Indeed, at the level of pNets we do not separate elements of the two concerns as their separation has already been carefully checked during the static validation. In addition, the pNet of a composite includes the pLTSs that process the non-functional requests (\symb{CPM\_m1}, \symb{CProxy\_m1}, \symb{Deleg\_m1}, etc) and the synchronisation vectors for the treatment of requests. The treatment of the non-functional method calls is very similar to the treatment of the functional ones. Hence, the synchronisation vectors modelling the communications among the non-functional elements are based on the rules given in Tables~\ref{tab:SVComp}~and~\ref{tab:SVBind}.

\begin{figure}
     \centering
     \includegraphics[width=\linewidth]{drawings/nf-pnet.pdf}
     \caption{pNet of a component with a componentised membrane}
     \label{fig:nf-pnet}
 \end{figure}

The queue of the composite can receive external requests to the methods exposed on its server interfaces (\symb{iQ\_m1}, \symb{iQ\_m2}, and \symb{iQ\_m5}) and the invocations to \texttt{m3} (\symb{iQ\_m3}) can be received from \symb{Contr1}. Then, those requests are treated by the proxy, proxy manager, and delegate pLTSs similar to the functional calls discussed in Section~\ref{sec:vce-pnets-gcm}. The results of the calls to the external methods are returned to outside of the composite: \symb{R\_m1}, \symb{R\_m2}, \symb{R\_m5}, and the result of the invocation of \texttt{m3}  is returned to the caller, i.e. to \texttt{Contr1}. The communication between the two component-controllers in the membrane is modelled in exactly the same way as if it occured between two functional sub-components.


Below we provide a detailed description of the sub-nets and synchronisation vectors generated for the non-functional part of a composite. 

%
%At the level of pNets, we do not distinguish  between the functional and non-functional aspects, i.e. the pLTSs %processing the methods of the non-functional interfaces are included in the same family as the pLTSs for the %functional methods.

\subsubsection{Sub-nets} First, we generate the proxy, proxy manager, and delegate pLTSs for each method of the   external non-functional client and server interfaces of a composite in the same way as for the functional methods.

 The construction of functional and non-functional internal interfaces is different. For each internal functional interface of a composite, there exists a symmetric external one, hence, while building the pLTSs of the functional methods, we discussed only external interfaces. However, an internal non-functional interface may not have the corresponding external one, when it is connected to a component inside a membrane. Such interfaces enable the communications between sub-components in the membrane and in the content.
 Figure~\ref{fig:nf-bindings} illustrates an example of such an interface: the \texttt{C2-controller} internal interface receives method calls from the \texttt{Contr1} component in the membrane and forwards them to \texttt{Prim1} in the content. The example illustrates a client interface, but an internal non-functional server interface could be also modelled in a similar way. In this case, a method invocation would be triggered by a component in the content and served by a component in the membrane.
  According to the semantics of GCM/ProActive components, the invocations to such kind of interfaces also go through the queue and the body of the composite, hence, the proxy, proxy manager, and delegate pLTSs should be generated for the methods of such interfaces (\symb{CProxy\_m2}, \symb{CPM\_m2}, and \symb{Deleg\_m2} in the example).  

Second, we include the pNets encoding the behaviour of the non-functional sub-components in the pNet of a composite (\symb{Contr1} and \symb{Contr2} in the example). In fact, the pNets encoding the behaviour of the components inside the membrane have exactly the same generation procedure as the ones of the functional components discussed in Section~\ref{sec:vce-pnets-gcm}. 

\subsubsection{Synchronisation vectors} 
As it was discussed in Section~\ref{sec:semant-comp-comp} 
the synchronisation vectors of a composite are organised into three sets:
server-side ($\symb{SV}_S$), client-side ($\symb{SV}_C$), 
and binding-related ($\symb{SV}_B$) synchronisation vectors. We start by explaining how the first two sets should be extended with the synchronisation vectors for the non-functional interfaces. Then, we discuss the binding-related communications. 

Requests to the non-functional server interfaces of a composite are treated in exactly the same way as the functional calls, hence the proxy, proxy manager, and delegate pLTSs generated for the methods of the non-functional interfaces should be synchronised following the rules from Table~\ref{tab:SVComp}. A non-functional server  request is, first dropped in the queue (Rule~\RrefOne{C1}), then taken by the body (Rule~\Rref{C2}{C2Serve}) and forwarded to the delegate method (Rule~\Rref{C2}{C2Call}), the new proxy is allocated by the proxy manager (Rule~\RrefOne{C4}), and then the proxy can wait for the reply. A non-functional client request is treated in a similar manner except that it cannot be en-queued by an external component (i.e. Rule~\RrefOne{C1} is not applied), and
it is forwarded to outside of the composite (Rule~\RrefOne{C3}). 

The only exception is the internal non-functional interfaces which are connected to the components in the membrane but not to the external interfaces (e.g. \texttt{C2-controller} in Figure~\ref{fig:nf-bindings}). Their methods are not exposed to outside of the composite, and hence the synchronisation vectors \RrefOne{C1}, \RrefOne{C3} which express the communications with the external environment are not generated for them. Still, all the interactions between the queue, the body, proxies, proxy managers, and delegate methods are involved in the treatment of the requests, and, hence the synchronisation vectors based on Rules~\RrefOne{C2}, \RrefOne{C4} are constructed.

The communications which occur on the bindings in the membrane should be also encoded with synchronisation vectors which extend the ($\symb{SV}_B$) set. We distinguish four cases of possible types of interactions, and each of them is illustrated in Figure~\ref{fig:nf-bindings} (see \texttt{b1}, \texttt{b2}, combination of \texttt{b3a} and \texttt{b3b}, and \texttt{b4}). 

First, a binding can connect directly an external and an internal non-functional interfaces of a composite (see \texttt{b1}). In this case, the treatment of request is exactly the same as for the functional calls, because the requests are served (or called) by the sub-components in the content. 
Such communications rely on several rules from Table~\ref{tab:SVBind}. A server method call should be forwarded by the delegate structure to the plugged sub-component inside the content~\Rref{C5}{C5Q}, and the invocation result should be returned to the caller~\Rref{C5}{C5R}. A client method invocation should be forwarded from the caller located in the content to the queue~\Rref{C6}{C6Q}, and its result should be returned to the caller~\Rref{C6}{C6R}.

Second, a component in the membrane can be connected to a non-functional external interface of the composite (e.g. \texttt{b2}). In fact, this is very similar to the previous case, and it involves the same synchronisation vectors (i.e. the vectors expressed by the Rules~\RrefOne{C6},~\RrefOne{C7)} but the synchronised sub-components are located in the membrane, not in the content.

Third, component-controllers can use non-functional internal interfaces of the composite in order to communicate with the sub-components inside the content (\texttt{b3a}).  Invocations going through such interfaces are also processed by the queue and the body of the composite. Hence, all components plugged to an internal non-functional interface should synchronise with the pLTSs of the composite but not with each other.
In the given example, \texttt{Contr1} should be synchronised with the queue of the composite on a method invocation and with the corresponding proxy on the result reception (Rule~\RrefOne{C6}). \texttt{Prim1} should be synchronised with the corresponding delegate pLTS on a method invocation, and when it sends the computed result, it synchronises with the proxy pLTS (\RrefOne{C5}). 

Finally, sub-components inside a membrane can communicate with each other (e.g. \texttt{b4}). They are synchronised on a method call and on a reply following the Rules \RrefOne{C7}, i.e. in the same way as the communicating sub-components in a content. 

\subsection{Implementing pNet generation and integration with CADP}

As it was discussed in the previous section, all the constructs encoding the non-functional part of a component are based on the ones for the functional elements. Hence, in order to generate a pNet of a composite with a componentised membrane from VerCors, we did not have to implement any additional builder. One additional analysis step that we have to do while pre-processing the input models is extracting a set of internal non-functional interfaces of composite components which are connected to the sub-components in a membrane. Then, in addition to the pNet generation process discussed in Section~\ref{subsec:pnets-generation}, we invoke the proxy, proxy manager, and delegate builders for the methods of the non-functional interfaces, and the pNet builders for the sub-components in a membrane. The synchronisation vector generator constructs additionally the synchronisation vectors encoding the communications related to the non-functional aspect as it was discussed in the previous section. While producing .fiacre and .exp files, VerCors does not distinguish the constructs encoding functional and non-functional features. The internal communications in the sub-components of a membrane are by default hidden in the behaviour graph of the enclosing component.

%While generating the behavioural models, we do not distinguish component-controllers and interceptors. We have to admit, that we did not make a deep study on the model-checking of interceptors and it is possible that in the future we will have to modify the existing generator.
%\subsection{From pNets to CADP}
%check if there is anything to say

\subsection{Code generation} 

VerCors includes the specification of a component membrane in the generated ADL file and Java code.
This requires an additional pre-processing step where the input architecture is analysed in order to extract interceptors and to map functional interfaces of a composite to a chain of interceptors.

Listing~\ref{list:adl-nf} illustrates a simplified version of an ADL file generated for \texttt{Composite} from Figure~\ref{fig:nf-bindings}. The specification of its membrane is given in lines~7-22. It includes the non-functional internal and external interfaces (lines~8-11), both component-controllers  (lines~13-14) and an interceptor (line~15), bindings in the membrane (lines~16-19), and bindings connecting the non-functional internal interfaces and the components in the content (lines 20-21). One can notice that all the bindings in the membrane are included in the ADL specification except from the ones connecting an interceptor to the interfaces which calls it intercepts. Instead, the interceptor is referenced by the corresponding interface in line~2.

\begin{minipage}{\linewidth}
 \begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={Generated ADL file of a composite with a componentised membrane}, label = {list:adl-nf}, basicstyle=\scriptsize]
<definition name="Composite">
 <interface name="S3" role="server" signature="interfaces.FItf" interceptors="Monitor.S1"/>
 <component name="Prim1">
 	...
 </component>  
 <binding client="this.S3" server="Prim1.S3"/>
 <controller desc="composite">
 	<interface name="S1-controller" role="server" signature="p.interfaces.NFItf"/>
 	<interface name="S2-controller" role="server" signature="p.interfaces.NFItf"/>
 	<interface name="C1-controller" role="internal-client" signature="p.interfaces.NFItf"/>
 	<interface name="C2-controller" role="internal-client" signature="p.interfaces.NFItf"/> 
 
 	<component name="Contr1"> ... </component>
 	<component name="Contr2"> ... </component>
	<component name="Monitor"> ... </component>
	<binding client="this.S1-controller" server="this.C1-controller"/>
 	<binding client="Contr1.C2" server="this.C2-controller"/>
 	<binding client="Contr1.C1" server="Contr2.S1"/>
 	<binding client="this.S2-controller" server="Contr1.S1"/>
 	<binding client="this.C1-controller" server="Prim1.S1-controller"/>
 	<binding client="this.C2-controller" server="Prim1.S2-controller"/>
  </controller>
</definition>
\end{lstlisting} 
\end{minipage}

The Java classes of component-controllers are constructed in the same way as the classes implementing functional components. For the interceptors we generate two additional methods which are invoked when a request is intercepted and after it has been served. 
%\texttt{beforeMethodInvocation} which is triggered before  \texttt{afterMethodInvocation} which is in 
