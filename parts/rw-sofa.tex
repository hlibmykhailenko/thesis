\section{The SOFA~2 project}
\label{rw-sofa}

Among the existing component models and development tools, the approach closest to VerCors/GCM is presented in the SOFA~2 \cite{Malohlava2013101} project. It comprises a component model supported by a development framework and a runtime environment. We start this section by describing the SOFA~2 component model and comparing it to GCM. Then, we present the dedicated tools. Finally, we elaborate on the positioning of our work with respect to the SOFA~2 project. 

%SOFA component model
\paragraph{The SOFA~2 component model}
The SOFA~2 component model has the same advanced features as the GCM including hierarchical components, the support of dynamic reconfiguration, separation between functional and non-functional concerns.
A SOFA-based application represents an assembly of components with their interfaces and a set of connectors used for communication between components. 
 
A SOFA component is modeled by its \textit{frame} and \textit{architecture}. A frame is a black-box view  and is associated with a set of provided and required interfaces. An interface is characterised by a signature and a number of properties. The first property, \textit{isCollection} is similar to the cardinality of GCM interfaces and defines how many bindings can be attached to the interface. The second property, \textit{connectionType} defines the reconfiguration pattern \cite{Hnetynka:2006:DRA:2171366.2171395} applicable to the interface. 
An architecture is a grey-box view on a SOFA component. An architecture can implement several frames which is similar to a GCM component implementing several interfaces. An architecture can be associated with a set of subcomponents and connections between them. If the set is empty, the architecture refers to a primitive component; otherwise, to a composite one. 

The interfaces are connected by \textit{connectors} which are similar to the GCM bindings at the design stage.
During program execution the connectors may be implemented by various interaction types depending on the underlying infrastructure \cite{Bures04communicationstyle}. The interaction types include a classic synchronous client-server call, asynchronous message passing and uni- or bidirectional streaming of data. A connector in SOFA has much more features than a GCM binding. For example, it can have a monitoring interceptor, while in GCM interceptors are modelled as components inside a membrane. A connector can slightly change a request in order to solve minor incompatibilities between components which is not possible in GCM. In theory, a GCM interceptor can impact request arguments, but such behaviour cannot be modelled and analysed in the current version of VerCors.

The control part of a SOFA component consists of so-called \textit{microcomponents}. As opposed to the GCM controllers, the controllers in SOFA are flat, they do not feature connectors, control part or distribution. A SOFA controller could be seen as a simple class implementing an interface. The way to assemble microcomponents is by constructing so-called \textit{delegation chains} which connect several controllers. A microcomponent can also be an interceptor which resembles the way interceptors are implemented in GCM.

On the dynamic reconfiguration side, SOFA~2 supports multiple patterns allowing one to add and remove components and connectors at run-time~\cite{Sofa2MM}. %An interesting feature is the utility interface 

%
%Considering all the similarities between GCM and SOFA, we believe, that modeling SOFA components should be possible in Vercors. Generating behavioral models for SOFA should be also possible, because encoding  asynchronous message passing and synchronous communications with pNets should be easier than encoding futures mechanism which is implemented in the current version.  

%\TODO{more about connectors, reconfiguration?}
%tools for SOFA
\paragraph{The tools}
The SOFA~2 component model is supported by a modelling platform SOFA~IDE, a set of tools for verification and an execution framework.

SOFA~IDE is a model-driven environment for graphical design of the SOFA~2 components. The tool supports UML-based modelling of the components architecture. The executable code for connectors can be automatically generated as described in \cite{Bure2006GeneratingCF}. The behaviour of the component interfaces can be textually expressed with Behavior Protocols \cite{Plasil:2002:BPS:630831.631297}. Then, the behaviour should be manually associated to a component in an XML-based file. 

The correctness check for the SOFA applications can be done only at the level of the Behavior Protocols. A Protocol Checker presented in \cite{mach05behavior} takes a set of protocols as an input, creates a parse tree for each protocol, combines the trees and creates a state space reflecting the parallel composition of the protocols. Based on the obtained model, the Protocol Checker is able to verify the compliance between two protocols and translate the Behavior protocols into an input for the CADP Caesar tool.

In \cite{DBLP:conf/sew/ParizekPK06} the authors present an approach for checking whether the Java implementation of a component behaviour obeys the specified protocol. For this, the authors combine a slightly modified versions of  Java PathFinder (JPF)~\cite{JPF} and the Protocol Checker.
JPF is a model-checker of Java byte code implemented as an extension of the Java Virtual Machine (JVM). Unlike the usual JVM, JPF performs all possible executions of a program and builds its state space as a tree-like structure where the branches reflect the interleaving of thread instructions. As in the classical model-checking, the tool traverses the constructed state space to check built-in properties like assertion violation, and deadlocks. Except from that, JPF includes several extensions allowing more complex analysis. Among them, we are interested in the mechanism of \textit{Listeners} which allows the programmer to define an observer that will register particular type of events that occurred in the byte code e.g. thread start, object creation, byte code instruction execution. The result of the monitoring can be used  to check custom properties. In \cite{DBLP:conf/sew/ParizekPK06} the authors configure JPF listeners so that they record all invoke and return instructions for the interface methods of SOFA component byte code and then notify the Protocol Checker. The Protocol Checker, in its turn, checks whether the instruction is possible with respect to the state space of the specified behaviour protocols. If the execution is not possible, the protocol violation is reported.

The behaviour description can be also traslated into Promela and checked by the Spin \cite{Holzmann:1997:MCS:260897.260902} model-checker as discussed in \cite{Kofron:2007:CSC:1244002.1244326}. Three types of errors can be detected by the described approach: bad activity (a component emits an event but there is no other component to accept the event), no activity (a deadlock) and divergence (a component behaviour contains a cycle from which there is no way to reach an accepting state). 

SOFAnode is a distributed execution environment for the SOFA~2 components implemented as Java classes. The environment consists of a set of component containers called \textit{deployment docks} which can be located on different machines. A deployment dock can execute a component which was assigned to the dock during the deployment. 
%Additionally, SOFAnode has a repository for the components' description and implementation. 
SOFAnode can be managed from an Eclipse-based tool MConsole where the user can start and stop  docks as well as visualize system execution.

\paragraph{Positioning}
The SOFA~2 framework for modelling and verification of distributed component-based systems is very close to VerCors/GCM. However, there is a number of advantages of the work presented in this thesis. First, in order to master SOFA~2, the user will have to additionally learn the Behavioral Protocols and the XML specification linking a component and its behaviour. In VerCors, both components architecture and behaviour are modelled in integrated graphical editors; the user does not need to know any formalism for behaviour specification except from the UML state machines that are well-known among programmers. Moreover, the behaviour is associated to the components within the graphical designer and the user does not need to be aware of the structure of the generated ADL description.
%Additionally, all the correctness check of SOFA-based application is done at the level of the Behavior Protocols.

Second, GCM and VerCors provide more expressiveness for modelling and verification of the non-functional part of an application than SOFA which, to the best of our knowledge, allows neither structured controllers nor specifying relations between controllers. 

Finally, we believe that the SOFA framework could benefit from validation of the architecture static constraints formalised in Chapter \ref{chap:formalisation}. To the best of our knowledge, the current version of SOFA only supports verification at the level of the Behavior Protocols. Implementation of the architecture static validation could be useful for the programmers who would like to make sure that the components assembly is statically correct before dealing with the Behavioral Protocols.

At the same time, we should note that SOFA~2 project comprises a number of interesting features which are not included in VerCors. First, it would be useful to allow the user to model and verify various communication styles in VerCors.
% Second, we could investigate whether VerCors, could benefit from the translation of the UML state machines into Behavior Protocols in order to use Spin for model-checking. 
Another interesting task would be checking whether the generated components implementation obeys the modelled behaviour by adapting the approach presented in \cite{DBLP:conf/sew/ParizekPK06}.

\label{sec:rw-sofa}