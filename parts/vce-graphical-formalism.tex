\section{Diagrams for architecture and behaviour specification}
\label{sec:vce-graphical-formalism}

In this section we present the diagrams for the design of a component system architecture and behaviour in VerCors. The section is illustrated by a small size use-case example - a GCM-based application implementing Peterson's leader election algorithm \cite{Peterson}. We start by introducing the algorithm. Then, we present the four core types of diagrams used in VerCors: component diagrams, UML class diagrams, UML state machine diagrams and type diagrams. We show how the four graphical languages are integrated together in order to provide an expressive formalism for component-based application specification. 

\subsection{An illustrative example}
\label{subsec:peterson}

Distributed processes often need to select a unique leader; Peterson's
election algorithm can be used for this purpose. The participants are organised in a
unidirectional ring of asynchronous processes. 
Every process participating in the elections has a FIFO queue and the
order of sent messages is preserved by the communication channels.
Each process can be either in active mode if the process participates in the election, or
in passive mode if it only forwards
messages. Initially, every  process stores a unique number that
will be modified during the election. The processes exchange two
rounds of messages
so that every active process learns the numbers stored by the two nearest
active processes preceding it. 
If the maximum of the two values of the nearest active processes 
and the value held by the current process is the value received from
the nearest predecessor of the process, then the active process
takes this value as its own value; otherwise the process becomes
passive. The rounds of messages and local decision steps are repeated until a process receives
its own number, this process is the leader.

In  details, every process $P$
stores variables $max(P)$ and $\symb{left}(P)$. $\symb{Max}(P)$ is the number stored by
$P$. $\symb{Left}(P)$ is the number of the active process on
the left of $P$. Processes exchange messages of the form $M(\symb{step},
\symb{value})$ where $\symb{step}$ is the phase of the algorithm. At the \emph{preliminary phase}, each process $P_i$ sends
 $M(1, \symb{max}(P_i))$ to its neighbor. Then, if an active process
$P_i$ receives a message $M(1, x)$ and $x$ is equal to its own number,
the process is the leader, otherwise it assigns $x$ to
$\symb{left}(P_i)$ and sends $M(2, x)$ to its neighbor. When an active
process $P_i$ receives $M(2, x)$ it compares $\symb{left}(P_i)$ to $x$ and
$\symb{max}(P_i)$. If $\symb{left}(P_i)$ is greater than both values, $P_i$ assigns
$\symb{left}(P_i)$ to $\symb{max}(P_i)$ and sends $M(1, \symb{max}(P_i))$; otherwise $P_i$ becomes
passive.

In~\cite{Peterson-proof} the authors prove that "if the algorithm ever terminates, it does so correctly" in the sense that one and only one leader is elected. 

\subsection{Architecture specification}

\paragraph{Component diagrams} The component diagrams are used to define the architecture of a component-based application, i.e. to design the composite and primitive components with their interfaces and relations between them. Figure~\ref{fig:vce-component-diagram} illustrates an example of a simple component diagram. A primitive is depicted as a grey box (\texttt{Prim1} in the figure) while a composite (\texttt{Composite} in the figure) is illustrated as a rectangle divided into two parts: the grey part shows its membrane while the white part corresponds to the content. A GCM interface can be either attached to the border of its host component (e.g. \texttt{C-ext}), or to the border of a content in the case of internal interfaces (e.g. \texttt{S-int}). An interface has a set of characteristics that impact its graphical representation. The icon of an interface changes depending on whether it is a server (e.g. \texttt{S1}) or a client interface (e.g. \texttt{C1}), a singleton or a collective interface, a functional or a non-functional interface. The bindings are shown as black arrows that go from the requesting interfaces to the serving ones.


\textit{Graphical distinction between functional and non-functional aspects}
The specification of a GCM application functional part can be separated from the non-functional one thanks to the separation of a composite into membrane and content and the distinction between functional and non-functional interfaces. The non-functional sub-components (e.g. \texttt{Controller}) are located in the membrane of a composite while the functional ones (e.g. \texttt{Prim1} and \texttt{Prim2}) are placed in the content. The non-functional interfaces (such as \texttt{S1-contr}) have green color whether the functional ones are blue. Note that the non-functional component \texttt{Controller} has two functional interfaces \texttt{S} and \texttt{C} that fulfil a non-functional role at the level of the composite.    

\begin{figure}[t]
     \centering
     \includegraphics[width=12cm]{drawings/vce-component-diagram.pdf}
     \caption{VerCors component diagram}
     \label{fig:vce-component-diagram}
 \end{figure}

\paragraph{UML class diagrams} Another kind of diagrams used for the conceptual model specification in VerCors is UML class diagrams. The platform relies on the following elements: interfaces, classes and generalisation relations, methods, and attributes. 

The \textit{UML interfaces} are used to define the list of methods that can be called and served by client and server GCM interfaces correspondingly. The UML interfaces attached to GCM ones appear on the corresponding component diagrams.

Every primitive should have an attached \textit{UML class} that appears on the component diagram and defines the list of methods implemented by the component and the list of owned attributes. The user can specify  generalisation relations between classes.

A \textit{UML attribute} is characterised by its type, its name and a default value. There exist two ways to define the default value: either in the class specification, or in the component definition. The former will assign the same default value to the attribute for all components using this class or its successor; the latter is the approach to specify the default value of an attribute belonging to a particular primitive. 
%The attributes whose values are given in a component definition are considered accessible from outside of the component. 
If the default value is given both in the class specification and in the component definition, the second one has priority.

Every \textit{UML method} is described by a name, a list of input parameters and an output type. Additionally, any method belonging to a class can redefine an interface method; this means that the class method implements the corresponding method of an interface. The methods that do not redefine anything are considered as local ones, i.e. they are used for the component internal computations. For every class attribute the user should provide the signature of its set and get methods in order to access the attribute value. It would not be difficult to implement an automatic generation of those methods in the future versions of VerCors.

\paragraph{Type diagrams} The data types used by UML methods and attributes should be declared in a type diagram. The user can define integer intervals, enumerations, record types similar to C-like structs and arrays of fixed size. Additionally, there exist number of built-in types that do not appear on type diagram: boolean, integer and natural number types.  

\paragraph{}
\textit{The use-case example}
The Component diagram representing the architecture of our use-case model of Peterson's leader election algorithm is shown in Figure \ref{fig:peterson-components}. It relies on the UML class disgram illustrated in Figure~\ref{fig:peterson-classes}
% UML Class diagram of our example is given in Appendix \ref{sec:cl-diag}.

\texttt{Application} is a composite; it includes four primitives that
participate in the leader election process. The primitives are connected
in a ring topology and have similar structure. The entry point of the
system is the \texttt{runPeterson()} method of \texttt{Application}
server interface \texttt{S1}. This request is  forwarded to \texttt{Comp4} that triggers the election
process. During the election,
components
invoke method \texttt{message} on their client interfaces
\texttt{C1}. As defined in Section~\ref{subsec:peterson}, each message transmits two parameters: \textit{step} and \textit{val}. The message is transmitted to the server interface \texttt{S1} of the called
component. The signature of \textit{message} is specified in a UML
interface \texttt{ElectionItf}. If
a component decides to become a leader or a non-leader, it reports its decision to
the environment by invoking an
\texttt{IAmTheLeader(cnum)} or an \texttt{IAmNotTheLeader(cnum)}
method on its client interface \texttt{C2}. These
methods take the identifier of the component as a parameter. 

In order to illustrate the futures mechanism, the leader component triggers an external computation that returns a result; here we consider getting a key for encryption process.
%we extend the modelled system with a few constructs which are not involved in the leader election process.
We extend each primitive with a local method \texttt{encrypt(key)} and a client interface \texttt{C3} with a method \texttt{requestKey()} which should be served outside of the composite and which returns an encryption key. As we will show in the behaviour specification, the component who claims itself as the leader requests the encryption key by invoking \texttt{requestKey()} on its client interface, and performs the encryption once the key is obtained. 


 All four primitives have the same set of attributes. They have the \texttt{message(...)}
 method implementing the leader election algorithm and a list of methods to access local
 attributes. \texttt{Comp4} has an additional interface providing the method \texttt{runPeterson()} which triggers the election process.
 \texttt{Comp1}, \texttt{Comp2}, and \texttt{Comp3} are implemented by \texttt{Class0}
 while \texttt{Comp4} uses \texttt{Class1} that extends \texttt{Class0} with
 \texttt{runPeterson()} method. Initially, the components should have different default
 values of attribute \texttt{max} and \texttt{cnum}. \texttt{cnum} is a static unique identifier
 of a component. To specify the values of those attributes for every component
 individually, we define them in the \textbf{Attributes} field represented as a green box in
 each primitive definition.

\begin{figure}
     \centering
     \includegraphics[width=13cm]{drawings/peterson-components.pdf}
     \caption{A component diagram of Peterson's leader election use-case example}
     \label{fig:peterson-components}
 \end{figure}
 
\begin{figure}
     \centering
     \includegraphics[width=13cm]{drawings/peterson-classes.pdf}
     \caption{VerCors class diagram}
     \label{fig:peterson-classes}
 \end{figure}
%Figure \ref{fig:peterson_classes} illustrates the class diagram of our use-case example. The class 
%
%\begin{figure}
%     \centering
%     \includegraphics[width=13cm]{drawings/peterson_classes.jpg}
%     \caption{Class diagram}
%     \label{fig:peterson_classes}
% \end{figure}
 
\subsection{Behaviour specification}

\textbf{State machine diagrams} UML state machine diagrams are used for behaviour specification in
VerCors. Each state machine defines the behaviour of a single method of a UML
class.

A state machine has a set of states connected by transitions. In order to make the further analysis less complicated, we rely on flat state machines (they do not have hierarchical states) with only one region each.
A state stores its name, while the logic code is specified on the transitions. 
 The UML specification does not provide any strict syntax for the state machine labels, but since we would like to be able to analyse component behaviour and to perform the model-checking, we have to define a specific syntax.
%To enable behavioural analysis we defined a specific syntax 
%for UML transitions: 
A transition has a label of the form
\texttt{[guard]/action1....actionN} where  \texttt{guard} is a boolean expression and an 
\texttt{action} is an assignment or a method
call. An expression can be constructed from variables, constant values (integer and boolean values, enumeration elements), access to array elements, and arithmetic and logical operators. An assignment includes a name of a variable (or an array element) whose value is modified, and an expression or a method call whose value is copied into the variable. The syntax for a method invocation is \texttt{owner"."method\_name"("possibly list of argument)")"} where an \texttt{owner} is either a name of a client interface in the case of a remote method invocation, or "\texttt{this}" if the called method is local to the component.   
When constructing the grammar we tried to keep it coherent with the UML specification, expressive, and simple enough so that the labels can be analysed easily. 

State machine transitions should not include any variable declarations, instead, the local variables of a state machine must be declared in a special area. For each variable the user can specify its name and its type.
A state machine has access to its own local variables, to the
client interfaces and to the local methods of the component which behaviour
the state machine describes; the value of the component attributes can be accessed only through getters and setters.

The labels prefixed by "//" are ignored during model-checking and generated in the executable code. The programmer can use them in order define instructions which cannot be analysed by VerCors, and which do not have impact on the computations but serve for monitoring purposes. We often used them in our experiments in order to lot messages from the generated Java program.
% This is the way for the programmer to add the code which is specific to the implementation language and which cannot be analysed by VerCors. For example, it could be useful for logging. 
 However, the programmer should be very careful with such kind of instructions because VerCors does not check whether they have impact on the computations. Hence, if they, in fact, influence the control flow, VerCors cannot guarantee the model-checked properties in the generated code.
 
The usage of futures is transparent: the designer does not need to specify explicitly which variables are futures; whenever a state machine invokes a method on a client interface and assigns the result to a given variable, the variable is interpreted as a future. This set of constructs is sufficient to encode any behaviour of  
distributed objects; the control structures (like do-while, if-else) have to be encoded as guards on 
transitions.
 % The user enters the labels text, that will be parsed into
% a structure analyzed later by other tools in VerCors platform.  


\paragraph{}
\textit{The use-case example}
Figure \ref{fig:peterson-sm} illustrates the state machine of the
\texttt{message} method of Peterson's leader election algorithm. It
uses six variables where \textit{step} and \textit{val} are input
parameters of the method. The initial state is
illustrated with a blue circle (\texttt{Initial}). % First, the State Machine checks if
% the component is active.  
First, \texttt{Choice6} checks the phase of the election algorithm.  If the algorithm is in the
preliminary (zero) phase either the component is active -- it already
participates in the election -- or the component triggers the 
election process on its neighbour and performs the preliminary phase described in Section
\ref{subsec:peterson}. If it is not the preliminary phase, either the
component is passive and the message is forwarded to the neighbour
\texttt{[isActive==false]/C1.message(step,val)}, or the actions of the
state machine correspond to the two cases $M(1, x)$ or $M(2, x)$
depending on the value of \textit{step} (see
Section \ref{subsec:peterson}).
\begin{figure}[t]
     \centering
     \includegraphics[width=14cm]{drawings/peterson-sm.pdf}
    \caption{State machine diagram}   
     \label{fig:peterson-sm}
\end{figure}


As it was mentioned earlier, to illustrate future-based communications, we extend our use-case as follows.
If a component decides to become the leader, it sends a \texttt{requestKey()} invocation
on its client interface (see the transition from \texttt{State10} to \texttt{State12}). The request is forwarded to outside of \texttt{Application}.
Then, the component claims itself as the leader by sending an \texttt{IamTheLeader(cnum)}
request. Finally, the component calls its local method \textit{encrypt(key)} using the
result of \texttt{requestKey()} as a parameter. The component should be able to claim
itself as the leader before it receives the result of \texttt{requestKey()}. However, it
cannot execute \textit{encrypt(key)} if the \textit{key} is not obtained.
The VerCors user does not need to explicitly model future-based communications. Whenever a
state machine has a non-void client method invocation, it is interpreted  as a future-based one.

\textit{Scenario specification}

The user can additionally model the calls that the system will receive from its environment. For this purpose, the user specifies a state machine which has its own local variables and access to the server interfaces of the modelled application root component. A scenario can have loops and choice states as any other state machine, but it does not use the wait-by-necessity mechanism. We modelled a very simple scenario (in Figure~\ref{fig:scenario-sm} for our use-case example which invokes the method \texttt{runPeterson} triggering the election process once.

\begin{figure}
     \centering
     \includegraphics[width=6cm]{drawings/scenario-sm.pdf}
    \caption{Scenario state machine}   
     \label{fig:scenario-sm}
\end{figure}