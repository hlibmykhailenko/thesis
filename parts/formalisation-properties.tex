\section{Properties}
\label{sec:formalisation-properties}
The well-formedness definition of the preceding section guarantees that,
from an architectural point of view, the specified component assembly
is well-formed. It entails some properties both at deployment time and during execution. 
More precisely, the constraints specified above ensure the following
properties:
\begin{description}
\item[Component encapsulation] \emph{Bindings do not cross
  boundaries.} Indeed, \symb{GetSrc} and \symb{GetDst} predicates are
  only defined in the context of the parent component, for example,
  the call to \textsl{GetDst} and \symb{GetSrc} inside  the definition
  of the
  \symb{BindingRoles} predicate ensure that both bound interfaces are
  either internal interfaces of the parent component or external interfaces of
  its sub-components, which guarantees that no binding crosses
  component boundaries. If the source and the destination interfaces are not have the same context, the predicate returns an error.  
  The property allows preventing issues similar to the one illustrated in Figure~\ref{fig:properties-encapsulation} where a component (\texttt{Prim1}) outside a composite communicates directly with a sub-component (\texttt{Prim2}).
  
\item[Deterministic communications] The \symb{CardValidity} predicate
  guarantees that each singleton client interface is bound to a single server
  interface, which guarantees that \emph{each communication from a singleton interface is targeted at a single, well-defined,
  destination}. The predicate helps to avoid a situation illustrated in Figure~\ref{fig:properties-deterministic} where it is not clear which component (\texttt{Component2} or \texttt{Component3}) will receive requests from \texttt{Component1}.
   On the other hand, \symb{CardValidity}  takes into account collective communications and ensures that in the case of a multicast client, it will be bound to a well-defined set of target server interfaces. 
%  
%   Section~\ref{sec:NxMCommunications} will introduce
%  mutlticast interfaces to express communications with multiple destinations.
  
\item[Unique naming] Several predicates ensure the uniqueness of
  component or interface names
  in each scope (sub-components of the same component, interfaces of
  the same component, etc.). This restriction is crucial for
  \emph{introspection and modification of the component structure}. For
  example rebinding of interfaces can be easily expressed based on
  component and interface names.
  
\item[Separation of concerns] The definition of non-functional aspects
  ensure that: 1) \emph{each component has a well-defined nature}: functional
  if it belongs to the content, and non-functional if it belongs to
  the membrane. 2) \emph{each interface has a well-defined control level}, 
   depending on the component it belongs
  to and on the nature of the interface. The nature of components and interfaces is defined by the
  control-level (\symb{CL}) predicate. 3) \emph{Bindings only connect
  together functional} (resp. non-functional) \emph{interfaces}. 4) We clearly
  identify \emph{interceptor components} that are the only structural
  exception to these rules: an interceptor is a  component in the
  membrane that can
  have a single functional client and a single functional server
  interface, but as many non-functional interfaces as necessary.
  
\item[Correct typing] For each request sent by a client interface, there should be the corresponding method on the target server interface which is able to serve it. This is ensured by the predicate \symb{BindingTypes} which checks the compatibility between the method signatures of two interfaces connected by a binding. Figure~\ref{fig:properties-typing} illustrates an example of an architecture where the typing constraint is violated: the issue may occur if at run-time \texttt{Component1} invokes method \texttt{write()} on \texttt{Component2}, because the former cannot serve the request.
\end{description} 

\begin{figure}[t]
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/properties-encapsulation.pdf}
  \caption{Incorrect encapsulation}
  \label{fig:properties-encapsulation}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/properties-deterministic.pdf}
  \caption{Non-deterministic communication}
  \label{fig:properties-deterministic}
\end{subfigure}\newline
\begin{center}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/properties-typing.pdf}
  \caption{Incorrect typing}
  \label{fig:properties-typing}
\end{subfigure}%
\end{center}
%\begin{subfigure}{.5\textwidth}
%  \centering
%  \includegraphics[width=1\linewidth]{drawings/context-futures-continue.pdf}
%  \caption{Result return}
%  \label{fig:context-futures-continue}
%\end{subfigure}
%\caption{Request-reply by futures}
\label{fig:formalisation-properties}
\caption{Examples of architecture constraint violations}
\end{figure}

In order to guarantee that the generated ADL deploys correctly,
i.e. without runtime error, it is sufficient to ensure that (1) each
interface and class the ADL file refers to exists (which is ensured by
the generation process presented in Section~\ref{subsec:vce-adl-gen}), that (2) no binding exception will be raised
at instantiation time (which is ensured by the well-formedness property and
in particular by the determinacy of communications), 
%and that (3) each
%mandatory interface is bound and thus the component system can be
%started without error (which is again ensured by the
%well-formedness property dealing with bindings);  
and that (3) the
unique names ensure that the components and interfaces can be
manipulated adequately during instantiation. All those arguments ensure
that \emph{each ADL file generated by VerCors deploys correctly},
provided the well-formed property is verified by the system.


The properties verified by well-formed components not only guarantee
that the ADL generated from a well-formed specification deploys
correctly, but also ensure some crucial properties concerning the
runtime semantics (deterministic communications, separation of
concerns, reconfigurability ...).