\section{Reconfiguring multicast interfaces from component-controllers}

\label{sec:advanced-integration}
The internal and external interfaces of a composite component can be reconfigured by the non-functional components located in its membrane.
%A GCM non-functional component located in the membrane of a composite can reconfigure the internal and external interfaces of its container. 
In this section we explain how such reconfiguration can be modelled graphically, how it is encoded in pNets and translated into Java code. 

\subsection{Graphical specification}

While executing a server or a local method, a component-controller can trigger reconfiguration of an interface of its container.
As for any primitive in VerCors, the behaviour of non-functional primitives is defined using UML state machines (see Section~\ref{sec:vce-graphical-formalism}). Hence, the requests that trigger the reconfiguration should be also specified in the state machines. For this, we introduce two specific state machine instructions: \texttt{unbind} and \texttt{bind}, which represent method invocations on the \texttt{parent} component. They take two input parameters: the name of the interface to be modified and the index of the binding to be unbound or bound (remember the binding indices discussed in Section~\ref{sec:advanced-nulticast-graphical}).  

 Figure~\ref{fig:reconfig-sm} illustrates an example of an application where a component-controller unbinds an internal interface of its container. More precisely, \texttt{Controller} has one server method \texttt{unbindM1} which has only one instruction: \texttt{parent.unbind(Interfaces.M1, 1)}. Here, \texttt{M1} is the name of the internal interface which will be modified, and \texttt{1} is an index of the binding which should be removed.

\begin{figure}
\centering
    \includegraphics[width=12cm]{drawings/reconfig-example.pdf}
 \caption{Modelling binding reconfiguration}
 \label{fig:reconfig-sm}
\end{figure}

\subsection{From application design to pNets}

When generating the pNet of a composite, for each method of its multicast interfaces we construct the pLTSs of a group proxy and a group manager which encode possible reconfiguration. The only additional thing needed in order to trigger the reconfiguration is to synchronise the pNet of a component-controller with the queue of the composite when the reconfiguration request is en-queued. More precisely, we generate a synchronisation vector propagating each reconfiguration action of a component-controller so that it is visible from outside. Then, we synchronise it with the queue of the composite.

In our example, we extend the pNet of \texttt{Controller} with the following synchronisation vector:

\centerline{$\langle -, -, \symb{Unbind}\_\symb{Parent}\_\symb{M1}(\symb{t}) \rangle \rightarrow \symb{Unbind}\_\symb{Parent}\_\symb{M1}(\symb{t})$}
where the sub-nets are organised as follows: $\langle \symb{Queue}, \symb{Body}, \symb{unbindM1} \rangle$. 
Next, when generating the pNet of \texttt{Composite}, we synchronise the reconfiguration action of \texttt{Controller} with the queue of the composite:

\centerline{$\langle iQ\_\symb{Unbind}\_\symb{M1}(\symb{t}), -, -, -, \symb{Unbind}\_\symb{Parent}\_\symb{M1}(\symb{t}), - \rangle \rightarrow iQ\_\symb{Unbind}\_M1(\symb{t})$}
where the order of the synchronised sub-nets is:

$\langle \symb{Queue}, \symb{Body}, \symb{ProxyManagers}, \symb{Proxies}, \symb{MembraneSubcomps}, \symb{ContentSubcomps} \rangle$

\subsection{Implementing pNet generation and integration with CADP}

In order to generate pNets of applications with component-controllers which reconfigure interfaces of their containers, we include several additional steps in the pNet construction process discussed in Section~\ref{sec:vce-java}.

First, pre-processing the state machines which model the behaviour of component-controllers becomes slightly more complex. %
In addition to the usual analysis, we extract all reconfiguration instructions. For each reconfiguration instruction, we create a Java object which stores a reference to the modified interface, a reference to the modified binding, and the instruction type (bind or unbind). This object is stored in the parsed state machine and used when the transition label with the corresponding instruction is translated into a pLTS action.

When generating the synchronisation vectors for a pNet of a component-controller, we check whether the parsed state machines modelling its behaviour include reconfiguration instructions. If so, each analysed instruction is translated into a pLTS action and included in a synchronisation vector in order to be visible from outside of the component-controller. This allows us to synchronise it later with the queue of the composite-container.

Finally, when constructing the pNet of a composite, we check if the state-machines modelling the behaviour of its component-controllers include reconfiguration instructions. If so, each reconfiguration instruction is translated into a pLTS action and synchronised with the corresponding action in the queue of the composite.

\subsection{Code generation}

From a model of an application with reconfiguration of multicast interfaces, VerCors generates executable ProActive/Java code. However, the way reconfiguration instructions are specified in GCM/ProActive and in VerCors is quite different, thus the straightforward translation of reconfiguration statements from state machines to Java code is not possible.

For a given composite component, GCM/ProActive allows the programmer to get an instance of the \texttt{PAMulticastController} class which is able to reconfigure the multicast interfaces.  
For this, it has two methods: \texttt{bindGCMMulticast} and \texttt{unbindGCMMulticast}; both of them take two input parameters: the name of the multicast source interface and a reference to the target interface of the modified binding. On the other hand, in VerCors the reconfigured binding is referenced through the name of its source interface and a binding index (which does not exist in GCM/ProActive). 
When translating state machine instructions into Java code, we can compute statically the target interface from the binding index if the index is a constant value like in our example in Figure~\ref{fig:reconfig-sm}. However, it is not always the case: the index of the reconfigured binding in a state machine instruction can be specified as a variable. 

%From now and on we will speak about the indices of target interfaces in the reconfigurable group because we can assume that they correspond to the indices of the plugged bindings.

The solution we offer is to translate the mapping between bindings and their indices in VerCors into Java code in order to be able to retrieve the target interface by binding index during program execution. More precisely, when generating the ADL file of a component-controller, we include an additional attribute \texttt{hostReconfBindings} which encodes a mapping from the reconfigurable multicast interfaces to the list of target interfaces. The order of the elements in the lists corresponds to the indices of the plugged bindings. Listing~\ref{list:reconfig-adl} illustrates the attribute included in the ADL file of \texttt{Controller} from Figure~\ref{fig:reconfig-sm}.

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={An ADL attribute which stores reconfigurable interfaces}, label = {list:reconfig-adl}, basicstyle=\scriptsize]
<attribute name="hostReconfBindings" value="M1:[SubComp1.S1, SubComp2.S1]"/>
\end{lstlisting}

By default, when the GCM/ProActive factory constructs a component, it invokes a user-defined set-method for each of its attributes specified in the ADL file.
%The attribute value is parsed during the component construction when the GCM/ProActive factory invokes \texttt{set%\_hostReconfBindings} method on the component-controller . 
We generate a specific implementation of the \texttt{set\_hostReconfBindings} method which parses the value of \texttt{hostReconfBindings} and constructs two maps. \texttt{itfsMap} maps the name of a reconfigurable multicast interface to the list of names of the target interfaces, \texttt{compsMap} maps the name of a multicast interface to the list of names of the target components. The elements in the lists must be ordered according to the attribute value in the ADL file.  These names are used later in order to retrieve the target interface by its index.

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={Java code of a component-controller}, label = {list:controller-java}, basicstyle=\scriptsize]
public class Controller extends AbstractPAComponentController implements ... {
	//a map from the source reconfigurable interfaces to the target interfaces
	private Map<String, List<String>> itfsMap;
	//a map from the source reconfigurable interfaces to the target components
	private Map<String, List<String>> compsMap;
	
	//the method computes and returns a reference to an interface based on its name,
	// the name of its host component and the container of its host component	
	public Object getInterface(Container compContainer, String compName, String itfName) {...}
	
	//the method parses the input string and fills itfsMap and compsMap
	public void setHostReconfBindings(String val) {...}
	
	public void unbindM1() {
		...
		String tgtCompName = this.compsMap.get("M1").get(1);
		String tgtItfName = this.itfsMap.get("M1").get(1);
		ISingle tgtItf = (ISingle)this.getInterface(Container.CONTENT, tgtCompName, tgtItfName );
		Utils.getPAGCMLifeCycleController(this.hostComponent).stopFc();
		Utils.getPAMulticastController(	this.hostComponent).bindGCMMulticast("M1", tgtItf);
		Utils.getPAGCMLifeCycleController(this.hostComponent).startFc();
		...			
	}
}

\end{lstlisting}

Listing~\ref{list:controller-java} provides a simplified snippet of the Java code generated for the class implementing the behaviour of \texttt{Controller}. The maps storing the information about the reconfigurable interfaces are declared in lines~2-5. Line~9 defines a method which uses ProActive API in order to retrieve the reference to an interface by its name, the name of its component, and the container of its component. This method will be used in order to get the target interface of the reconfigured binding. The information about the container of the component to which the interface is attached is needed because the target interface can be attached to a component in the content (when reconfiguring an internal interface) but also to a component outside the composite (while the reconfigured interface is external). Line~12 defines \texttt{setHostReconfigBindings} method which is used by the factory to set the values of \texttt{itfsMap} and \texttt{compsMap}. Finally, the translation of \texttt{parent.unbind(Interfaces.M1, 1)} instruction is given in the lines~16-21. It, first gets the names of the target interface and of the target component (lines~16-17) based on the source interface and the binding index. Here, the binding index is equal to 1 but it could also be a variable or an expression. Then, we use the retrieved names for the invocation of the \texttt{getInterface(...)} method in line~18  which returns a reference to the target interface. Now, all the information needed for the reconfiguration is gathered, but before modifying an interface of a composite component, we have to stop its functional part (line~19). Note, that here we stop \texttt{this.hostComponent}, i.e. the composite containing our component-controller. Once the functional part is stopped, the reconfiguration can be performed (line~20). Finally, the composite can be started again (line~21).    


