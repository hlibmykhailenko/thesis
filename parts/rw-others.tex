\section{Other frameworks}
\label{sec:rw-other}
In this section we discuss several other frameworks for modelling and verification of component-based systems. We start by the component models and dedicated development tools. Then, we make an overview of the verification tools that could be potentially used by VerCors as an alternative to CADP. 
%Finally, we present the other formalizations of Fractal and GCM.

\subsection{Component models and tools}

\paragraph{Palladio.} 
Another framework for component-based systems modelling is provided in the Palladio \cite{Becker:2007:MPP:1216993.1217006} project which presents the Palladio Component Model (PCM) and a development environment. 
%As opposed to VerCors/GCM, Palladio does not focus on ensuring functional properties of a system. Instead, it focuses on the application performance prediction.
PCM allows specification of primitive and composite components, their interfaces with method signatures and connectors. In addition, the information about resource consumption should be specified for the served methods. The PCM is supported by an Eclipse-based development environment where the user can model a system and simulate its execution. Alternatively, the Palladio models can be automatically extracted from the system implementation provided in Java, C or C++. Based on the input model and the results of simulation, the tool is able to predict a number of system performance metrics such as the response time, throughput and resource consumption.  

While both Palladio and VerCors are used for system analysis at the early design stage, the two frameworks have different objectives. Palladio targets mainly performance analysis while VerCors is able to verify functional properties of a system by applying formal verification techniques. We should also highlight here several limitations of the Palladio framework. First, the results of performance prediction for concurrent systems significantly differ from the actual performance as mentioned in \cite{Becker:2007:MPP:1216993.1217006}. Another limitation is that Palladio does not support modelling of reconfigurable systems. While we did not address the performance here, we showed that we are able to handle certain forms of reconfiguration in VerCors.

\paragraph{DEECo.}
Another framework developed by the authors of the SOFA component model is the DEECo (for Dependable Ensembles of Emerging Components) component model~\cite{Bures:2013:DEC:2465449.2465462}. It is used for the implementation of large-scale distributed ensembles of components which cooperate together in order to achieve a common objective. An application in DEECo is constructed from components assembled into ensembles. A component features a hierarchical data  structure called \textit{local knowledge} which is a mapping from the data variables to their values. The knowledge of a component can be exposed to the other components through its interfaces. A component has a set of \textit{processes} which are basically tasks manipulating the knowledge. A process can be scheduled so that it is either executed periodically or triggered each time a particular condition is met. The DEECo components are assembled into flat ensembles where one component plays the role of a \textit{coordinator} and the others are the \textit{members}. When a programmer describes an ensemble, he should specify the membership conditions, i.e. the interfaces that should be exposed by the coordinator and the members of the ensemble. The ensembles are formed dynamically from the set of components that satisfy the defined conditions. The components do not explicitly send messages to each other, instead they exchange their knowledge. The ensemble specification defines how the coordinator exchanges the knowledge with the members, thus defining an abstraction for the one-to-many communications. The DEECo component model features the formal semantic including the time aspect and presented in~\cite{DeecoTechRepo}.
%The component model has been extended with the real-time constructs 

The DEEco component model is implemented in the jDEECo framework~\cite{JDEECo} which provides the Java libraries necessary for the development, deployment, and execution of the DEECo ensembles. The framework is integrated with the Java PathFinder model-checker for the verification of the designed applications.

One of the advantages of the DEECo framework is that thanks to the notion of ensembles and membership conditions it allows implementing much more dynamic applications than GCM/ProActive systems. On the other hand, the dynamically evolving structure of such applications makes it more challenging to reason about their properties formally. To the best of our knowledge, neither modelling of future-based communications, nor design of hierarchical applications is possible with DEECo as opposed to the GCM/VerCors framework. 

\paragraph{Helena.} 
Helena\cite{Futatsugi14} is another framework for modelling highly dynamic ensembles of autonomic distributed components. In addition to the components and ensembles, the Helena approach relies on the notions of \textit{roles}. A Helena \textit{component} is characterised by a set of attributes and operations that can be served. It can be said that a component "fulfils" particular \textit{roles} which describe the functionalities of a component in terms of attributes and operations. There can be several components able to fulfil the same role. A set of roles connected by so-called \textit{role connectors} form a Helena \textit{ensemble}. The components filling the roles of an ensemble collaborate in order to achieve a particular goal. The ensemble structure specifies the size of the queue for the input messages of each role. 
%No particular interaction paradigm has to be fixed at the architecture level.
%No particular communication paradigm is fixed for the Helena systems. 
The behaviour of a role can be modelled as an LTS whose transitions are labelled by either message sending or message reception over a role connector. The LTSs of roles behaviour are assembled into an ensemble behaviour automaton where each state represents an ensemble state and transitions are labelled by message labels.
The Helena roles can communicate both by synchronous and asynchronous message-passing. The communication style depends on the role queue size specified by the user. Thanks to the role-based modelling, the Helena framework allows one to design highly dynamic heterogeneous systems with rich communication patterns. On the other hand, the variety of components that which can be dynamically included in the ensembles, makes it more challenging to reason about the designed applications. 

HelenaText\cite{DBLP:conf/facs2/KlarlCH14} is an Eclipse-based text editor for Helena ensembles specification. The editor features syntax highlighting and content assistance. A HelenaText file can be automatically translated into the implementation code that can be executed on the jHelena\cite{DBLP:conf/aswec/KlarlH14} platform. A model specified in HelenaText can be transformed into Promela \cite{DBLP:conf/spin/Klarl15} and model-checked with Spin as demonstrated in \cite{DBLP:conf/birthday/HennickerKW15}. 
At this point, the programmer chooses the communication paradigm for the translated processes (either asynchronous or synchronous message-passing) by specifying the input queue size for the roles. 
The typical property verified by the authors states that in a modelled peer-to-peer system supporting distributed storage of files the requester will always receive the requested file. The peers are connected in a non-parameterised ring topology.

While both Helena and VerCors/GCM are used for modelling and implementation of distributed systems, they should be applied for different types of applications. To the best of our knowledge, neither hierarchical components, 
%no group communications,
 nor futures mechanism can be modelled, verified or executed with Helena. On the contrary, more custom patterns like publish-subscribe are easy to express in Helena and more difficult to encode in VerCors.
%Helena is a framework for ensemble-based systems with a focus on the role of components  
%HelenaLight is a restricted version of Helena where the notion of data is omitted.
  
\paragraph{Credo, Reo, and Creol.}
Credo\cite{DBLP:conf/fmco/GrabeJKKSBBABG09} is a toolsuite for modelling and analysis of highly reconfigurable asynchronous distributed systems. The design process comprises two main steps.
First, the high-level application dataflow is designed with Reo\cite{Arbab:2004:RCC:992032.992035} which allows specifying a reconfigurable network of components where only a \textit{facade} of a component is visible. A facade consists of a set of communication points called \textit{ports}, event declarations, and an abstract behaviour modelled by constraint automata~\cite{Baier200675} specifying the order of raised events and port operations. The ports of components are connected into a \textit{network}. A \textit{networkmanager} defines how the events raised by the components are handled by the network. 
 The ports are synchronised with an automaton modelling the  communication inside the network. Overall, this specifies the inter-component communications.

At the second step, the intra-component behaviour is designed with an object-oriented executable modelling language Creol~\cite{johnsen07sosym}. In fact, Creol programming model is very close to ABS. Creol objects rely on asynchronous method calls and processor release points, feature an execution thread, a set of attributes and methods to be served. The communication paradigm in Creol is based on request/reply by futures mechanism %except that the user can explicitly ask an object not to wait for the reply.

Credo, is able to check the conformance of the models specified in Reo and Creol. The tool transforms a facade with its behaviour into an intermediate abstract behaviour specification \cite{Grabe2010} from which a Creol model is derived. The transformation result is executed together with the original component specification in Creol in a special version of Maude\cite{Clavel2002187} configured for the testing purpose.
In addition, Credo can check conformance and between the Creol model and the actual implementation in C by testing. Furthermore, a Creol skeleton of the application can be automatically derived from the network specification. The network specification can be checked for  absence of deadlocks. 

Apart from the tools provided in Credo, the Creol language is supported by an Eclipse-based modelling environment which encompasses a type-checker and a simulation environment. Creol programs can be executed using Maude which additionally provides means for model-checking of infinite-state models.

Credo is a powerful framework for modelling and analysis of distributed systems where a reconfigurable network of processes is modelled independently from the actual processes implementation. However, in order to master Credo, the user will have to learn Reo, Maude and Creol while in VerCors the system specification is based on the UML models well-known among the programmers. Additionally, VerCors is able to generate executable Java code of a system.

\paragraph{CORBA Component Model and Cadena.}

The CORBA Component Model (CCM) \cite{CORBA} was designed by the Object Management Group for modelling and implementation of distributed components. Components in CORBA have provided and required ports, publish events on the ports, store attributes. CCM allows components to be dynamically created, connected, and disconnected.

CCM is supported by a variety of development frameworks. The one closest to VerCors is presented in the Cadena \cite{DBLP:conf/icse/HatcliffDDJR03} project. Cadena is implemented as an Eclipse plugin and supports the following development workflow. First, the user should load a library of predefined domain-specific components and define his own project-specific components with their dependencies. Second, the user can specify the non-functional information such as the distribution related data. Finally, Cadena uses Bogor \cite{Robby:2003:BEH:949952.940107} to generate system state-space and model-check the global system properties. Bogor is able to check deadlock-freedom and safety properties expressed as assertions and invariants. Alternatively, the conceptual model can be transformed into an input for the dSpin\cite{DIS99i} model-checker able to verify LTL formulas.

Overall, Cadena includes a number of features that are not supported by VerCors such as real-time systems analysis, modelling of sensor networks, integration of domain-specific libraries of components. It would be very useful to implement the latter functionality in VerCors. On the negative side, Cadena does not support modelling hierarchical systems and request/reply by futures.   

\paragraph{Omega2.}
A tool for modelling and verification of timed component-based systems which completely relies on the UML formalism is presented in the OMEGA project. An application architecture can be modelled with hierarchical UML composite structures and classes, the behaviour on the components' ports can be designed as UML state machines. An OMEGA component can be either executed in its own thread or share a thread with the other components. The components can communicate either synchronously or asynchronously. The specified models can be translated into an input for the IF \cite{bozga:hal-00369349}  validation environment. From the given model, IF generates Promela code, on which one can model-check LTL formulas in Spin. The tool is also linked to CADP Evaluator for checking MCL formulas and Kronos\cite{Yovine1997} for verifying TCTL formulas on timed automata.

Again, VerCors does not target timed systems. Instead, as opposed to OMEGA, VerCors provides capabilities for design and verification of system reconfiguration and ensures separation between the business logic and the control part of an application.
%\paragraph{something for choreographies}

\paragraph{A graphical designer and a code generator for CADP.}
Except from the model-checking part, CADP is also equipped with a front-end designer ELOTON\cite{ELOTON} and a code generation module~\cite{evrard:hal-01086522}, i.e. CADP could be used for modelling, verification and executable code generation. ELOTON aims at helping the users write LOTOS formal specification. It comprises a text editor featuring text highlighting, auto-completion, error marking, and a graphical visualizer of the specified graphs. The code generator translates a distributed system specification from LotosNT to the executable C code. The generated program can be connected to the external code via user-modifiable C-functions.  
We believe that the programmers who are not familiar with LOTOS should benefit from using VerCors because it allows specifying software on higher level than ELOTON and is better adapted to the user who is not an expert in formal methods. For example VerCors allows one to define system behaviour as a set of UML state-machines while in ELOTON it is specified textually in LOTOS.
Regarding the code generator, it would be interesting to investigate whether we could use the approach presented in~\cite{evrard:hal-01086522} to connect the executable code produced by VerCors to the external functions.
%\TODO{a bit mode about the front-end and the code verification}

\subsection{Verification platforms}

In this section we discuss some of the verification platforms that could be potentially used by VerCors as an alternative to CADP. We analyse here three verification toolsets that are used by many component development frameworks in the literature, namely: Spin, NuSMV, and Maude.

%\paragraph{LTSmin}
\paragraph{Spin.}

Spin\cite{Holzmann:1997:MCS:260897.260902} (for Simple Promela INterpreter) is an open-source model-checker for multi-threaded software systems specified in the high-level language called Promela (a Process Meta Language)\cite{Holzmann:1990:DVC:95422}. It is linked to a tool which is able to extract the Promela models from the implementation level C code based on some guidelines from the user\cite{10.1109/TSE.2002.995426}. Spin can benefit from exploiting several cores for model-checking as presented in \cite{Holzmann:2007:DME:1314033.1314051}. The tool supports verification of systems with dynamically growing number of processes (a Promela process can instantiate other processes at run-time). Besides model-checking, the tool provides simulation capabilities which allow for the early system prototyping.

Spin supports verification of liveness and safety properties as well as deadlock-freedom, the logical consistency of the specification and absence of unexecutable code for the finite systems. The verified property can be expressed as an LTL formula, as a process invariant (based on assertions) or a Büchi automaton.  

Given system specification in Promela and a property to be checked, Spin generates the C code of the verifier which implies that a new verifyer will be constructed for each property. In order to tackle the state explosion, Spin performs on-the-fly verification, which avoids construction of the global state graph. Additionally, Spin relies on the partial order reduction techniques \cite{Peled:1994:CPO:647763.735529} in order to reduce the number of states considered during model-checking depending on the verified formula. 

Spin is a popular model-checker which has been proven to be efficient by multiple examples from industry and academia \cite{CORBA-SPIN, SPACECRAFT-SPIN, FLIGHTGUIDANCE-SPIN}. Seeing, the successful results of applying Spin to large systems, it would be interesting to investigate whether the GCM components could be translated into Promela and model-checked with Spin.
% in order to benefit from the on-the-fly model-checking and exploiting multiple cores for verification.
The main challenge here would be translating the components hierarchy, because Promela supports only specification of flat systems. Such issue does not appear while using CADP because CADP includes the mechanisms for modelling hierarchical structures. This is one of the reasons why we prefer using CADP but not Spin for our first verification experiments in VerCors. Another reason is that unlike Spin, CADP uses branching bisimulation for state-space reduction which are efficient for the GCM systems because many processes in various components have bisimular behaviour (e.g. the attribute controllers, proxy managers).  

\paragraph{NuSMV.}

%in nusmv, the vars are on the states
NuSMV\cite{CAV02} is a model-checker supporting symbolic verification of synchronous and asynchronous systems. The tool takes a set of hierarchical finite state machines expressed in the SMV language and checks system properties written in CTL or LTL. 

Each state machine has local variables and could be seen as a reusable module which can be instantiated multiple times. The modules are composed together (either synchronously or asynchronously) into a so-called parent module. The latter has access to the local variables of its sub-modules and can pass any of them by reference to another sub-module as well as its own variables. Thus, the state machines can share variables. 
The composition style - synchronous or asynchronous - is defined by the parent module.
In the first case all sub-modules move at each step simultaneously. In the case of asynchronous composition, only one randomly chosen sub-module proceeds at each step. In order to ensure fair interleaving, the programmer can define a fairness constraint saying that each asynchronously composed module will execute infinitely often. Every SMV program should have the root module \texttt{main}. A state machine can express deterministic or non-deterministic behaviour.   
%
% Let us assume a module \texttt{B} includes two sub-modules: \texttt{A1} and \texttt{A2}, they store local variables \texttt{b}, \texttt{a1} and \texttt{a2} correspondingly. 
%
%The letter has access to the local variables of the sub-modules and can give access .
% In the case of asynchronous composition, several modules execute in parallel and their actions are asynchronously interleaved.

%The input processes can rely on synchronous and asynchronous communications, have hierarchical structure and reusable components. 

Similar to CADP, for each verified property, NuSMV answers whether it holds on the given system and provides a counterexample if the property is not satisfied. The verification engine is an implementation of the symbolic model-checking \cite{McMillan:1993:SMC:530225} which combines BDD-based (Binary Decision Diagrams) and SAT-based techniques \cite{frocos02} for bounded model-checking. Before the verification is started, the input system is preprocessed in several steps including flattening and applying reduction techniques \cite{Berezin98compositionalreasoning}. Then, the user chooses which verification mechanism should be triggered: either the symbolic model-checking or SAT-based verification. In the first case, the model-checker builds a BDD-based representation of the input model and checks LTL or CTL formulas on it. In the second case, NuSMV needs to be linked to an external SAT-solver that could be either MiniSat\cite{DBLP:conf/sat/2003} or Zchaff\cite{Zchaff}. The user should provide the length of a counterexample, and NuSMV translates the given LTL model-checking formula into a SAT problem. 
% Additionally invariant checks can be done.

The latest version of NuSMV is distributed as an Eclipse plug-in and is an open-source project. It is used as a back-end model-checker by multiple projects aiming at rigorous development of embedded systems \cite{mbeddr, twotowers, goanna}. It would be interesting to apply the symbolic model-checking techniques provided by NuSMV to the GCM components modelled in VerCors. For this, we would first have to translate the pLTSs into the finite state machines. The main challenge here would be to deal with the synchronisation mechanism of SMV which is different from the one of LOTOS and EXP as discussed in \cite{lvl-2008-1}. In fact, some studies about the symbolic reasoning on pNets have already been started in~\cite{DBLP:conf/forte/HenrioMZ16}, but they are still at a theoretical stage.

\paragraph{Maude.}

Maude \cite{Clavel2002187} is a rich framework based on the rewriting logic for rigorous development of various application types including concurrent and distributed object-oriented systems. The platform comprises an executable modelling language and a set of analysis tools. 

The Maude language is based on the rewriting logic which has an underlying equational logic as a parameter. The basic elements of a Maude program are \textit{equations} and \textit{rules} that have rewriting semantics meaning that during the program execution the lefthand side pattern will be replaced by an instance matching the righthand side pattern. Equations are used as simplification patterns while rules can be seen as transition rules for a possibly concurrent system and a way to express the interaction between different processes. Maude supports user-defined syntax of operators, objects and types with inheritance. A Maude program containing rules and possibly equations is called a system module. The modules can form a hierarchy.

A Maude program can be both executed, formally analysed, and verified. The verification toolsuite includes an inductive theorem prover, a tool able to prove that a Maude program terminates, a model-checker for the temporal logic formulas (LTL, CTL, CTL* and others), tools for specification and verification of real-time and probabilistic systems. 

Systems from different domains can be specified and analyzed with Maude. This includes semantics of programming languages, distributed algorithms, biological applications. We believe that VerCors could benefit from translating a GCM system specification into Maude mainly because Maude supports parametrised modules which could be potentially used for analyzing GCM applications with parameterised topologies. Another reason is that we could try to apply the Maude LTL bounded model-checker \cite{Escobar:2007:SMC:1779782.1779795} which able to verify infinite state-space systems. The tool
checks LTL formulas on symbolic representation of an application state-space and either finds a finite state-space for which the formula is fully verified, or performs the verification up to a given bound and does not succeed to produce the result, or provides a counterexample.

Moreover, VerCors could benefit from the rich and expressive type system supported by Maude. However, translation of GCM components specification into equations and rules of rewriting logic would require significant effort. Still, we believe that it should be possible because a translation to Maude has already been done for the ABS language which shares a lot of common features with GCM/ProActive. Moreover, Maude provides a mechanism for modelling object-oriented systems.



%The type of rewriting typical of functional modules terminates with a single value as
%its outcome. In such modules, each step of rewriting is a step of replacement of equals
%by equals, until we $nd the equivalent, fully evaluated value. In general, however, a set
%of rewrite rules need not be terminating, and need not be Church–Rosser. That is, not
%only can we have in$nite chains of rewriting, but we may also have highly divergent
%rewriting paths, that could never cross each other by further rewriting.
%The essential idea of rewriting logic [43] is that the semantics of rewriting can
%be drastically changed in a very fruitful way. We no longer interpret a term t as a
%functional expression, but as a state of a system; and we no longer interpret a rewrite
%rule t → t  as an equality, but as a local state transition, stating that if a portion of
%a system’s state exhibits the pattern described by t, then that portion of the system
%can change to the corresponding instance of t  . Furthermore, such a local state change
%can take place independently from, and therefore concurrently with, any other non-
%overlapping local state changes. Rewriting logic is therefore a logic of concurrent state
%change

%
%    Models of computation. Many models of computation, including a very wide range of concurrency models, can be naturally specified as different theories within rewriting logic, and can be executed and analyzed in Maude.
%    Programming languages. Rewriting logic has very good properties—combining in a sense the best features of denotational semantics’ equational definitions with the strengths of structural operational semantics—to give formal semantics to a programming language. Furthermore, in Maude such semantics definitions become the basis of interpreters, model checkers, and other program analysis tools for the language in question.
%    Distributed algorithms and systems. Because of its good features for concurrent, object-based specification, many distributed algorithms and systems, including, for example, network protocols and cryptographic protocols, can be easily specified and analyzed in Maude. Furthermore, making use of Maude’s external object facility to program interactions with internet sockets, one can not just specify but also program various distributed applications in a declarative way (see Section 8.4).
%    Biological systems. Cell dynamics is intrinsically concurrent, since many different biochemical reactions happen concurrently in a cell. By modelling such biochemical reactions with rewrite rules, one can develop useful symbolic mathematical models of cell biology. Such models can then be used to study and predict biological phenomena.

%\subsection{Formalisms}
%\TODO{Maybe, we could move it to the formalization chapter?}
