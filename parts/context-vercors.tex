\section{VerCors}
The VerCors platform has already undergone several major generations, with significant
evolutions for the underlying semantic model, as well as the modelling platform and the
specification formalisms. 

The very first version of VerCors was based on a textual description of component architecture.
The original version of the graphical front-end editor called \textbf{CTTool} \cite{conf/sccc/AhumadaABCMS07} %\cite{AABCMS:SCCC07}07} 
was using UML
component structures for describing the application architecture and activity diagrams for behaviour modelling. CTTool was generating LOTOS specification that could be given to CADP for model-checking. The tool was extensively used in the CoCome case-study \cite{CoCome}. The problem was that, at that time, the authors already aimed at using pNets as an intermediate format and they were not able to implement properly all the constructs with CTTool.
Additionally, the authors realised that the UML components were too far
from GCM needs. 

Hence, a new DSL for component structure and a new graphical formalism were defined and implemented in a tool called \textbf{VCE} (VerCors Component Editor) \cite{Cansado2009}. At the same time, aiming at better support for maintenance and usability, the platform was moved to an Eclipse-based environment and EMF. The new version included a graphical designer, an architecture static validation engine, an ADL generator, and a module transforming ADL into graphical models. The generated ADL could be given to \textbf{ADL2N} tool that transformed it into a pNet in \textbf{fc2} format \cite{FC2Tool}. The signatures of methods had to be specified in Java interfaces. Additionally, with the help of dedicated GUI, the user had to abstract the data domains. Then, a generated pNet and a set of manually written fc2 files with server methods behavior could be given to \textbf{FC2Parametrized} \cite{Barros-phd} tool which instantiated the system and produced EXP files. 

%In \cite{conf/sccc/AhumadaABCMS07} the authors discuss the ideas on using UML for behaviour specification %which was not implemented at that time.
A series of
publications \cite{Cansado2010155, BHM:wcsi10}
described the support for several features of distributed component-based
systems, including group communications and futures, but the toolchain from the specification to behavioural model generation was incomplete and relied on several manual steps. Moreover, generation of the behavioural models including some particular features (e.g. group communications) has been discussed and illustrated by examples in the previous works, but has never been  implemented.

The work presented in this thesis has significant changes regarding the previous tools. It is the first version of VerCors that gathers all bits and pieces of the previous works and implements fully automatised generation of behavioural models and executable code. The key improvements are listed below:: 

\begin{itemize}
\item[$\bullet$]
This is the first version of VerCors that integrates the GCM architecture DSL with UML and allows specifying all core features of GCM. We extended the existing architecture DSL with references to UML classes and interfaces that include method signatures. We integrated UML state machine editor for behaviour specification and we defined precisely the state machine structure and semantics in the context of GCM. The graphical formalisms are explained in details in Section~\ref{sec:vce-graphical-formalism}. In fact, there was no graphical editor for behaviour specification in the previous versions.  
\item[$\bullet$]
Second, we changed the underlying platform to Obeo Designer and we benefit from its rich infrastructure for building and maintaining graphical editors. We discuss the architecture of the latest version of VerCors in Section~\ref{sec:vce-architecture}.
\item[$\bullet$]
We refined and extended the existing set of architecture static validation constraints to deal with the non-functional aspect. The formalisation of the validation rules and the implementation of their verification are given in Chapter~\ref{chap:formalisation}.
\item[$\bullet$]
Next, since the latest version of VerCors, the semantics of GCM components in pNets has significantly evolved, and all the changes are taken into account in the new version. We generate pNets directly from the graphical representation. Moreover, this is the first version of the platform which automatically constructs the behaviour of server and local methods. We explain in details the latest version of the pNets encoding the GCM component semantics and the VerCors pNets generator in Section~\ref{sec:vce-pnets-gcm}. We address the advanced features (the non-functional aspect, the group communications) in Chapter~\ref{chap:advanced}.
Still, we reuse some code for pNets construction from the previous versions.
\item[$\bullet$]
This is the first version of VerCors where the user can specify graphically a reconfigurable system, automatically translate it into an input for the model-checker and generate its Java code. For more details, we refer to Section~\ref{sec:advanced-reconfiguration}.
\item[$\bullet$]
We almost fully reuse the engine generating ADL. We improved it significantly by implementing the non-functional part generation. 
\item[$\bullet$]
This is the first version of VerCors that produces executable Java code from the graphical model. The generator is discussed in Section~\ref{sec:vce-java}.

\end{itemize}
%
%
% For the graphical designer, we extend the existing 
%
%
% A series of
%publications described the support for several features of distributed component-based
%systems, including group communications, first-class futures,
%%\cite{CHM:FACS08}, 
%and reconfiguration.
%%\cite{GHM:FACS13}
%At that time, the
%platform was only able to generate part of the behavioural model and it relied on several
%manual steps only realizable by experts in formal methods. No code generation was
%supported. 

In the rest of this thesis we will first make a overview of the current version of the VerCors platform in Chapter~\ref{chap:vce-overview}. We will present it from the user point of view, explain its core functionalities, and describe its architecture which relies model-driven technologies. Then, in Chapter~\ref{chap:formalisation} we will formalise the architecture of GCM components and the notion of component well-formedness. Next, in Chapter~\ref{chap:vce-core} we will formalise how the basic behaviour of GCM components can be encoded in pNets, we will explain how the formalised generators are implemented in VerCors, and how the constructed pNets can be then given as an input to the model-checker of CADP. The generation of the executable code which can run on top of ProQActive is also presented in Chapter~\ref{chap:vce-core}.

\label{sec:context-vercors}