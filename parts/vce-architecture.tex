\section{The architecture of VerCors}
\label{sec:vce-architecture}

\begin{figure}[t]
     \centering
     \includegraphics[width=13cm]{drawings/vce-architecture.pdf}
     \caption{Architecture of VerCors}
     \label{fig:vercors_arch}
 \end{figure}

VerCors is implemented as a set of plug-ins for Eclipse; its architecture is illustrated in Figure \ref{fig:vercors_arch}. The modules of the platform can be divided into four categories based on their functionality: meta-models with their EMF editors, graphical designers, generators, and integration plug-ins. 

\paragraph{Meta-models} VerCors relies on six meta-models based on the EMF ecore technology.
%\footnote{https://eclipse.org/modeling/emf/}. 
The \textbf{Components} meta-model is used for the component-based system architecture specifications. Its structure reflects the GCM components structure.
The Components meta-model references the \textbf{Eclipse UML}~\cite{Eclipse-UML} meta-model for the UML classes, UML interfaces and components behaviour specification (state machines).
Additionally, we implemented the \textbf{SMVariables} meta-model for the variables declaration on the UML state machines.
The types used by the state machine variables and UML method signatures are based on the \textbf{VCETypes} meta-model. Its root element \textit{VCEType} extends UML \textit{Type} which allows using the types declared by the user in the specification of the UML elements.
The \textbf{pNets} meta-model is used for the pNets construction.
Finally, the pLTS' labels are based on the \textbf{Expressions} meta-model. Its structure reflects the grammar of the UML state machine labels.  

\paragraph{Graphical designers} The four graphical designers provided by VerCors are fully based on the Obeo Designer platform and rely on the meta-models described above. 
The core editor is the \textbf{Components} graphical designer where the user can graphically specify the architecture of his/her application. The component designer includes a \textbf{static correctness validator} which checks the correctness of the user-defined models with respect to a set of rules formalised in Chapter~\ref{chap:formalisation}. 
The \textbf{Obeo UML} graphical designer is integrated into VerCors and can be used to define classes that implement components, UML interfaces that define signatures of methods of component interfaces, and state machines that specify component behaviour. Obeo UML designer includes a number of other UML diagram editors (e.g. Use-case and Activity diagrams).
We extended Obeo UML state machine diagram editor with tools and graphical representation for the \textbf{Variable declarations}.
Finally, \textbf{VCETypes} designer can be used for the specification of the types built from integer intervals, enumerations, records, arrays of fixed size, and boolean.

In addition to the graphical designers, there exist so-called EMF editors that represent a model as a tree-like structure and allow its modification. We generated the EMF editors for those structures that can be edited by the users of VerCors (i.e. components, state machine variables, and VCETypes). An EMF editor for the UML models is included in the UML Eclipse plug-in.

\paragraph{Generators} The core part of the VerCors platform is the GCM/ADL+Java generator which produces the  implementation code of a modelled system and a pNet generator that constructs the input for the model-checker. Both construction processes involve the analysis of a component behaviour modelled with UML state machines. More precisely, the behavioural instructions are specified as a state machine labels, and in order to interpret them, both generators invoke a dedicated \textbf{Parser} which takes a state machine, parses all its transition labels in the context of a given component, and returns a map from a state machine transition  to an instantiation of the \texttt{Expression} meta-model classes corresponding to the parsed label. The context is used to establish references to the signatures of the methods invoked by the state machine. We implemented the parser using the combination of the Cup~\cite{JCup} and JFlex~\cite{JFlex} technologies. They allow one to specify textually the BNF grammar of the parsed text and to map the grammar symbols and expressions into a sequence of actions which will be performed each time when the parser recognises a symbol or an expression. The actions in our case include the instantiation of the \texttt{Expression} meta-model classes. From the given specification, Cup and JFlex produce the Java code of the parser.   

The \textbf{ADL generator} takes a Component diagram and a package name as an input and produces an XML-based (GCM/ADL) file with the given architecture. The package name corresponds to the package where the Java classes and interfaces will be produced. Then, the ADL generator invokes a \textbf{UML generator} that analyses the UML, VCETypes, and Component models and uses Acceleo templates in order to produce Java classes and interfaces. Both generators are explained in Section~\ref{sec:vce-java}.
For every set/get method of a UML class, the UML generator produces the corresponding template-based Java code. For every method which behaviour is defined by a state machine diagram, UML generator translates the parsed version of the state machine into Java.
%invokes a \textbf{state machine parser} that parses the labels of the state machine transitions in the context of a given component architecture.  The parsed state machine is then translated into Java code of the method.

The \textbf{PNets generator} takes the following input: component architecture, referenced UML elements, scenario state machine if there is any, queue size for each component, and interactions that should be hidden during model-checking. Then, it processes the input as follows:

\begin{enumerate}
\item
\textbf{Pre-processing}. The pre-processor analyses each composite component in the model being generated and gathers auxiliary information. For each server interface it finds the sub-component that will process the requests. For every client interface it finds the sub-components that can send the request. Then, the state machine Parser is invoked to parse labels of all state machines of the primitive components and to gather information about local and remote methods invoked by each state machine.
\item
\textbf{PNets generation}. Starting from the root component, a pNets generator recursively produces a pNet encoding the behaviour of each component. More precisely, for a composite, it generates pLTSs of internal processes (body, queue, etc), produces a set of synchronisation vectors, and triggers the pNet generation for each subcomponent. For a primitive, it produces pLTSs of internal processes and a set of synchronisation vectors. The formalisation and the implementation details are given in Section~\ref{sec:vce-pnets-gcm}. The scenario state machine is also translated into a pLTS. Synchronisation vectors of the root component include synchronisation with the scenario. 
\item
\textbf{Fiacre generation}. Every constructed pLTS is translated into a \texttt{.fiacre} file. This and the following two generators are presented in Section~\ref{sec:vce-model-checking}.
\item
\textbf{EXP generation}. A set of synchronisation vectors of each pNet is translated into an EXP file.
\item
\textbf{Auxiliary scripts generation}. For every pNet we generate a script assembling its sub-nets into a common structure with respect to the synchronisation given in the corresponding \texttt{.exp} file. The scripts also hide communications that should not be observed during model-checking. More precisely, VerCors generates one \texttt{.svl} file for each pNet encoding the behaviour of a component which should be model-checked. The script does the necessary renaming in the sub-nets, invokes EXP.OPEN in order to construct an automaton, hides some of the communications in the generated LTS, and finally reduces the state space to obtain the final model of the component behaviour. In addition, VerCors creates a \texttt{.sh} file which calls the Flac compiler on each generated \texttt{.fiacre} file and triggers the execution of each produced SVL script. The order in which the instructions are executed is important: we have to make sure that when an SVL script corresponding to a given component invokes EXP.OPEN to construct the component behaviour, all automaton synchronised by the \texttt{.exp} file have already been built.
Hence, the construction should start from components at the lowest levels of hierarchy, and the \texttt{.svl} file corresponding to the root component should be invoked in the last step.
\end{enumerate}

\textbf{Integration} Finally, integration modules are used to integrate VerCors in Eclipse. The  \textbf{VCEWizard} plug-in implements a wizard creating a VerCors project with the Component, UML and VCETypes model files and one diagram illustrating each model. The user can then add other models and diagrams. The \textbf{VCE Features} module makes VerCors installation/update accessible via the standard Eclipse plug-in installation/update wizard.  