\section{The BIP Component Framework}

\begin{figure}
     \centering
     \includegraphics[width=9cm]{drawings/BIP-design-flow.jpg}
     \caption{The BIP design flow \cite{BBB+11e}}
     \label{fig:bip-design-flow}
 \end{figure}
 
 
%what is bip
BIP (Behavior Interaction Priority) \cite{BBB+11e} is a framework allowing rigorous model-based design and programming of complex hierarchical component-based systems. BIP is supported by a set of tools for reverse engineering various programming languages into BIP language, a dedicated model-checker and an executable code generator. In this section we first make an overview of the BIP formalism and then present the tools for BIP systems development.

%about BIP and representation
\paragraph{The BIP component model.} In BIP \cite{Basu:2006:MHR:1158333.1158344}, a software system is designed as a composition of components connected by possible interactions. The interactions are characterised by priorities which are used for resolving conflicts and defining the scheduling policies. 
%There exist two types of components in BIP: \textit{atomic} (the simplest components) and \textit{compound} components composed of a set of components, interactions and priorities. The behavior of an atomic component is specified as a finite automata or a Petri net \TODO{REF} extended with data and ports. 
The components expose ports which are used for the communication with the other components. 

The interactions in BIP are structured in \textit{connectors} characterised by a set of connected port types and the data variables that can be transferred between the ports. As opposed to the GCM bindings, connectors may be hierarchical, i.e. a connector itself can expose a single port that may be bound to another connector. All the communications done through the connectors are synchronous. An interaction that can be performed via a connector is described by a subset of the connector's ports and can be additionally restricted by a guard expression.

There are two types of components in BIP: \textit{atomic} and \textit{compound} ones. The atomic components are simple entities similar to the GCM primitive components. They expose a set of ports and perform some behaviour modelled as a Petri net or a labelled transition system. A compound component is characterised by a set of contained subcomponents, connectors, priorities and a set of exposed ports that define an interface of the component. The ports exposed by a compound can belong to the connectors or components inside the compound.   

%rigorous design
\paragraph{The tools.} Figure \ref{fig:bip-design-flow} illustrates the workflow of a BIP-based application development. First, a software application is translated from the actual code into a BIP model. The framework supports full or partial source to source translation from various input languages such as C+XML, MATLAB/Simulink \cite{STS+10}, Lustre and model-based formalisms such as AADL \cite{CRB+08}. Alternatively. the user can specify the system directly in BIP textual language or using the EMF framework. In addition, the user should provide the specification of the hardware target platform and a mapping of the atomic components to the processing units. A BIP model of the system is derived based on this information and it takes into account the hardware architecture constraints. Then, the obtained models can be used for verification, simulation and generation of the software code. 

\TODO{Explain better}
The verification of deadlock-freefom and safety properties of BIP-based applications is done by the D-Finder tool \cite{BBN+09}.  The  verification method of D-Finder takes a BIP program constructs a predicate characterising the deadlock states. Then, it computes a local invariant for each atomic component in the system and an invariant on the  interactions of components. Finally, the tool checks satisfiability of the conjunction of the obtained invariants in order to prove the deadlock-freedom. 
%
%and for each atomic component 
%
%The  verification method of D-Finder is based on computing component invariants. D-Finder checks static correctness of atomic components and then performs compositional verification of the entire BIP system.  Thanks to the incremental verification technique, D-Finder is able to check system properties each time a new interaction is added to the designed application. The experimental results demonstrated in \cite{BBL+14} show that D-Finder is able to verify safety and deadlock-freedom properties of a component-based system.}

The drawback of D-Finder is that it does not take into account the data transfer on the interactions. A solution to this issue was proposed in~\cite{Bliudze2015} where the authors investigate an approach for the verification of the safety property of BIP systems with infinite state-space and data transfer. The authors encode a BIP program into as a symbolic transition system and give it as an input to the nuXmv~\cite{Cavada:2014:NSM:2735050.2735080} model-checker.


A completely different verification technique applied to a BIP system in \cite{FJN+15} is based on the runtime verification approach. For a given component-based system and a property to be verified, the authors synthesize a monitoring BIP component \textit{M} and modify the given atomic components so that they communicate with \textit{M}. \textit{M} observes the information provided by the other components and emits verdicts regarding the property violation. The authors provide not only formalisation of the approach, but also prove the equivalence of the behaviour of the initial and monitored systems. 

Finally, from a BIP model the user can generate C++ code for several execution platforms. There are two main compilation flows offered by the framework: engine-based compilation for non-distributed systems (single-, multi-threaded and real-time implementations) and generation of so-called Send/Receive BIP models for distributed implementation. In the second case, the synchronous communications are transformed into asynchronous message-passing as explained in \cite{BBQ11}.
% In the first case, three engines are provided for the execution of BIP systems: single-threaded, multi-threaded and real-time engines.     

\paragraph{Positioning.} BIP is a powerful framework offering variety of tools for distributed systems modelling, verification and executable code generation. The clear advantages of the BIP framework compared to VerCors is the ability to model real-time systems, the notion of priorities, the translation from the source code to the BIP models. Moreover, BIP allows expressing complex synchronisation patterns thanks to the hierarchical connectors and to the interactions which involve several ports. In this sense BIP is closer to pNets which describe a system on much lower level than BIP but also allow interactions (i.e. synchronisation vectors) to involve several entities. However, when specifying the interactions between GCM components in VerCors, the user can only define one-to-one, n-to-one, and one-to-n communications.  %  which is not possible in VerCors.
% However, if the user wants to model an application directly in BIP instead of translating the source code, he would have to learn all the notions related to the BIP formalism, whereas the modelling approach used in VerCors extensively relies on the UML formalism well-know by the programmers. 
 However, to the best of our knowledge, BIP does not allow modelling and verification of reconfigurable systems, does not support the separation of functional and non-functional concerns, and the parameter-parameter passing is not very well integrated with the analysis of component hierarchy. 
 


\label{sec:rw-bip}
%
%[2] V. Sfyrla, G. Tsiligiannis, I. Safaka, M. Bozga and J. Sifakis Compositional Translation of Simulink Models into Synchronous BIP. In Symposium on Industrial Embedded Systems SIES’10 Proceedings, WIP track.
%[3] Y. Chkouri, A. Robert, M. Bozga and J. Sifakis Translating AADL into BIP - Application to the Verification of Real-Time Systems In Model-Based Architecting and Construction of Embedded Systems ACES MB’08 Workshop Proceedings
%[5] A. Basu, M. Gallien, C. Lesire, T. Nguyen, S. Bensalem, F. Ingrand and J. Sifakis. Incremental Component-Based Construction and Verification of a Robotic System. In European Conference on Artificial Intelligence ECAI’08 Proceedings, volume 178 of FAIA, pages 631-635, IOS Press
