\section{Well-formed component architecture}
\label{sec:formalisation-well-formed}
Now, we can specify the well-formedness requirements.
First, we introduce the core rules and predicates used for the
definition of the well-formedness; then, we focus on the
non-functional aspects and on the collective communications.

\begin{table}
\begin{center}
  \begin{tabular}{| l | p {10 cm} | }
 \hline
\multicolumn{1}{|c|}{ Predicate name} & \multicolumn{1}{c|}{Predicate Definition} \\ 
 \hline
 UniqueCompNames &
$UniqueCompNames(\symb{Comp}_i^{i\in I} : \text{set of } Comp) \Leftrightarrow$ \newline
$\forall i, i' \in I. i \neq i' \Rightarrow \symb{CName}(\symb{Comp}_{i}) \neq \symb{CName}(\symb{Comp}_{i'})~~
$\\
 \hline
 
 UniqueItfNames &
$\symb{UniqueItfNames}(\Itf_i^{\,i\in I} :\text{set of } \Itf) \Leftrightarrow$ \newline
$\forall i,i'\in I.\, i\neq i'\Rightarrow \symb{Name}(\Itf_i)\neq \symb{Name}(\Itf_{i'})~~
$\\
 \hline

 BindingRoles &
$
\symb{BindingRoles}(b:\symb{Binding}) \Leftrightarrow$ \newline
\text{} $\symb{Role}(\symb{GetSrc}(b,\symb{Parent}(b))) = \textsf{C} ~\land$  \newline
\text{} $\symb{Role}(\symb{GetDst}(b,\symb{Parent}(b))) = \textsf{S}$ \\
 \hline

 BindingTypes &
$
\symb{BindingTypes}(b:\symb{Binding}) \Leftrightarrow $ \newline
\text{} $\symb{MSignature}(\symb{GetSrc}(b,\symb{Parent}(b))) \leq $ \newline
\text{} $\symb{MSignature}(\symb{GetDst}(b,\symb{Parent}(b))) $\\
 \hline
CardValidity &
$\symb{CardValidity}(\symb{Binding}_l^{l\in L} : \text{set of } \symb{Binding}) \Leftrightarrow$\newline
\text{} $\forall l,l' \in L.\,l \neq l'\Rightarrow$\newline
\text{}\qquad$\symb{GetSrc}(\symb{Binding}_l, \symb{Parent}(\symb{Binding}_l))\!\neq\!$\newline
\text{}\qquad$\symb{GetSrc}(\symb{Binding}_{l'}, \symb{Parent}(\symb{Binding}_{l'}))$\\
\hline
  \end{tabular}
\end{center}
\caption{Core Predicates}
\label{table-pred}
\end{table}

\subsection{Core}
 Table \ref{table-pred} formalises the auxiliary predicates which are used for the definition of the following  constraints:

\begin{itemize}
\item[$\bullet$]
Component naming constraint ($UniqueCompNames$): all the components at the same level of hierarchy must have
different names. This restriction is due the fact that components are
referenced by their name; typically, \symb{QName} (see Table~\ref{table-core}) can be of the form
$\textsf{CName}.\textsf{Name}$; and if two components had the same name
the functions \symb{GetSrc} and \symb{GetDst} would not return a
single deterministic result.
 The two contents of two different composite
components as well as two membranes are considered to be different
name-spaces. A membrane and a content are also different name-spaces
even if they belong to the same component.
%
\item[$\bullet$]
Interface naming constraint ($\symb{UniqueItfNames}$): all the interfaces of a component must have different
names. This constraint will be checked separately, for the external
interfaces of a component, and for the internal interfaces
 of a content to allow external and internal interfaces to have the same name. This constraint also ensures the determinacy of the
 functions \symb{GetSrc} and \symb{GetDst}.
%
\item[$\bullet$]
Role constraint ($BindingRoles$): a binding must go from a client interface to a server interface. 
The predicate uses a function $\symb{Role}(I)$ that returns $\textsf{C}$
(resp. $\textsf{S}$) if $I$
is a client (resp. server) interface.
\item[$\bullet$]
Typing constraint ($BindingTypes$): a binding must bind interfaces of compatible
  types. The compatibility of interfaces means that for each method of
  a client interface there must exist an adequate corresponding method in the
  server interface. In other words, if a client interface is
  connected to a server interface and it wants to call some method,
  then this method must actually exist on the server interface.
In general, a corresponding method does not need to have exactly the
same signature as the one required, but can use any sub-typing or
inheritance pre-order available in the modeling language. We denote
$\leq$ such an order between interface signatures.
\item[$\bullet$] 
Cardinality constraint, \symb{CardValidity}, ensures that a client interface
is bound to a single server one. In other words, there is not two bindings going from the same client interface.
\end{itemize}

We use the previous constraints to specify a well-formedness predicate,
denoted \symb{WF}. It is defined recursively on the component architecture,
namely on primitive components, on composite components, and on contents. 
 

A GCM primitive component is well-formed if all its interfaces have distinct names and its membrane is well-formed.
%
\begin{eqnarray}
&&\text{Let} ~prim:Prim = \textsf{CName} <\symb{SItf}_i^{i\in I}, \symb{CItf}_j^{j\in J}, M_k^{k\in K}, \Membrane>; \\ \nonumber
&&WF(prim) \Leftrightarrow   
\symb{UniqueItfNames}(\symb{SItf}_i^{i\in I} \cup \symb{CItf}_j^{j\in J}) \land 
WF(\Membrane)
\end{eqnarray}
%
%
%

A GCM composite component is well-formed if  all its external interfaces have distinct names, and its content and its membrane are well-formed.
%
\begin{eqnarray}
&&\text{Let} ~compos:Compos = \textsf{CName} <\symb{SItf}_i^{i\in I}, \symb{CItf}_j^{j\in J},
\Membrane, \Content>; \\ \nonumber
&&WF(compos) 	\Leftrightarrow   
\symb{UniqueItfNames}(\symb{SItf}_i^{i\in I}\cup \symb{CItf}_j^{j\in J}) \land 
WF(\Membrane) 	\land WF(\Content) 	
\end{eqnarray}
%
The content of a GCM component is well-formed if all its interfaces have distinct names, all its sub-components have distinct names, all its bindings have a valid cardinality,  all its sub-components are well-formed, the role, type, and nature constraints are respected for all its sub-bindings. The nature constraint for the bindings relies on The $BindingNature$ predicate. It is discussed  in Section~\ref{sec:NonFunctional} because it is related to the non-functional aspects.
%  
%
%
\begin{eqnarray}
&&\text{Let} ~cont : Cont = <\symb{SItf}_i^{i\in I}, \symb{CItf}_j^{j\in J}, \symb{Comp}_k^{k\in K},  \symb{Binding}_l^{l\in L}>; \\ \nonumber
&&WF(cont) \Leftrightarrow  
\left\{\begin{array}{l}
\symb{UniqueItfNames}(\symb{SItf}_i^{i\in I} \cup \symb{CItf}_j^{j\in J}) \land \\
UniqueCompNames(\symb{Comp}_k^{k\in K})  \land \\
\symb{CardValidity}(\symb{Binding}_l^{l\in L}) \land 
\forall k \in K.  WF(\symb{Comp}_k) \land \\
\forall B\in \symb{Binding}_l^{l\in L}. BindingRoles(B) \land \\
 BindingTypes(B) \land BindingNature(B) 
\end{array}\right.
\end{eqnarray}
%
%
%
The well-formedness of a membrane is only significant for the non-functional aspect, it is defined below.
\subsection{Non-functional aspects}
\label{sec:NonFunctional}
\begin{table}
\begin{center}
  \begin{tabular}{|l | p {8 cm} |}
 \hline
\multicolumn{1}{|c|}{Predicate name} & \multicolumn{1}{c|}{Predicate Definition} \\
 \hline

 UniqueNamesAndRoles &
$ \symb{UniqueNamesAndRoles}(\Itf_i^{\,i\in I} : \symb{set} ~\symb{of}~ \Itf) \Leftrightarrow $ \newline
\text{} $\forall i,i'\in I.(i\neq i'\land \symb{Name}(\Itf_i)= $ \newline
\text{} $\symb{Name}(\Itf_{i'})) \Rightarrow \symb{Role}(\Itf_i) \neq \symb{Role}(\Itf_{i'})$
\\ \hline

BindingNature &
$BindingNature(b:Binding) \Leftrightarrow $ \newline
\text{} $\ControlLevel(GetSrc(b, Parent(b))) = $ \newline
\text{} $\ControlLevel(GetDst(b, Parent(b))) = 1 \lor $ \newline
\text{} $(\ControlLevel(GetSrc(b, Parent(b))) > 1 \land $ \newline
\text{} $\ControlLevel(GetDst(b, Parent(b))) > 1)$
\\ \hline

  \end{tabular}
\end{center}
\caption{Non-functional predicates}
\label{table-pred_nf}
\end{table}
In this section we define the static semantic constraints ensuring
safe composition of the non-functional part of a component-based
application. The correctness of a membrane relies on the two predicates defined in Table~\ref{table-pred_nf}:

\begin{itemize}
\item[$\bullet$]
Interface naming constraint ($UniqueNamesAndRoles$): if there are two
interfaces with the same name in a membrane, then they must have
different roles. We recall here that the interfaces of a membrane are not declared explicitly but computed as the symmetry of the interfaces belonging to the parent component and its content. This is as slight relaxation from the
$\symb{UniqueItfNames}$ rule of the general case: we generally want to allow
corresponding external/internal interfaces pairs of opposite role to
have the same name. This also ensures
compatibility with the  original 
Fractal model, where internal interfaces were implicitly defined as
the  symmetric
of external ones.


\item[$\bullet$]
Binding nature constraint($BindingNature$) is a rule imposing the
separation of concerns between functional and non-functional
aspects. We want the functional interfaces to be bound together (only
functional requests will be going through these), and non-functional
interfaces to be connected together as a separate aspect. This is simple to impose in the content of a composite components, but a little trickier in the membrane because of the specific status of interceptors.

The solution is to qualify as functional all the components in a
content and all the interceptors, while all the other components in the
membrane are non-functional. As mentioned earlier, the interfaces are declared functional
or non-functional. From this we compute for each interface a
control level ranging from 1 to 3, where 1 means functional; 2 and 3 mean non-functional. The two levels are needed because a non-functional component inside a membrane can have both functional and non-functional interfaces. However, the computed control level of any of them should indicate that an interface belongs to the non-functional aspect because the interface belongs to a non-functional component. Then, the external functional interfaces of non-functional components will have a control level 2 while their non-functional interfaces will have a control level 3. 
Then
the compatible interfaces are either both ``1'', or both greater than
``1''.
The $ControlLevel$ function is formally defined as:
\begin{eqnarray}
&&\ControlLevel(X:\symb{Comp}) ::=
\left\{\begin{array}{l} \nonumber
  2, ~if ~Parent(X, context):\Membrane \land \\ ~~~\lnot IsInterc(X, Parent(X))) \\
  1, ~else
\end{array}\right.
\nonumber\\
%
&&\ControlLevel(X:\symb{Cont}~|~\symb{Membr}) ::= 1
\nonumber\\
%
&&\ControlLevel(X:\Itf) ::=
\left\{\begin{array}{l}
  \ControlLevel(Parent(X)), ~if ~\Nature(X) = \textsf{F}
 \\ \ControlLevel(Parent(X)) + 1, ~if ~\Nature(X) = \textsf{NF}
\end{array}\right.
\end{eqnarray}

This constraint on the nature of bindings was already mentioned in
\cite{naoumenko10}, we propose here a formal definition that is simpler and more intuitive.


\end{itemize}
As the last step, we define the well-formedness predicate for a
membrane. A membrane is well-formed if all its sub-components have
distinct names, the naming constraint is respected for its interfaces,
all its sub-components are well-formed, all its bindings have a
valid cardinality, and the role, type, nature constraints are respected for all its sub-bindings,  
\begin{eqnarray}
&&\text{Let} ~membr:Membr = <\symb{Comp}_k^{k\in K},  \symb{Binding}_l^{l\in L}>; \\ \nonumber
&&WF(membr)  \Leftrightarrow\left\{\begin{array}{l}
UniqueCompNames(\symb{Comp}_k^{k\in K})  \land \\
UniqueNamesAndRoles(\symb{Itf}(membr)) \land \\
\forall k \in K.  WF(\symb{Comp}_k) \land CardValidity(\symb{Binding}_l^{l\in L}) \land \\
\forall B\in \symb{Binding}_l^{l\in L}. BindingRoles(B) \land \\
BindingTypes(B) \land BindingNature(B) 
\end{array}\right.
\end{eqnarray}

%\TODO{maybe, move
%This section presented an extension to the definition of well-formed
%components defined in previous works
%\cite{HKK:FMCO09,gaspar:HLPP13}. More precisely, most of the definitions given in
%this section allow us to formalize the notion of well-formed
%non-functional features and well-formed interceptors, that have never
%been formalized before. The new definition of well-formed components
%is the basis for the correct composition of distributed components
%with  clear separation of
%concerns.
%} 

\subsection{Collective communications}
\label{subsec:formalisation-collective}
One of the crucial features of GCM is to enable one-to-many and
many-to-one communications through specific interfaces, namely
gathercast and multicast.

In order to specify such communications let us add a Cardinality
($Card$) field in the specification of the GCM interfaces. The cardinality
can be \emph{singleton}, \emph{multicast} or \emph{gathercast}.
We recall here that a multicast can send requests to several target interfaces at the same time; a gathercast  can be plugged to multiple client interfaces. 
%In Figure \ref{fig:gcm} all interfaces are singleton, except the
%multicast interface \emph{M1} of the \emph{Master} component.

These new interfaces modify the definition of cardinality validity.
In particular, the multicast interface allows two bindings to originate
from the same client interface. The \symb{CardValidity} is modified
as follows:
%  Particularly, if there exist two bindings that go from the same interface, then the cardinality of such interface is $multicast$. The constraint uses $CardValidity$ defined below.
\begin{eqnarray}
\symb{CardValidity}(\symb{Binding}_l^{l\in L} : \text{set of } Binding) \Leftrightarrow 
\forall \symb{itf}: \Itf.\,\forall l,l' \in L.l \neq l' \\ \nonumber 
\quad (\symb{itf}\!=  
\!GetSrc(Binding_l, Parent(Binding_l))\!= \\ \nonumber
\!GetSrc(Binding_{l'}, Parent(Binding_{l'})  
\Rightarrow Card(\symb{itf}) \!=\! multicast)
\end{eqnarray}

The intended semantics  is that an invocation emitted by a multicast
interface is sent to all the server interfaces bound to it.
Gathercast interface on the contrary synchronises several calls
arriving at the same server interface,
they do not entail any structural constraint. Indeed, multicast and gathercast interfaces were designed for sending or synchronising several invocations correspondingly. From an architectural point of view, there is no difference between a gathercast and a singleton server interface because both of them can be plugged to several 
client interfaces.
 An interceptor chain
should not contain any multicast functional interface because it
should transmit invocations in a one-to-one manner.

\subsection{Additional rules}
\label{subsec:formalisation-additional}

In order to facilitate the further analysis and in particular the generation of the behavioural models, we add two additional requirements to the well-formed components. First, we require that no binding has the same component as source and destination: there is no binding looping back directly to the same component. This condition is checked by the predicate below:
\begin{eqnarray}
\symb{NoLoopBinding}(b:\symb{Binding}) \Leftrightarrow \\ \nonumber
Parent(\symb{GetSrc}(b,\symb{Parent}(b))) \neq Parent(\symb{GetDst}(b,\symb{Parent}(b))
\end{eqnarray}

The second additional rule states that no two bindings going from the same multicast interface reach the same target component; it is expressed by the predicate below:
\begin{eqnarray}
\symb{NoEqualTarget(b,b':\symb{Binding})} \Leftrightarrow 
\symb{GetSrc}(b, \symb{Parent}(b)) =  \symb{GetSrc}(b', \symb{Parent}(b'))  \land \\ \nonumber
\symb{Parent}(\symb{GetDst}(b, \symb{Parent}(b))) = \textsf{CName.Name} \Rightarrow  \\ \nonumber
 \symb{Parent}(\symb{GetDst}(b, \symb{Parent}(b))) \neq \symb{Parent}(\symb{GetDst}(b', \symb{Parent}(b)))
\end{eqnarray}

The two additional predicates should be checked for each binding in a container (in a content or in a membrane). This validation is not required by the GCM model but it simplifies the rules for the construction of the behavioural models presented in Section~\ref{sec:advanced-multicast}.