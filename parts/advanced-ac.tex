\section{Component attributes and attribute controllers}
\label{sec:advanced-ac}

A GCM primitive component can have attributes which are used by its methods.
% which value can be modified using so-called attribute controllers.
% In this section we recall how component attributes should be specified graphically in VerCors. Then, we discuss how  attribute controllers are encoded in pNets, and how their specification is translated into an input for CADP. Finally, we explain the generation of the executable code needed to include attributes in the specification of a primitive component.
In fact, we should distinguish between the two aspects: the statefull components (i.e. the attributes of a component which can be accessed and modified by its methods) and the get/set access to the attributes of a component from outside. For the first aspect, we discuss in this section how the attributes of a primitive can be designed graphically, accessed from its methods, translated into pNets, and generated in the implementation code. However, we have not yet decided which graphical notations should be used for the second aspect. Hence, if the user would like to make an attribute accessible from outside of a component, he must explicitly define a server method which implements the necessary access. In the generated code we make the attributes accessible from outside of a component because this is needed for the GCM/ProActive factory during the deployment.   

\subsection{Graphical specification} 

As it was discussed in Section~\ref{sec:vce-graphical-formalism} the attributes of a primitive component should be declared in the UML class attached to the component. Figure~\ref{fig:ac-graphical} illustrates a simplified version of a leader election algorithm participant from our use-case discussed in Chapter~\ref{chap:vce-overview}. The component has only one attribute - \texttt{max}.
In addition, for each attribute of a class, the user should declare so-called set- and get-methods which are used to modify and access the value (\texttt{set\_max()} and \texttt{get\_max} in our example). The user should not define any state machine for the behaviour of these methods. The default initial value of an attribute can be provided in the class specification (it is equal to 1 in our case). If the user wants to define a specific value for a particular component, then it should be given in a dedicated green area (the initial value is equal to 2 in our example). 

\begin{figure}
     \centering
     \includegraphics[width=10cm]{drawings/ac-graphical.pdf}
     \caption{Graphical specification of a component attribute}
     \label{fig:ac-graphical}
 \end{figure}

\subsection{From application design to pNets}


\paragraph{Global structure.} For each pair of a get- and a set-methods defined for an attribute, we construct a  pLTS called \textit{attribute controller} which stores the value of the attribute and provides actions to modify and to access it. The pLTSs should be included in the pNet of the primitive together  with the synchronisation vectors modelling access to the attributes.
%%In order to include the attribute controllers in the pNet of a primitive, 
%We will extend the pNet of a primitive with an additional set of sub-nets modelling the behaviour of the attribute controllers, and with several new synchronisation vectors. We will rely on the $\oplus$ operator with the following definition.

The behaviour of a primitive component with attribute controllers is formalised below. We extend the definition of a primitive with a set \symb{Attributes} which includes its attributes.

\begin{small}
  \[
{ {\begin{array}{ll}
       \llbracket\CName<\!{\SItf}_i^{\,i\in I},{\CItf}_j^{\,j\in J}, M_k^{k\in
         K}, Attributes\!>\rrbracket^{\symb{AC}}= \\ \qquad\qquad   
  \llbracket \CName<\!{\SItf}_i^{\,i\in
         I},{\CItf}_j^{\,j\in J}, M_k^{k\in K}, Attributes\!>\rrbracket
       \begin{array}[t]{l}
         \oplus         \mylangle\mathcal{AC}, \symb{SV}_{AC}\myrangle
       \end{array}
     \end{array} 
} }
  \]
  
  

\end{small}

%\llbracket \CName<\set{\SItf},{\CItf}_j^{\,j\in J}, \symb{Impl}>\rrbracket

%$  \llbracket\text{ }\rrbracket_{\symb{ac}}$

%a_i\in\text{Attributes}(\symb{Comp}


\begin{mathpar}
\begin{small}

\text{Where $\mathcal{AC}$ is computed by the following rule: \hspace{20em}~} 

\inferrule{ a_i^{i\in AI}=Attributes(\symb{Comp})
}
{\mathcal{AC} =\PNfamily{\llbracket a_i \rrbracket_{\symb{ac}}^{n\in AI}} 
P
}
  \label{pagePrimsem}
\end{small}
\end{mathpar}
Here the function $\llbracket~\rrbracket_{\symb{ac}}$ returns the pLTS of an attribute controller. 

\paragraph{Attribute controller pLTS.} The behaviour of an attribute controller is encoded as a pLTS as illustrated in Figure~\ref{fig:ac-pnet}. This pLTS  stores a variable \texttt{max} which represents the modelled attribute, and provides actions to access its value (\symb{Call\_Get\_max()}, \symb{R\_Get\_max(max)}) and to modify it (\symb{Set\_max(?max)}).

\paragraph{Synchronisation vectors.} The pNet of a primitive component should include one attribute controller pLTS for each attribute. The pLTSs can be accessed from the server and local methods of the component; in order to enable such access, the pNet of the primitive is extended with the synchronisation vectors given in Table~\ref{tab:ac-sv}.  The vectors rely on an additional construct Attributes(\symb{Comp}) which provides a set of attribute names of the generated primitive where each attribute has a unique name. Rules~\Rref{AC}{ACcallGet}~and~\Rref{AC}{ACRget} synchronise the server methods and the attribute controllers when a server method modifies the value of an attribute. Rule~\Rref{AC}{ACset} allows server methods to access the attribute values. As in the previous definitions, the rules include only pLTSs of the server methods, but the local methods are synchronised with the attribute controllers in the same way. 

In order to make an attribute accessible from outside of a primitive, we would need to extend the queue and the body with the actions for the treatment of the requests to the attribute controller, and to synchronise them as for any other server method.

\begin{figure}
     \centering
     \includegraphics[width=9cm]{drawings/vce-pnets-ac.pdf}
     \caption{An attribute controller pLTS}
     \label{fig:ac-pnet}
 \end{figure}

%  \begin{table}
%  \captionsetup{justification=centering}
%\caption[.]{\TODO{fix the representation} Attribute controller synchronisation vectors for primitive components. The 
%synchronised
% sub-pNets are:
%  \makebox{$\mylangle\Queue,\symb{Body},\symb{ServiceMethods},\symb{AttributeControllers}, \symb{ProxyManagers},\symb{Proxies}\myrangle$}
%      }\label{tab:ac-sv}
%{\begin{small}
%\begin{mathpar}
%  \inferrule{ l\in L\\a_i\in\text{Attributes}(\symb{Comp})} 
%{ {\begin{array}[t]{@{}l@{}l@{\quad}r@{}}\{ &
%        \langle -, -, l\mapsto Call\_Get\_ac_i, Call\_Get\_a_i, -, - \rangle \to 
%        Call\_Get\_a_i,&\rulelabel{ACcallGet} \\
%        &\langle -, -, l\mapsto \symb{R\_Call\_Get}\_a_i(\symb{val}), \symb{R\_Call\_Get}\_a_i(\symb{val}), -, - \rangle \to 
%        \symb{R\_Call\_Get}\_a_i(\symb{val})),&\rulelabel{ACRget} \\
%         &\langle -, -, l\mapsto \symb{Set}\_a_i(\symb{val}), \symb{Set}\_a_i(\symb{val}), -, - \rangle \to 
%        \symb{Set}\_a_i(\symb{val})),&\rulelabel{ACset} \\       
%         \end{array}}
%  }~~\rulelabelOne{AC}
%%\rulecounterreset
%\end{mathpar}
%\end{small}}
%\end{table}

  \begin{table}
  \captionsetup{justification=centering}
\caption[.]{Attribute controller synchronisation vectors for primitive components. The 
synchronised
 sub-pNets are:
  \makebox{$\mylangle\Queue,\symb{Body},\symb{ServiceMethods}, \symb{ProxyManagers},\symb{Proxies}, \symb{AttributeControllers}\myrangle$}
      }\label{tab:ac-sv}
{\begin{small}
\begin{mathpar}
\rulecounterreset
  \inferrule{ m_l^{l\in L}=\MethLabel(\set{\SItf})\\a_i^{i\in AI}=Attributes(\symb{Comp})} 
{ {\begin{array}[t]{@{}l@{}l@{\quad}r@{}}\{ &
        \langle -, -, l\mapsto Call\_Get\_ac_i, -, -, i\mapsto Call\_Get\_a_i \rangle \to 
        \underline{Call\_Get\_a_i},&\rulelabel{ACcallGet} \\
        &\langle -, -, l\mapsto \symb{R\_Call\_Get}\_a_i(\symb{val}), -, -, i\mapsto \symb{R\_Call\_Get}\_a_i(\symb{val}) \rangle \to 
        \underline{\symb{R\_Call\_Get}\_a_i(\symb{val}))},&\rulelabel{ACRget} \\
         &\langle -, -, l\mapsto \symb{Call\_Set}\_a_i(\symb{val}), -, -, i\mapsto \symb{Call\_Set}\_a_i(\symb{val}) \rangle \to 
        \underline{\symb{Call\_Set}\_a_i(\symb{val})})\}&\rulelabel{ACset} \\    
        &\hspace{\fill}\subseteq \symb{SV}_{AC}(m_l^{l\in L})   
         \end{array}}
  }~~\rulelabelOne{AC}
%\rulecounterreset
\end{mathpar}
\end{small}}
\end{table}


\subsection{Implementing pNet generation and integration with CADP}

VerCors generates one attribute controller pLTS for each attribute of a primitive component and synchronises it with the pLTSs encoding the server and local methods of the component. The construction requires several steps in addition to the pNet generation process discussed in Section~\ref{subsec:pnets-generation}. First, at the pre-processing phase, VerCors extracts a set of attributes for each generated primitive; and for each state machine encoding the behaviour of a method, the pre-processor extracts the set of attributes accessed by the state machine.
 Second, while constructing a pNet encoding the behaviour of a primitive, for each attribute belonging to the component, the platform additionally invokes an attribute controller builder which creates the corresponding pLTS. Finally, VerCors generates the synchronisation vectors following the rules from Table~\ref{tab:ac-sv}. Thanks to the analysis done at the pre-processing phase, the synchronisation between methods and attribute controllers which do not communicate is not generated.
 
%The pLTSs of attribute controllers are translated into Fiacre just like any other pLTSs. 

\subsection{Code generation}

Listing~\ref{list:ac-java} provides an example of the generated Java class for \texttt{Class0} from Figure~\ref{fig:ac-graphical}. In order to include attributes in the generated code of a GCM/ProActive primitive component, we have to produce several additional constructs. 

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={A Java class implementing the behaviour of a primitive component with an attribute}, label = {list:ac-java}, basicstyle=\scriptsize]
public class Class0 implements Class0AC ... {
	public int max = 1;
	
	public  void set_max(int value) {
		this.max = value;
	}
	public  int get_max() {
		return this.max;
	}
}	
\end{lstlisting}

First, we include a field corresponding to each attribute in the Java class (line~2). If its default value is specified in the UML class, it is also translated into Java code. 

GCM/ProActive provides a mechanism to set a value of an attribute which is specific to a particular component (the value that is graphically defined in the green area included in the specification of a primitive). For this, we need to include the value in the ADL description of the component as shown in Listing~\ref{list:ac-adl}. The GCM/ProActive factory reads the value of each attribute in the ADL file and assigns it during the component construction.

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={An ADL specification of a component attribute}, label = {list:ac-adl}, basicstyle=\scriptsize]
    <component name="Comp2">
     ...
        <attributes signature="example.interfaces.Class0AC">
            <attribute name="_max" value="2"/>
            ... other attributes
        </attributes>
        <controller desc="primitive"/>
    </component>
\end{lstlisting}

Line~3 in in this ADL description refers to the Java interface \texttt{Class0AC} which methods will be invoked by the factory in order to set the values of the component attributes. The interface includes the signatures of the set- and get-methods for the attributes included in the ADL description of the component. The interface should be implemented by the class modelling the behaviour of the component (\texttt{Class0} in our example).
Such kind of interfaces are also generated by VerCors. 

Finally, VerCors automatically produces the code of the set- and get-methods for each attribute (see lines 4-9 in Listing~\ref{list:ac-java}).
