\section{Discussion and Related work}
\label{sec:formalisation-rw}

In this chapter we have formalised the architecture of GCM components including the non-functional aspect and interceptors, we have formalised the specification of component well-formedness predicates which ensure that the components are well-encapsulated, the communications are deterministic, and the business-logic of an application is separated from the control part. We fully implement the automatic check of the well-formedness  constraints in VerCors. 

Even though in this chapter we mainly consider the GCM components, we believe that our formalisation is quite general and the other component models can also benefit from it. For example,  
SOFA 2.0~\cite{Malohlava2013101} which will be discussed in details in Section~\ref{sec:rw-sofa} has a structure very similar to the GCM, it features hierarchical composition and
componentised component control. Interestingly, similarly to our approach,
one of the objectives of SOFA is also to provide formal verification
tools for distributed component systems. 
 It would be easy to
adapt our formal specification to SOFA 2.0, more precisely:
\textit{Micro-controllers} are components dedicated to non-functional
  concerns, they reside in the membrane and are not hierarchical: to
  take them into account, we should \emph{restrict the correctness rules for
  the membrane to only allow  primitive components in the
  membrane}. Also, the SOFA~2  
 \textit{delegation chains} are chains of interceptors, following
  exactly the \emph{rules defined  in Section~\ref{sec:formalisation-interceptors}}. 
% a re-configurable structuration
% of controller components into micro-controller; of componentized membrane to the case
% of SOFA 2.0 which is more restrictive than GCM concerning the
% structure of the membrane. 
Another example could be AOKell~\cite{seinturier06aokell} -- an extension
of Fractal specifically for componentising membranes, it is
interesting to note that the authors define a notion of control level,
which is quite similar to the \emph{ControlLevel} function used in our
paper. In this case again, our approach could be used to verify the
correct composition of AOKell-based components, and ensure the safe
design of AOKell component systems. 

% with the non-functional aspect, interceptors, a few other constraints; in addition, we implement their automatic validation.

%As it was mentioned earlier, the component architecture formalisation and the well-formedness definitions given in this chapter refine and extend the ones introduced in \cite{ameurboulifa:hal-00761073}. 
The two previous works closest to ours are the formalisation of Fractal in
Alloy~\cite{MERLE:2008:INRIA-00338987:1}, and the formalisation of GCM in
Isabelle/HOL~\cite{HKK:FMCO09}. The first framework focuses on
structural aspects in order to prove the realisability of component
systems. Except from the core elements (i.e. components, interfaces and bindings), the authors formalise the standard Fractal controllers (e.g. Lifecycle controller, Binding controller) and the components factory. One of the key goals of the work is to make clear a number of aspects that were left ambiguous in the informal Fractal specification. For example, the authors give a precise definition of the subtyping relation for the interfaces. 

The second work aims at providing lemmas and theorems to
reason on component models at a meta-level, and prove generic
properties of the component model. In addition to the core elements of a component-based architecture, the authors take into consideration the evolution of a component state at runtime and the semantics of communications based on request/reply by futures mechanism. The authors give an example of how a system reconfiguration can be formally specified in Isabelle. However, none of the two discussed formalisations included
the notion of non-functional components, many-to-many interfaces, or
interceptors. The formal specification of component correctness
defined in this thesis could be used to extend the expressiveness of
the component model in  the Alloy and the Isabelle frameworks.

Another similar work was provided in~\cite{gaspar:HLPP13} where the authors investigated a
language for generating correct-by-construction component systems, and
reconfiguring them safely. The core contribution is a framework Mefresa which is based on the Coq~ \cite{DBLP:journals/corr/abs-cs-0603118} proof assistant and aims at automatic reasoning on component-based application architecture. The authors show how a GCM-based system structure can be encoded in Coq and how its correctness can be checked with respect to a set of predefined properties. An interesting aspect of the work is an approach to encode and validate scenarios for the dynamic application reconfiguration.  
Similarly to the cases above, this study
does not deal
with the structure of the membrane. It could
be extended to the enhanced component structure presented here. Also, it could benefit from using the VerCors graphical designer as a front-end, as encoding manually components structure in Coq might be a bit tedious for a non-experienced user.

Finally, we assume the conceptual model to be correct with respect to the static correctness predicates while translating it into an input for the model-checker and generating the implementation code. Both transformation processes will be presented in the following chapter. In particular, we generate one pNet encoding the behaviour of one component and we construct synchronisation vectors to express the communications between components. The unique naming property is crucial for the synchronisation, because the synchronised pNets are identified by the full path to the encoded components, and if two components in a container have the same name, their pNets will also have the same name which might lead to synchronising the wrong entities. The unique naming property is also important for the generated program execution, because the GCM/ProActive factory does not allow constructing two components with the same name in one container. 
It is also crucial to make sure that the functional part of a modelled application is properly separated from the non-functional elements before constructing the pNets. The reason is that when building the behaviour model, we pre-process separately the functional and non-functional interfaces and components, hence, any violation of the binding nature constraint can lead to unpredictable results of pNet construction. 
%the binding typing constraint is not violated because we synchronise the communicating components on each client method invocation, and if the target component is not able to serve a client request, the synchronisation   
