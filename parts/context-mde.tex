\section{Model-Driven Engineering}

Model-driven engineering (MDE)\cite{RodriguesdaSilva2015139} is a software development paradigm that allows constructing an abstract model of an application which can be analysed at the design stage, transformed into the actual implementation, and used as a documentation and for automatic testing. Models can describe a system at different levels of abstraction and from different viewpoints such as hardware/software requirements, application architecture or behaviour.

The pivotal concept of MDE - a \textit{model} - is often defined as an abstraction of a system under study. Another key notion - a \textit{meta-model} - specifies how the model should be described, i.e. the building blocks of a model and the relations between them. For example, if we would like to create a model of a family, the meta-model would specify that the model consists of family members who have various properties (for example, name and age) and can be related as children, spouses, cousins, etc. Models are often associated with \textit{views} that are their graphical representations. 

Constructing a conceptual model before starting the implementation is good because it provides a clear view on system requirements and can be used as a source of documentation, but there are other advantages of the MDE approach. Multiple analysis techniques can be applied to a model in order to evaluate the quality of the future application and detect errors at the design stage. The static correctness of a model can be ensured by static analysis techniques; formal methods such as model-checking can be applied to check functional properties. Also, conceptual models can be used for an application performance predication at the design stage. Additionally model-to-model and model-to-text transformation techniques can be used to automatically transform a model into another model or textual representation that could be an executable code. 	 

Models are specified in modelling languages. The \textbf{domain-specific languages} (DSLs) are used to design systems for a particular domain, and there exist hundreds of DSLs in the world. However, sometimes it is a good idea to design a software in a language that will be easily understood by a wide audience, and this is what is provided by the Unified Modelling Language (UML).

\subsection{Unified Modelling Language}

UML \cite{omg2011umls} is a general-purpose modelling language created and supported by the Object Management Group (OMG). The language is mainly dedicated to the design of object-oriented applications and widely used in industry and academia. UML allows describing a system at different levels of abstraction as a set of diagrams. The examples of diagrams could be: use-case diagrams illustrating how the user should use the application, sequence diagrams that depict the interactions between entities, and activity diagrams that describe step-by-step behaviour. 

The two UML diagrams used in this work are the class diagrams and the state machine diagrams. The \textbf{class} diagrams define program classes and interfaces with their attributes and method signatures, and relations between them that include inheritance, aggregation and composition. Figure \ref{fig:context-class} illustrates an example of a class diagram with class \texttt{A} that implements an interface \texttt{Itf}, owns two attributes \texttt{at1} and \texttt{at2}, and defines a method \texttt{mthd}. The behaviour of an entity (class, method, or even a system as a whole) can be described with possibly hierarchical \textbf{state machine} diagrams (also known as state charts). A state machine diagram (we will call them state machines for short) illustrates the sequence of events that an entity goes through and their impact on the entity state. An example is illustrated in Figure \ref{fig:context-sm}. The model is composed of different type of states, e.g. choice (\texttt{S2}), initial (\texttt{S1}), fork, and join states, connected by transitions that describe possibly guarded events. States can be assembled in regions. In this work we will not use hierarchical state machines and we will have one single region per state machine. 
%UML allows describing system both at the high-level and with different  

 \begin{figure}
     \centering
     \includegraphics[width=6cm]{drawings/class-example.pdf}
     \caption{UML class diagram}
     \label{fig:context-class}
 \end{figure}

\begin{figure}
     \centering
     \includegraphics[width=8cm]{drawings/sm-example.pdf}
     \caption{Uml state machine diagram}
     \label{fig:context-sm}
 \end{figure}
 
\subsection{Eclipse Modeling Framework}
The Eclipse development environment~\cite{Eclipse} provides a rich ecosystem for model-driven engineering; its core technology is the \textbf{Eclipse Modeling Framework} (EMF) \cite{EMF} which includes the following features. The first element is a set of tools for constructing a meta-model (so-called \textit{ecore} model). The construction is supported by a tree-like editor and a static validator. From the ecore model, EMF automatically generates the Java API of the designed model, an editor that represents a model in a tree-like viewer (we will call it an \textit{EMF-editor} in the next chapters) and a standard Eclipse property editor for the parameters of the modelled elements. The models are stored in an XML-based format. Figure \ref{fig:context-emf} illustrates an example of an ecore meta-model of a family on the left and the corresponding generated EMF editor with a model of a family and properties editor on the right. 

A graphical editor for an EMF-based model can be implemented with the help of the Graphical Modeling Framework (GMF) \cite{GMF}. Both EMF and GMF editors are distributed as Eclipse plug-ins, i.e. a software that can be installed on top of Eclipse.
For example, a UML ecore and dedicated editors are implemented as an Eclipse plug-in \cite{Eclipse-UML}. 

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{drawings/context-ecore.pdf}
  \caption{Ecore model}
  \label{fig:context-ecore}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{drawings/context-model.pdf}
  \caption{EMF-based editor}
  \label{fig:context-model}
\end{subfigure}
\caption{EMF example}
\label{fig:context-emf}
\end{figure}

\subsection{Obeo Designer}
\label{subsec:context-mde-od}
EMF and GMF provide a large variety of instruments for building domain-specific model editors. We will not get into details here, but one who has an experience of working with GMF knows that the technology is powerful, but using it for constructing and maintaining a large graphical designer requires significant effort. A platform that allows one to implement easily a GMF-based modeller "without knowledge of GMF" is Obeo Designer (it has an open-source version Sirius also known as "Obeo Designer Community") \cite{OD, OD-paper} that was developed by the Obeo company. The framework is built on top of EMF and GMF and provides techniques for specifying a domain-specific language, various model representations such as a diagram, a table or a matrix, and tools to edit the model. 
%In \cite{OD-paper} the authors justify the usability of the framework. 
We would like to highlight the following features of Obeo Designer: 

\begin{itemize}
\item[$\bullet$]
a technique to extend the modeller with external Java code which allows implementing complex computations;
\item[$\bullet$]
built-in layout managers and possibility to implement a custom layout manager;
\item[$\bullet$]
wide range of predefined graphical representations of diagram elements and a mechanism to implement a custom graphical view; 
\item[$\bullet$]
a mechanism for diagram validation: the programmer can declare the rules to be checked and Obeo Designer will automatically generate the validation engine and mark the erroneous elements;
\item[$\bullet$]
for any graphical editor, Obeo Designer automatically provides instruments to export a view in various graphical formats, to hide and show representation elements, to undo/redo actions, and many other features.
\end{itemize}

A graphical editor for UML models based on Obeo Designer is implemented in \cite{Obeo-UML}. It is an open-source project that can be easily extended.

Another framework developed by Obeo is a model-to-text translator \textbf{Acceleo}~\cite{Acceleo} which is integrated with Obeo Designer. It can be used to transform an EMF-based model into any kind of code. The technology is based on templates: the programmer has to define textually the template of an input element and the corresponding output text; the programmer can invoke external Java services during text generation. From the given input, Acceleo constructs the generator that can be then distributed as an Eclipse plug-in.  
%that facilitates translation of EMF models into text. 
%In order to implement a code generator for the models developed in Obeo Designer, one can use the %Acceleo technology. It relies on the model-to-text transformation by templates.

\paragraph{}
In this work we will use Obeo Designer to define our own DSL that relies on UML and to implement a graphical editor. We will apply model static validation to ensure that the models are statically correct 
%we will use model-checking 
and use Acceleo model-to-text transformation to generate executable Java code.
\label{sec:context-mde}
