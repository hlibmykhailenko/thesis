\section{Parameterised networks of synchronised automata (pNets)}
\label{sec:context-pnets}
In this section we present the parameterised networks of synchronised automata (pNets) - an intermediate formalism that we will use in this work to encode the behaviour of GCM components. 

\newcommand{\Act}{\symb{Act}} 
\newcommand{\fv}{\symb{fv}}
\newcommand{\iv}{\symb{iv}}
\newcommand{\BC}[2]{\symb{BC}_{#1}(#2)}
% \newcommand{\PNfamily}[2]{\overleftrightarrow{\mylangle#2,#1\myrangle}}
% Now ignore the second parameter, that was _always_ P:
\newcommand{\PNfamily}[2]{\overleftrightarrow{\mylangle#1\myrangle}}
\def\AlgT{\mathcal{T}}
\def\AlgE{\mathcal{E}}
\def\AlgA{\mathcal{A}}
\def\AlgAS{\mathcal{A}_S}
\def\AlgB{\mathcal{B}}
\def\AlgI{\mathcal{I}}

\subsection{Term algebra and notations}
In the following definitions, we extensively use indexed structures
(maps) over some countable indexed sets. The indexes will usually be
integers, bounded or not. Such an indexed family is denoted as
follows: $a_i^{i\in I}$ is a family of elements $a_i$ indexed over the
set $I$. Such a family
is equivalent to the mapping $(i\mapsto a_i)^{i\in I}$.
To specify the set over which the structure is indexed, 
indexed structures are always denoted with an exponent of the form $i\in I$
(arithmetic only appears in the indexes if necessary).
Consequently, $a_i^{i\in I}$ defines first $I$ the set over which the
family is indexed, and then $a_i$ the elements of the family.

 For example $a^{i\in\{3\}}$ is
the mapping with a single entry $a$ at index $3$; exceptionally, such mappings with
only a few entries will also be denoted $(3\mapsto a)$.
When this is not ambiguous, we shall use abusive vocabulary and
notations for sets, and typically write ``indexed set over I'' when  
formally we should speak of multisets, and ``$x\in
A_i^{i\in I}$'' to mean $\exists i\in I.\, x=A_i$.
An empty family is denoted $[]$. % (it can be defined as $a_i^{i\in\emptyset}$).
%%\LUDO
To simplify equations, an indexed set can be denoted $\set{M}$
  instead of $M_l^{l\in L}$ when $L$ is not meaningful.


In all forthcoming definitions, we suppose that we have a fixed set
of variables, used to construct the expressions of our term algebra.
Our models rely on the notion of parameterised actions.  
We leave
unspecified the constructors of the algebra that will allow building
actions and expressions used in our models. Let us denote $\Sigma$ the
signature of those constructors, and $\AlgT$ be the term
algebra of $\Sigma$ over the set of 
variables $P$. We suppose that we are able to distinguish inside
$\AlgT$ a 
set of \emph{action terms} (over variables of $P$) denoted
$\AlgA$ (\emph{parameterised actions}), a set of
\emph{data expression terms} (disjoint from actions) denoted
$\AlgE$, % ($\AlgE\cap\AlgA=\emptyset$), 
and, among expressions,
a set of \emph{boolean expressions} (guards) denoted $\AlgB$.
For each term $t\in \AlgT$ we define $vars(t)$ the set of variables
of $t$. 

The countable indexed sets can also depend upon variables, and we
denote $\mathcal{I}$ the set of indexed sets using variables of $P$.
There must exist an inclusion relationship $\subseteq$ over the indexed sets
of $\mathcal{I}$, with the natural guarantee that this operation
ensures set inclusion when one replaces variables by their values.
In practice we will mostly use intervals for which the upper bound
depends on the variables of $P$: $\mathcal{I}=[1..n]$ where $n$ is an integer.

\subsection{The pNets model}
\label{section:pNets}

pNets were first formalised in \cite{BBCHM:article2009}. pNets are hierarchical structures made of parameterised labelled transition systems (pLTSs) at their leaves, synchronised by synchronisation vectors. 
In this section, we define the structure of pLTSs,
pNets and Queues. The  formal
properties of pNets have been further studied in \cite{HMZ:PDP15, DBLP:conf/forte/HenrioMZ16}. 

\paragraph{pLTS.} A pLTS is a labelled transition system with variables; a pLTS can have
guards and assignments of variables on transitions. Variables can be
manipulated, defined, or accessed inside actions, guards, and
assignments. 

We first identify the actions a pLTS can use. 
Let $a$ range over action
labels, $\symb{op}$ are operators, and $x$ range over variable names. The set $\AlgA$ of 
action terms used in pLTSs is 
defined as follows:
\[
\begin{array}[l]{rcl@{\quad}l}
  \alpha\in\AlgA&::=&a(p_1,\ldots,p_n)&\text{action terms}\\
  p_i&::=& ?x~|~\symb{Expr}&\text{action parameters (input variables or expression)}\\
  \symb{Expr}&::=& 
  \symb{Value}~|~x~|~\symb{op}(\symb{Expr}_1,..,\symb{Expr}_n)&\text{Expressions}
\end{array}
\]
We additionally suppose that each input variable does not
appear somewhere else in the same action term:
$p_i=?x\Rightarrow\forall j\neq i.\, x\notin vars(p_j)$

For $\alpha\in\AlgA$ we also suppose that there is a function $\iv(\alpha)$ that returns
a subset of $vars(\alpha)$ which are the input variables of $\alpha$, i.e. the
variables preceded by a ``$?$'' in the action label.

%A pLTS is formally defined as follows.
\begin{definition}[pLTS] 
\label{pLTS}
A parameterised LTS is a tuple
$pLTS\triangleq\mylangle S,s_0, L, \to\myrangle$ where:
\begin{itemize}
%% \item[$\bullet$] $P$ is a finite set of parameters, from which we construct
%%   the term algebra $\AlgT$, with the parameterised actions
%%   $\AlgA$, the parameterised expressions $\AlgE$, and
%%   the boolean expressions $\AlgB$.

\item[$\bullet$]
$S$ is a set of states. %  For each state $s \in S$, variables of $s$ are
% global to the pLTS. LUDO REMOVED : THERE ARE ONLY GLOBAL VARS

\item[$\bullet$]
$s_0 \in S$ is the initial state.

\item[$\bullet$]
$L$ is the set of labels of the form 
$\langle \alpha,~e_b,~(x_j\!:= {e}_j)^{j\in J}\rangle$,
where $\alpha \in\AlgA$ is a parameterised action, $e_b \in
\AlgB$ is a guard, and the variables $x_j\in P$
are assigned the expressions $e_j\in \AlgE$. Variables in
$\iv(\alpha)$ are assigned by the action, other variables can be assigned
by the additional assignments.
\item[$\bullet$] $\to \subseteq S \times L \times S$ is the transition relation.
%\item[$\bullet$] $\to \subseteq S \times L \times S$ is the transition relation and 
%if 
%$s \xrightarrow{\langle \alpha,~e_b,~(x_j\!:= {e}_j)^{j\in
%		J}\rangle} s'\in \to $ then $\iv(\alpha)\!\subseteq\! vars(s')$, 
%		$vars(\alpha)\backslash \iv(\alpha)\!\subseteq\! vars(s)$, 
%		$vars(e_b)\!\subseteq\! vars(s')$, and
%		$\forall j\!\in\! J .\,vars(e_j)\!\subseteq\! vars(s)\land 
%		x_j\!\in\!vars(s')$. %,  and $s= s'\Rightarrow J=\emptyset$. 
\end{itemize}
%\NoEndMark
\end{definition}

Note that we make no assumption on finiteness of $S$ or of branching
in $\to$.

\paragraph{pNet.} pNets are constructors for hierarchical behavioural structures: a
pNet is formed of other pNets, or pLTSs at the bottom of the
hierarchy tree. Request queues can also appear in leaves of a pNet system.
 A composite pNet consists of a set of pNets exposing
a set of actions, each of them triggered by internal actions in each of
the sub-pNets. The synchronisation between global actions and 
internal actions is given by  \emph{synchronisation vectors}: a
synchronisation vector synchronises one or several internal actions, and
exposes a single resulting global action.
Actions involved at the pNet level do not need to distinguish input 
variables. The set $\AlgAS$ of action terms used in pNets  is defined as follows:
\[\begin{array}[l]{rcl@{\quad}l}
  \alpha\in \AlgAS &::=&a(\symb{Expr}_1,\ldots,\symb{Expr}_n)
\end{array}
\]

%pNets are formalised as follows:
\begin{definition}[pNets]
A pNet is a hierarchical structure where leaves are pLTSs (or queues
defined below), and nodes are synchronisation artefacts:\\
$\pNet\triangleq pLTS~|~\Queue(\set{m})~|~\mylangle \pNet_i^{i\in I}, \symb{SV}_k^{k\in 
K}\myrangle$
where
\begin{itemize}

\item[$\bullet$] $I \in \I$ is the set over which sub-pNets are indexed, $I\neq\emptyset$.
\item[$\bullet$] $\pNet_i^{i\in I}$ is the family of sub-pNets.

\item[$\bullet$] $\symb{SV}_k^{k\in K}$ is a set of
  synchronisation vectors ($K\in\AlgI$). $\forall k\in K,
  \symb{SV}_k=\alpha_j^{j\in J_k}\to\alpha'_k$ where $\alpha_j, \alpha'_k\in\AlgAS$.  
  Each synchronisation vector
  verifies: %$\alpha'_k\in L$,
 $J_k\in\AlgI$ and $\emptyset\subset J_k\subseteq I$. %  , and $\forall j\!\in\!
%   J_k.\,\alpha_j\!\in\!\Sort(\pNet_j)$.% (see the definition
%   of sort below).
%  \Remarque{Question: $I \in \AlgI$ and $J_k \subseteq I \nRightarrow   J_k\in\AlgI$
%    ??? We have not said it for the moment, but we could add it}   
\end{itemize}

%\NoEndMark
\end{definition}

A synchronisation vector $\symb{SV}_k$ of the form $\alpha_j^{j\in J_k}\to\alpha'_k$ 
means that if each
sub-pNet $j$ in $J_k$ performs synchronously the action $\alpha_j$; this
results in a global action labelled $\alpha'_k$. For example, the
synchronisation of the same action in two processes indexed $i$ and $j$ corresponds to
the synchronisation vector $(i\mapsto a,j\mapsto a)\!\to\!\tau$ (recall that we
identify indexed sets and mappings, giving us a convenient notation
for synchronisation vectors). Brute-force unification of the sub-pNets actions with the corresponding vector actions, possibly
followed by a simplification step, allow us to identify the exact actions of the sub-pNets that should be synchronised.

When $I=[1..n]$, it is equivalent to use tuple notations instead of
indexed sets. In that case, we  denote the pNet as $\mylangle \pNet_{1}, \ldots,
\pNet_n,\symb{SV}\myrangle$,  and each synchronisation vector as: $\langle
\alpha_1,\ldots,\alpha_n\rangle\to \alpha$. In that case, elements not
taking part in the synchronisation  are denoted $-$ as in: $\langle -, -, \alpha, -, - 
\rangle\!\to\! \alpha$.

\paragraph{Queues.}
There also exists a particular pNet construct called $\Queue(\set{m})$; it
models the behaviour of a FIFO queue, with $\set{m}$ the set of enqueue-able
elements. %  It can be considered as
% an infinite pLTS with a set of actions depending on the chosen term
% algebra and of the set of enqueue-able elements $\mathcal{M}\subseteq\AlgT$.
  We assume that the term algebra has two generic constructors $iQ$ and
\symb{Serve}
%\footnote{We chose constructors coherent with the term algebra we will use in this article to simplify notations.} 
such that, 
$\forall m_i\in \set{m}.\,\symb{Serve}\_{m_i}\in\AlgAS\,\land\,iQ\_{m_i}\in\AlgAS$.
Then the queue pNet offers the following actions: $L= \{iQ\_{m_i}|m_i\in
\set{m}\}\cup\{\symb{Serve}\_{m_i}|m_i\in \set{m}\}$. The
behaviour of a queue is only FIFO en-queueing/de-queueing of
requests. It could be encoded as an infinite pLTS.

\paragraph{Sort.} For each pNet, 
we define a sort function ($Sort:\pNet\to\AlgA$).
The sort of a pNet is its signature: the set of actions that it can
perform. For a pLTS we do not need to distinguish input variables. More
formally\footnote{$\subst{y\gets x}$ is the substitution operation.}:
\[
\begin{array}{l}
\Sort(\mylangle S,s_0, L, \to\myrangle) = \{\alpha\subst{x \gets ?x| 
x\in\symb{iv}(\alpha)}|\langle \alpha,~e_b,~(x_j\!:= {e}_j)^{j\in J}\rangle\in L\} \\ 
\Sort(\mylangle 
\set{\pNet}, \set{\symb{SV}}\myrangle) =\{\alpha'_k |\, \alpha_j^{j\in 
J_k}\to\alpha'_k\in\set{\symb{SV}}\} \\
\Sort(\Queue(\set{m})) =  \{iQ\_{m_i}|m_i\in \set{m}\}\cup\{\symb{Serve}\_{m_i}|m_i\in 
\set{m}\}
\end{array}
\]



%Whenever pNets will be translated into (ultimately finite) automata structures for
%model-checking, pNet Queues will naturally be represented by finite
%automata.
%However, in order to be able to address more general approaches, and in
%particular specific model-checking algorithms for unbounded channels,
%we keep a high-level representation of Queues. From these
%abstract Queues, we will be able to generate both regular
%representation (for unbound queues), and finite representation (for
%explicit-state model-checking).

\paragraph{More notations.}
% In the original pNet definition some of the sub-pNets of a pNet
% could be a family of pNets, however we provided a clearer definition
% here, and add the possibility to instantiate a family of pNets.

A constructor for a pNet is made of an indexed family of pNets.
$\PNfamily{PN_i^{i\in I}} P$; it takes a family of pNets indexed over a set
$I\in\AlgI$ % (remember that indexes in $I$ may be parameterized
% expressions),
 and produces a global pNet. The synchronisation vectors
for 
this family will be expressed at the level above, consequently we
``export'' all the possible synchronisation vectors that
the family could offer, even if only some of them will be used.
\[
\begin{array}{l@{}l}
\PNfamily{\symb{PN}_i^{i\in I}} P& \triangleq \mylangle \symb{PN}_i^{i\in 
I},\{\set{\alpha}\to\set{\alpha}|\set{\alpha}\in \symb{V}\}\myrangle\\
&\text{where }\symb{V} = \{\alpha_j^{j\in J}|\, J\!\subseteq\!I\land\forall j\in 
J.\,\alpha_j\in\Sort(\symb{PN}_j)\}
\end{array}
\]
This supposes that the elements of \symb{V} % belong to the term
% algebra and more precisely 
are action terms.
If all the elements of the family are identical, then we simply write
$\PNfamily{\symb{PN}^{\,I}} P$.
 
%In fact, the definition of pNets shown here is a ``simplified''
%version of pNets \cite{BBCHM:article2009} that is convenient for
%providing a concise formal definition of both pNets themselves and the
%component specification in terms of pNets.  Especially concerning
%families of pNets, it is not reasonable, in practice, to define all
%the possible synchronisation vectors inside a family.  In
%\cite{BBCHM:article2009}, we defined a 
%version of pNets where the families are flattened in the enclosing
%pNet and only the used synchronisations are instantiated. Though more
%efficient in practice this notation was more complex, that is why a simpler
%definition is presented in this article. Section~\ref{sec:full-example}
%will show an optimised instantiation of the produced pNet structure and
%synchronisation vectors. 
\smallskip

An operational semantics for pNets is given in~\cite{HMZ:PDP15}.

\subsection{Observation and flow of information}\label{sec:more-details-term}
% Let us consider several aspects of the term algebra we might use in
% the description below, those aspects are not related to the pNets
% semantics but rather to the way we use it.
%The synchronisation vectors allows a lot of flexibility in the use of the variables, and
%the decidability of whether a synchronisation vector can be triggered depends a lot on
%the precise action algebra (operators, allowed expressions, ...). In this work however
%the case is much simpler: for each globally synchronised action there is a single pLTS
%that outputs the value of each parameter (and potentially several targets).
%

In the context of encoding GCM components with pNets we assume that
 for each globally synchronised action there is a single pLTS
that outputs the value of each parameter (and potentially several targets). However,
several actions will receive one parameter and output another one, without any
consequence on the decidability of which actions can be triggered. 
%In the drawings,
%arrows will always indicate the direction of the flow information (however sometimes,
%this flow is not unidirectional, in that case we indicate the most obvious direction and
%explain the details in the text). 
By convention, we use labels starting by ``\emph{i}''
for the input side of the main flow (if there is one) as shown in the diagrams, like \emph{iQ} and \emph{iR}.

Finally, we identify the actions that are already synchronised (they will not need 
further synchronisation). We slightly extend the action algebra with such already
synchronised actions (distinguished by underlined labels): 
\[\begin{array}[l]{rcl@{\quad}l}
  \alpha\in \AlgAS
  &::=&a(\symb{Expr}_1,\ldots,\symb{Expr}_n)~|~\underline{a}(\symb{Expr}_1,\ldots,\symb{Expr}_n)&\text{pNet   actions}\\ 
\end{array}
\]

% In our term algebra, we have three basic kinds of actions: \emph{input
%   actions} of the form $?a(x_1,...,x_n)$ where $x_i$ are input
% variables \TODO{mention special cases when they are not only input vars}, \emph{output 
%actions} of the form $!a(v_1,..,v_n)$, where
% $v_i$ are values (expressions), and \emph{synchronised actions} of the
% form $a$ only used for observation purposes. Our actions can also be
% parameterised by one or several arguments thus
% they can be of the form $a(\symb{arg})$ or $a(\symb{arg},p)$. 
Synchronised actions are not
meant to be used anymore for synchronisation purposes, they should
just be visible at the top-level of the pNet hierarchy for the observation purpose.  Consequently,
we define an operator that takes an indexed set of pNets and returns
the synchronisation vectors that should be included in the parent
pNets to allow the visibility of synchronised actions:
\[\Observe(\pNet_i^{i\in I}) = \{(i\mapsto\underline{\alpha})|i\in 
I\land\underline{\alpha}\in\Sort(\pNet_i)\}\]

In the following, those synchronisation vectors dedicated to
observation will be \emph{implicitly} included as synchronisation
vectors of all the pNets. This means that for all pNets,
$\Observe(\pNet_i^{i\in I})$ is implicitly included in the set of
synchronisation vectors (where $\pNet_i^{i\in I}$ is the set of
sub-pNets of the new pNet). These synchronisation vectors are only useful to observe the
internal reductions.
% one could exclude them without any consequence on
%the global semantics.
%
%In this article, we do not use explicitly invisible actions ($\tau$
%transition). If the action algebra contains $\tau$ transitions, then we
%would use weak bisimulation notions to deal efficiently with the
%pNets. More precisely, $\tau$ would behave similarly to the synchronised
%actions defined above. This implies that, upon composition of pNets, a
%sub-pNet can perform additional $\tau$ transitions.

In order to express synchronisation vectors of families of pNets, we
must allow families of actions to be considered as actions themselves.
More precisely, if  $a_i$ is an action, then actions can be of the
form $(a_i)^{i\in I}$, or $i\mapsto a$ to allow the sub-pNet at index $i$ to
perform an action. % ,
% also we use $i\in I\mapsto a_i$ to express the family of actions $a_i^{i\in I}$
% triggering a synchronous action on all the sub-pNets indexed in $I$,
% or $(i\mapsto a,j\mapsto b)$ to synchronise two elements of the family.

%  apply uses weak
% transitions ($(\xrightarrow{\tau})^*\xrightarrow{\alpha}(\xrightarrow{\tau})^*$),
% in particular when composing behaviour of pNets.

\subsection{Adequacy of pNets for modelling GCM components}
\label{subsec:context-pnets-adequate}
In this work we will present the behavioural semantics of GCM
components, expressed as a translation from a component architecture
into a hierarchical pNet. Before defining this
translation, we explain below why the pNets were chosen as a support for GCM
behavioural semantics.

First, our goal is to provide a model adapted to
the behavioural verification of the properties of GCM applications. It
must be adapted to the generation of a model that can then be
verified, typically a finite model that could be model-checked, even
if other techniques could be envisioned. In pNets, models can use
parameters, both in the structure and in the LTSs which allows us to
give a semantics based on an infinite set of states, but also to
easily consider finite instances by restricting each parameter to a
finite domain. Thus the first reason for the choice of pNet is that
\emph{it is adapted to the definition of infinite models from which a finite
instance can easily be extracted.}
%Defining finite instances in this manner can be done using abstract interpretation 
%techniques, while preserving logical properties of the system~\cite{CleavelandRiely94}.

Second, the semantics of communication and
asynchrony of pNets fits closely to the one of GCM. Indeed, 
to guarantee causal ordering of requests, GCM components communicate
by a rendez-vous mechanism. In GCM/ProActive the request sending and its
arrival in the queue of the destination component occur synchronously. The rest of the
execution is entirely asynchronous.
 pNets have a similar semantics:
they are made of independent pLTSs or pNets interacting by synchronous communications. On 
the
contrary, futures are a too high-level construct to be part of pNet
definition. They will have to be encoded by a set of pLTSs. Next
sections will show that the parameterised nature of pNets and the
synchronisation vectors allow  for encoding futures in a precise
and generic way. Thus the second reason why we chose pNet is
that \emph{it provides a communication model similar to GCM for
  requests and give enough expressive power to encode futures.}

%\LUDO{rewrote reconfig paragraph}
From another point of view, we mentioned earlier that we
want our behavioural models to represent the structure of the
application (e.g. to allow the encoding of reconfigurations). On that
aspect and more generally when encoding communication channels, $\pi$-calculus
might seem to be a reasonable
approach. However, we think that channels \emph{\`a la} $\pi$-calculus are
too powerful. Indeed, in GCM/ProActive bindings are not first-class
entities and can only be reconfigured by an application
manager. Additionally, pNets are much better adapted to the
verification techniques we target, i.e., finite state model-checking,  than  
$\pi$-calculus.

% \RAB{Reconfig part -- From another point of view, in order to
% provide model for reconfigurable component systems, reconfigurable
% channels, as they exist in $\pi$-calculus channels might seem to be a reasonable
% approach. However, we think that channels \emph{\`a la} $\pi$-calculus are too powerful
% relatively to our needs. Indeed, synchronisation vectors can be
% reconfigured easily, by using a controller pLTS, as we will illustrate
% in Section~\ref{section:BC}. Such reconfigurable synchronisation
% vectors are sufficient for our needs, and much better adapted to the
% verification techniques we target, i.e., finite state model-checking.}


Finally, \emph{the hierarchical structure of pNets fits well with
hierarchical components}. The different levels of hierarchy of the
ADL will lead to the same hierarchical levels in pNets, even if
additional pLTSs and pNets will  be defined
to encode specific features (e.g., future proxies).