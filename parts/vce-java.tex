\section{Code generation and execution}
\label{sec:vce-java}

When the user has checked that the conceptual model of the designed application is statically correct with respect to the rules formalised in Chapter~\ref{chap:formalisation} and if the model-checker has proven the desired functional properties of the modelled system as discussed in the previous section, then VerCors can generate the implementation code of the designed application. The generated code is ready to run in the GCM/ProActive environment. 

Figure~\ref{fig:code-gen-workflow} illustrates the general workflow of the process. The generator takes as an input a component diagram with components and connections between them, a UML class diagram with UML classes and interfaces, UML state machine diagrams specifying the behaviour of the server methods and a type diagram. Additionally, the platform asks the user to specify the name of the package in which the generated files will be placed. Before starting the generation, VerCors invokes the state machine parser which parses the UML state machines in the same way as for pNets construction (this step is omitted in the figure). Then, two generators are applied to transform the user-defined conceptual model into an input for ProActive: an ADL generator and a Java code generator.
As explained in Section~\ref{subsec:context-proactive}, ProActive takes as an input the component architecture specified in a GCM/ADL XML-based file, a set of Java classes implementing the behaviour of the primitive components and a set of Java interfaces with the signatures of GCM interfaces.

\begin{figure}
     \centering
     \includegraphics[width=12cm]{drawings/code-gen-workflow.pdf}
     \caption{The workflow of implementation code generation}
     \label{fig:code-gen-workflow}
 \end{figure} 

In this section we first discuss the ADL and Java code generators and then the execution of the produced code.

\subsection{ADL generation}
\label{subsec:vce-adl-gen}

We start by an overview of the ADL generator which transforms a component architecture model into an XML-based file. The kernel of its current version is mainly based on the code taken from the previous versions of VerCors and adapted by an engineer who worked on the project, hence, the  content of this sub-section should not be considered as an exclusive contribution of this thesis. Still, we  extended the generator with the non-functional part construction as will be explained in~\ref{chap:advanced}.


\paragraph{The JAXB framework.} Converting Java objects into XML is a very classical task which is supported by multiple libraries. The ADL generator of VerCors is implemented using the JAXB (Java Architecture for XML Binding) technology~\cite{JAXB} which is a Java library for converting Java objects to XML representations (the process is also known as "marshalling"). Listing~\ref{list:jaxb-class} illustrates the usage of JAXB. Lines~1-8 present an example of a class which stores a model of a simple component characterised by a name, a list of exposed interfaces and an implementation class. We omit the definitions of the companion \texttt{JAXBInterface} class. Lines~10-17 demonstrate an instantiation of the given class, and Listing~\ref{list:jaxb-xml} shows how the constructed object is marshalled. 
JAXB requires a Java class being translated into XML to be annotated with \symb{@XmlRootElement} (line~1). This and the other annotations used for the mapping can be followed by additional parameters. For example, the programmer can specify the name of the generated XML element as an attribute of \symb{@XmlRootElement} annotation. By default, the class name is used for this purpose. The technology allows for straightforward mapping of class fields, i.e. each field (of a generated class) of  type String is mapped to an XML element with the corresponding name and value without any additional effort from the programmer. For instance, the field \texttt{name} from line~3is translated this way.  The fields whose types are Java classes annotated with \symb{@XmlRootElement} (like the interface at line~4) are also mapped into XML elements. 
The programmer can use annotations in order to customise the mapping. For instance \symb{@XmlAttribute} with additional parameters preceding a field of a class allows specifying properties of the generated XML attribute. 
By default, he order of the generated entities reflects the order of the corresponding fields in the Java classes.
\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={An example of a JAXB-based class}, label = {list:jaxb-class}, basicstyle=\scriptsize]
@XmlRootElement(name = "component")
public class JAXBComponent {
    private String name;
    private List<JAXBInterface> interface;
    @XmlAttribute(name = "content class})
    private String clazz;
    ...
}

public static void main() {
	JAXBComponent c = new JAXBComponent();
	JAXBInterface itf = new JAXBInterface();
	itf.setName("myItf");
	itf.setType("server");
	c.setName("myComponent");
	c.getInterfaces().add(itf);
	c.setClazz("ImplClass");
}
\end{lstlisting}
\end{minipage}

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={An XML file generated by JAXB}, label = {list:jaxb-xml}, basicstyle=\scriptsize]
<component name="myComponent">
	<interface name="myItf" role="server"/>
	<content class="ImplClass"/>
 </component>      
\end{lstlisting}

\paragraph{The generator.} The ADL generator takes as an input Java objects computed from the GCM components which were defined by the user in the front-end graphical editor. We recall that those objects are based on the EMF technology and their generated classes do not include the annotations required for using JAXB. Hence, VerCors needs an intermediate set of classes that are able to store the component model and could be translated into XML. We will call them JAXB classes. VerCors has such classes for the GCM components (a more complex version of the class shown in Listing~\ref{list:jaxb-class}), interfaces, bindings and the rest of the GCM elements but not for the UML elements because they are not translated into XML.

Overall, the ADL construction procedure has the following workflow. The generator takes as an input the user-defined components and starting from the root component it converts them into JAXB objects. Then, it invokes a dedicated factory from the JAXB library which prints JAXB objects in an XML-based file.

We plan to reuse the JAXB objects for the plug-in dedicated to reverse engineering ADL files into the models of VerCors graphical front-end. Indeed, the JAXB technology also supports "unmarshalling" XML files, i.e. translating an XML file into Java objects. We could rely on this mechanism in order to allow the user to import an existing ADL file into VerCors. This could be particularly useful for analysing GCM-based projects which were not initially designed in VerCors.

\subsection{Java generation}

The Java code generation includes three main steps: translating types into Java classes, converting UML interfaces into Java interfaces and translating UML classes with state machines defining method behaviour into Java classes. All three steps are based on the Acceleo~\cite{Acceleo} technology dedicated to model-to-text transformation.

\paragraph{Acceleo and type generator.} Acceleo follows a template-based approach for model-to-text transformation. In a template-based approach, a programmer should define a so-called \textit{template} in a dedicated language (in our case it is the Acceleo language) that would take an object being translated and use a sequence of the language statements in order to construct the corresponding text. An Acceleo template has access to the fields of the object, allows for classical control-flow statements including loops and if-else conditions, and is able to invoke external Java services. From the programmer-defined templates, Acceleo automatically produces Java code of the corresponding generator (model-to-text translator) which can be invoked from elsewhere.  

Listing~\ref{list:acceleo-record} demonstrates the simplest Acceleo template implemented in VerCors. It translates a record type into a Java class. The template signature is given at line~1: it specifies the template name and an input parameter \texttt{rtype} of type \texttt{RecordType}. Line~2 defines the name of the file where the text will be printed and lines~3-13 define the content of the file. As for any other Java class, the generated code should include the name of the package in which the class is included. For this, Acceleo invokes an external Java service at line~3 in order to get the user-defined package name and if it is not empty, concatenates the name with \textit{".type"} which means that the generated type will be included in the "package\_name.types" package. Line~7 prints an imported class. Line~9 specifies the signature of the class being printed: for this, it gets the name of the record type. Finally, the loop at lines~10-12 iterates over all fields of a record and for each field it prints the type and the name. In order to get the name of a field type, the template also invokes an external Java service which maps a VerCors type into a Java type. 

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={An Acceleo template translating VerCors record type into a Java class}, label = {list:acceleo-record}, basicstyle=\scriptsize]
[template public generateRecord(rtype : RecordType)]
[file (rtype.getTypeName().concat('.java'), false, 'UTF-8')]
[if not(getPackage().toString().equalsIgnoreCase(''))]
package [getPackage(true) /].types;
[/if]

import java.io.Serializable;

public class [rtype.getTypeName()/] implements Serializable {
[for (field : Field | rtype.fields)]
	[getTypeName(field.type)/] [field.name/];
[/for]
}
[/file]
[/template]
\end{lstlisting}

Listing~\ref{list:rtype-code} provides an example of a record type \texttt{RecordExample} translated into a Java class. The type has two fields: a boolean field \texttt{b}, and a field \texttt{e} of a user-defined enumeration type \texttt{EnumType} which definition is omitted here.

\begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={Java code of a record type generated by VerCors}, label = {list:rtype-code}, basicstyle=\scriptsize]
package exmaple.types;

import java.io.Serializable;

public class RecordExample implements Serializable {
	Boolean b;
	EnumType e;
}
\end{lstlisting}

We defined templates for the record and enumeration VerCors types. Then we used Acceleo in order to produce the corresponding code generator. While constructing the implementation code, VerCors invokes the generator for each record and enumeration type of the user-defined type diagrams; the order in which the Java classes are produced is not important.

\paragraph{Translating UML interfaces and classes.} Similarly, we defined templates translating UML interfaces and classes into Java code. The case of interfaces is quite trivial: the template iterates over the UML methods of an interface and for each method it generates the signature in Java. Converting UML classes is, however, more complex. We recall that the UML classes are used in VerCors in order to specify the implementation of primitive components business logic. We say that a class is "attached" to a primitive when the behaviour and the attributes of the component are defined by the class.

While translating a class attached to a primitive, we have to take into account the component's server and client interfaces, the attributes and operations of the class. Listing~\ref{list:primitive-code} demonstrates a simplified version of the \texttt{Class0} attached to one of the primitive components participating in the Peterson's leader election algorithm modelled in Chapter~\ref{chap:vce-overview}; Figure~\ref{fig:comp1} illustrates once again this component. 
The Acceleo template dedicated to a UML class translation invokes an external Java service in order to obtain the set of server and client interfaces of a primitive. The server interfaces are included in the list of interfaces implemented by the class (\texttt{ElectionItf} at line~1), the client interfaces are converted into the fields of the class (lines~8-11) which can be then accessed by the class methods in order to perform a remote method invocation. Additionally, we generate several auxiliary methods which will be used by the ProActive factory in order to bind or access the interfaces (lines~13-15). The user-defined class attributes are translated into class fields (lines~2-6). Finally, VerCors generates the signatures and the bodies of the user-defined server and local methods (line~19). We distinguish three kinds of methods: methods with a state machine specifying the behaviour, methods for which the user did not provide behaviour description, and  attribute set/get methods. The first case involves translation of a state machine into Java code which will be explained in the next paragraph. If the user has not specified a method behaviour, an empty body is generated. Finally, VerCors produces standard code for the get- and set-methods which will be discussed in Section~\ref{sec:advanced-ac}

\begin{figure}[t]
     \centering
     \includegraphics[width=12cm]{drawings/comp1.pdf}
     \caption{A primitive with an attached UML class}
     \label{fig:comp1}
 \end{figure}
 
 \begin{minipage}{\linewidth}
 \begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={Generated Java code of a primitive component}, label = {list:primitive-code}, basicstyle=\scriptsize][t]
public class Class0 implements Serializable, BindingController, ElectionItf, Class0AC{
	//local variables	
	public boolean isActive = false;
	public int left = 1;
	public int cnum = 1;
	public int max = 1;
	
	//client interfaces
	protected ElectionItf C1;
	protected MonitorItf C2;
	protected KeyStorageItf C3;

	//Binding controller's methods
	public void bindFc(...) {...}
	public String[] listFc() {...}
	...
	
	//server methods
	public  void message(int step, int val) {...}
	
	//local methods
	public  void encrypt(int key) {...}	
	
	//Attribute controller methods
	public  void set_max(int value) {...}
	...}	

\end{lstlisting}
\end{minipage}

\paragraph{Translating UML state machines into Java code.} 
%VerCors relies on UML state machines for the specification of the business logic of primitive's methods. 
VerCors allows specifying one state diagram for each method of a primitive component which is then translated into Java code. Multiple studies~\cite{Kohler:2000:IUD:337180.337207, Niaz03codegeneration, Sekerinski2001} are dedicated to the translation of various types of state machine diagrams into executable code; most of them try to deal with the state machine hierarchy. However, the translation in our case is much simpler because a state machine in VerCors has only one region, only one level of hierarchy and no actions assigned to the states. 

As a part of the transformation process, VerCors constructs an enumeration \texttt{State} which stores the names of all states of the state machines being translated. Note that here we generate one enumeration type for all state machines modelling the behaviour of a given application.
 Each generated method has a local variable \texttt{curState}  of type \texttt{State} which holds the current
state of the state machine and actions are taken in a switch-case statement depending on the value of the variable.
Listing~\ref{list:sm-code} demonstrates the Java code constructed by the platform for the state machine from Figure~\ref{fig:simple-sm}. In addition to the \texttt{curState} variable, VerCors translates the user-defined local variables of a state machine~(line~2). Then, the platform  generates an infinite loop comprising a switch-case statement which checks the value of \texttt{curState}. For each state of a state machine VerCors creates a \texttt{case} statement that includes the Java code corresponding to the label of the outgoing transitions. Note, that an \texttt{if-else} statement is included in the case of multiple outgoing transitions (lines~11-20). In the current version, the translation of UML state machines is fully implemented in Java, but we believe, we could benefit from porting it to Acceleo. The reason is that the Acceleo code generators are easier to maintain and the code of the model-to-text transformation templates looks cleaner than a Java code generator with printing instructions.  

\begin{figure}
     \centering
     \includegraphics[width=6cm]{drawings/sm-example-code.pdf}
     \caption{A simple state machine}
     \label{fig:simple-sm}
 \end{figure}

\begin{minipage}{\linewidth}
 \begin{lstlisting}[language=Java, numbers=left, rulecolor=\color{black}, caption={Generated Java code of a state machine}, label = {list:sm-code}, basicstyle=\scriptsize]
	State curState = State.Initial;
	boolean x;
	
	while(true) {
		switch (curState) {
		case Initial:
			x = this.get_isActive();
			curState = State.Choice1;
			break;
		case Choice1:
			if(x == true) {
				curState = State.State1;
				break;
			}
			else if(x == false) {
				this.set_isActive(true);
				x = true;
				curState = State.State1;
				break;
			}
		case State1:
			C1.message(0, 0);
			return x;
		}
	}
}	
\end{lstlisting} 
\end{minipage}

The advantage of such an approach is that the generated code mirrors the state machine structure. However, a significant drawback is that the code is long and not very convenient
for the programmer since \textit{do-while, for, while} constructs
cannot be written as such in the state machine, but will rather be
encoded within the state structure, separated by case instructions. 
%\TODO{Should I write an equivalent normal code here?}.
We still discuss the possibility to enhance our framework by implementing a plug-in which would be able to recognise the classical control-flow patterns in a state machine and to generate more user-friendly code. On one hand, we would like to improve the quality and the readability of the produced code. On the other hand, this would require significant effort on the static analysis and restrict the user-defined input. This could be useful if we expected the user to modify the generated code which is not the case in our framework, because the constructed implementation has been model-checked by VerCors, and if the user tries to modify the logic of the generated methods, the verified functional properties cannot be guaranteed any more.   
 
\subsection{Code execution}

The code generated by VerCors can be executed on top of the ProActive platform. In order to check that the produced application, indeed, behaves as we expect, we use a dedicated visualiser~\cite{AO-viewer} which analyses and shows the request exchange between ProActive active objects at run-time.
For instance, we generated ProActive/Java code of our use-case example of Peterson's leader election algorithm from Figure~\ref{fig:peterson-components}; the resulting execution is shown in
Figure~\ref{fig:code_exec}.
 Black arrows represent
request emissions (the figure only shows some of them). Yellow and blue rectangles show request processing. For
example, we can see how the call to \textit{runPeterson} of
Application is transmitted to Comp4 and at the end of the
\textit{runPeterson} request processing Comp4 triggers the elections
on Comp1 by calling \textit{message(0,1)}. At the end of the
algorithm execution we can see how Comp3 reports to the Application
that it is not the leader and Comp1 claims to be the leader.

\begin{figure}
	\centering
	\includegraphics[width=8cm]{drawings/code_exec.pdf}
	\caption{Code execution}
	\label{fig:code_exec}
\end{figure}



