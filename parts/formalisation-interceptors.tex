\section{Interceptors}
\label{sec:formalisation-interceptors}

%In this section we define interceptors more precisely. 
The separation between the functional and non-functional parts of an application is important for the safety and re-usability of software components. However, a clean interaction between these two concerns is highly desirable because they can often influence each other. We formalise the notion of interceptors which are special components used for the interactions between functional and non-functional elements. Interceptors can
observe functional invocations and trigger a reaction of the control part of the component. In the other
direction, non-functional components can influence the functional behaviour by modifying its behaviour
in two ways: either through reconfiguration, i.e. modification at runtime of the component architecture,
or by changing parameters, i.e. component attributes, that will influence the functional behaviour.
The formalisation of component architecture with interceptors has never been provided before. In this section, we, first, we introduce the
non-formal definition of an interceptor. Then, we present a predicate
recognising the interceptors among the other sub-components of a
membrane. 

An interceptor is a functional component inside a membrane. It is
connected to one external and one internal functional interfaces of the
membrane's parent. % In the case of a primitive
% component, the internal interface does not exist explicitly, so the
% ``internal'' interface of the interceptor will be implicitly
% connected to the business code of the component. In all cases a
An
interceptor "intercepts" a functional call that goes from outside to
inside of the membrane (input interceptor) or vice versa (output
interceptors). The interceptors are used to monitor an application and
its functional calls. The only functional activity of an interceptor
should be to 
intercept and forward the functional calls through its functional
client and server interfaces. All the other actions are
performed through non-functional interfaces. To allow more
modularity in the design, interceptors can be assembled in
\emph{chains} inside the membrane. Interceptors in a chain must all be
either input or all output; we shall speak of input chains
and output chains. An example of an input chain is illustrated at Figure \ref{fig:formalisation-interc-chain}; here, the chain is formed by two interceptor components: \texttt{Products\_Monitor} and \texttt{Frequence\_Monitor}. By contrast, \texttt{Controller} is a component-controller: it does not intercept any functional call but communicates with one of the interceptors and with the component in the content.

\begin{figure}
     \centering
     \includegraphics[width=10cm]{drawings/formalisation-interc-chain.pdf}
     \caption{An input chain of interceptors}
     \label{fig:formalisation-interc-chain}
 \end{figure}

In principle, it would be possible to relax this definition, and allow for more
general interceptor structures, e.g. including some parallelism in the form of multicast
client interface in the ``chain'', allowing more efficient processing. However it is
not clear whether this would be useful for real applications, and this is not implemented 
in the GCM/ProActive middleware, so we prefer to keep the (relatively) simple form 
in our formal definition.

In order to distinguish formally the interceptors from the other components, we
define a predicate $\symb{IsInterc}$ (see Table \ref{table-Interc-pred}). It takes a component and a membrane as
an input and returns $true$ if the component belongs to a chain of
interceptors inside the given membrane. An interceptor chain consists
of one or several interceptors. $\symb{IsInterc}$ uses a predicate $\symb{IsIntercChain}$ which identifies if a given sequence of $K$ components is a chain of pipelined interceptors inside the given membrane. $IsIntercChain$ predicate checks the following features of the indexed set of components given as input:
\begin{itemize}
\item[$\bullet$]
all the components are inside the membrane;
\item[$\bullet$]
all the components have exactly one functional server and one functional client interface (they can have other non-functional interfaces);
% \item
% the functional interfaces of the components have singleton cardinality;
\item[$\bullet$]
a functional call can go through the sequence of components. More formally, for any $k \in {2...K}$ there is a binding connecting client functional interface of the component number $k-1$ and server functional interface of the component number $k$;
\item[$\bullet$]
the first component of an input chain intercepts a functional call from an external functional interface to
the content while the last component forwards the
functional call to the content or vice versa for an output chain.
\end{itemize}

Predicate $\symb{IsIntercChain}$ uses two auxiliary functions: $\symb{GetSItf}_F(\symb{comp})$
(resp. $\symb{GetCItf}_F(\symb{comp})$) returns the sets of all \emph{functional} server
(resp.  client) interfaces of component \symb{comp}.
%$\symb{Itf}_F(container)$ returns all functional interfaces of the container.

The predicate \emph{IsExtEnd} checks, for an input
interceptor chain,
whether the first interceptor in the chain is  connected to a
server functional interface of the parent component or, for the output
interceptors, whether the last one is connected to a client functional
interface of the parent component.
 Predicate \emph{IsIntEnd} is the symmetric, it checks connection with
 the content. However,
the content is not known for a primitive component; in that case the
interfaces of the content are computed by symmetry of
the external (functional) interfaces of the component.


\begin{table}
\begin{center}
  \begin{tabular}{ | p {1 cm} | p {13 cm} | }
 \hline
\multicolumn{1}{|c|}{ Predicate name} & \multicolumn{1}{c|}{Predicate Definition} \\ 
 \hline
IsInterc & $\symb{IsInterc}(comp : Comp, ~membr : Membr) \Leftrightarrow$\newline
           \text{}~~$\exists ic.IsIntercChain(ic, membr) \land comp \in ic;$\\
\hline
IsIntercChain & $\symb{IsIntercChain}(\{i_{1}\,...i_{K}\} : \text{set of} ~Comp, ~membr : Membr) \Leftrightarrow$\newline
   $\{i_{1}\,...i_{K}\}  \subseteq Comp(membr) \land \exists SI_1...SI_K, CI_1...CI_K.$\newline
    \text{}~$(\forall k \in \{1...K\}).$
    $(\symb{GetSItf}_F(i_k) \!=\! \{SI_k\}  \land
    \symb{GetCItf}_F(i_k) \!=\! \{CI_k\} ) ~\land $%\newline
    %\text{}\qquad$
%$\land Card(SI_k) \!=\! Card(CI_k) \!=\! \symb{singleton}) \land$
\newline
    \text{}~$(\forall k \in \{1...K-1\}).( \exists b_k \in \symb{Binding}(\symb{membr}).$\newline
    \text{}\qquad$\symb{GetSrc}(b_k, \symb{membr})=CI_{k} \land \symb{GetDst}(b_k, \symb{membr}) = SI_{k+1}) \land$\newline
%    \text{}~   $\symb{If}~ \symb{IsComposite} (\symb{Parent}(membr)) ~then $\newline
%
    \text{}~   $\exists b_0,b_K \in \symb{Binding}(\symb{membr}).
\exists I_0,I_K : \Itf.$ \newline
 $\big((\symb{IsExtEnd}(I_0,b_0,SI_1,\symb{membr},I_0) \land $ $\symb{IsIntEnd}(CI_k,b_K,I_K,\symb{membr},I_K))\lor$\newline
 $ (\symb{IsIntEnd}(I_0,b_0,SI_1,\symb{membr},I_0) \land
\symb{IsExtEnd}(CI_k,b_K,I_K,\symb{membr},I_K)) \big)$\\
%
%     \text{}~  $\symb{Else}~ \exists b \in \symb{Binding}(\symb{membr}).
% \exists I : \Itf.$\newline
%     \text{}\qquad   $\big((\symb{IsExtEnd}(I,b,SI_1,\symb{membr},I) \land \symb{IsCoreItf}(CI_k))$\newline
%      \text{}\qquad $\lor (\symb{IsCoreItf}(SI_1) \land
% \symb{IsExtEnd}(CI_k,b,I,\symb{membr},I)) \big)$\\
\hline
IsExtEnd &
  $\symb{IsExtEnd}(\symb{CI}:\symb{CItf},~b:Binding,~\symb{SI}:\symb{SItf},~\symb{membr}:Membr,~I:\Itf) \Leftrightarrow$\newline
   \text{}\quad $\symb{GetSrc}(b, \symb{membr})=\symb{CI} \land \symb{GetDst}(b, \symb{membr})=\symb{SI} \land Nature(I) = \textsf{F}\land $\newline
  \text{}\quad$\symb{Sym(I)}\in\symb{GetItf}(\symb{Parent}(\symb{membr}))$\\
 \hline
IsIntEnd &
  $\symb{IsIntEnd}(CI:\symb{CItf},~b:Binding,~SI:\symb{SItf},~\symb{membr}:Membr,~I:\Itf) \Leftrightarrow$\newline
   \text{}\quad $\symb{GetSrc}(b, \symb{membr})=CI \land \symb{GetDst}(b, \symb{membr})=SI \land Nature(I) = \textsf{F}\land$ \newline
    \text{}\quad $\symb{If}~ \symb{IsComposite} (\symb{Parent}(membr))
   ~then  \newline
   \text{}\quad ~\symb{Sym}(I)\in\symb{GetItf}(\symb{Cont}(\symb{Parent}(\symb{membr}))) $  \newline
\text{}~  $\symb{Else}~\symb{Sym}(I)\in\symb{Sym}(\symb{GetItf}(\symb{Parent}(\symb{membr}))) $  
    \\
 \hline
  \end{tabular}
\end{center}
\caption{Interceptor Predicates}
\label{table-Interc-pred}
\end{table}


%% \begin{eqnarray}
%% && IsInterc(comp : Comp, ~membr : Membr) \Leftrightarrow  \\ \nonumber
%% && \exists ic.IsIntercChain(ic, membr) \land comp \in ic;
%% \end{eqnarray}

%% \begin{eqnarray}
%% && IsIntercChain(\{i_{1}\,...i_{K}\} : \text{set of} ~Comp, ~membr : Membr) \Leftrightarrow \\ \nonumber
%% && \{i_{1}\,...i_{K}\}  \subseteq Comp(membr) \land 
%% \exists SI_1...SI_K, CI_1...CI_K. \\ \nonumber
%% && ((\forall k \in \{1...K\}). \\ \nonumber
%% && (Nature(SI_k) = F \land Itf_F(i_k) = \{SI_k\}  \land \\ \nonumber
%% && Nature(CI_k) = F \land CItf_K(i_k) = \{CI_k\} \land \\ \nonumber
%% && Card(SI_k) = Card(CI_k) = singleton) \land \\ \nonumber
%% && \land \\ \nonumber
%% %
%% && (\forall k \in \{2...K\}).( \exists b_k \in Binding(membr). \\ \nonumber
%% && GetSrc(b_k, membr)=CI_{k-1} \land GetDst(b_k, membr) = Sitf_k) \land
%% \\ \nonumber
%% && either IsExternalEndOfChain(SI_1,membr) \land IsInternalEndOfChain(CI_k,membr) \\ \nonumber
%% && or IsInternalEndOfChain(SI_1,membr) \land IsExternalEndOfChain(CI_k,membr)
%% \end{eqnarray}

%% \begin{eqnarray}
%% && IsExternalEndOfChain(SI_1,membr) \Leftrightarrow  \\ \nonumber
%% && If IsComposite (Parent(membr)) then   \\ \nonumber
%% && \qquad \exists b_1 \in Binding(membr), \exists Itf_1 \in
%%   Itf(membr)). \\ \nonumber
%% && GetSrc(b_1, membr)=Itf_1 \land GetDst(b_1, membr)=SI_1 \land \\ \nonumber
%% && \qquad Nature(Itf_1)=F \land  (Sym(Itf_1)\in Parent(membr)\\ \nonumber
%% && Else IsCoreITf(SI_1)
%% \end{eqnarray}


%% \begin{eqnarray}
%% &&\land \\ \nonumber
%% %
%% &&(\exists b_1, b_2 \in Binding(membr), \exists Itf_1, Itf_2 \in Itf(membr)). \\ \nonumber
%% &&GetSrc(b_1, membr)=Itf_1 \land GetDst(b_1, membr)=SI_1 \land \\ \nonumber
%% &&GetSrc(b_2, membr)=CI_k \land GetDst(b_2, membr)=Itf_2 \land \\ \nonumber
%% &&Nature(Itf_1)=F \land Nature(Itf_2)=F \land \\ \nonumber
%% &&(Sym(Itf_1)\in Parent(membr) \land Sym(Itf_2) \in Content(Parent(membr)) \lor \\ \nonumber
%% &&Sym(Itf_1)\in Content(Parent(membr)) \land Sym(Itf_2) \in Parent(membr)))) \\ \nonumber
%% \end{eqnarray}
