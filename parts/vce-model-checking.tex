\section{From pNets to CADP}
\label{sec:vce-model-checking}

The current version of VerCors relies on the model-checker of CADP and in this section we explain how the pNets are translated into the input for CADP and provide the experimental results on the verification of Peterson's leader election algorithm discussed in Section~\ref{sec:vce-graphical-formalism}.  

\subsection{Preparing the input: generating Fiacre, EXP and auxiliary scripts}

We recall that in CADP processes are stored as LTSs in BCG format and composed according to  rules expressed in EXP format.
Additionally, the tool provides techniques for state space minimisation.
Instead of generating directly BCG, we rely on a slightly more user-friendly Fiacre format for encoding the pLTSs which can be then translated into BCG by the flac compiler.
Overall, in order to prepare the input for CADP, we translate each pLTS involved in a system into Fiacre and the synchronisation vectors of each pNet into EXP. Then, we generate auxiliary scripts that assemble the produced files, build and minimise the state-space in a hierarchical manner.

%\begin{lstlisting}[float,language=Java, numbers=left, rulecolor=\color{black}, caption={Translating pNets into CADP input}, label = {list:cadp-translation}, basicstyle=\scriptsize]
%translatePNet (net : pNet)
%	for each subnet in net.subnets()
%		if subnet is pLTS
%			FiacreGenerator.generateFiacre(subnet)
%		else
%			PNetTranslator.translatePNet(subnet)
%	EXPGenerator.generateExp(net)
%	SVLGenerator.generateSVL(net)
%\end{lstlisting}
%
%
%\begin{algorithm}
%  \caption{Translating pNets into Fiacre and EXP}\label{alg:vce-mc-pnet-text}
%  \begin{algorithmic}
%    \Procedure{exportPNet}{$net$}\Comment{The g.c.d. of a and b}
%    \For {\textbf{each} $subnet$ in $net$}
%    	\If {$subnet$ is pNet}
%    		\State exportPNet($subNet$)
%    	\Else
%    		\State $subnet$.bcg $\gets$ flac($subnet$.fiacre)
%    		\State $subnet$.bcg $\gets$ minimise($subnet$.bcg)
%    	\EndIf
%    	\EndFor
%    \State $net$.bcg $\gets$ generate($net$.exp)
%    \State $net$.bcg $\gets$ minimise($net$.bcg)
%    \If {$net \neq root$}
%        \State $net$\_complete.bcg $\gets$ generate($net$.bcg)
%        \State $net$.bcg $\gets$ hide\_and\_minimise($net$.bcg)
%    \EndIf
%  \Return $net$.bcg
%  \EndProcedure
%\end{algorithmic}
%\end{algorithm}  

%Listing \ref{list:cadp-translation} provides a sketch of the recursive algorithm translating a pNet into an input for the model-checker. The algorithm traverses a hierarchical pNet 
%%in the depth-first-search order  
%while the order of visited sub-nets at the same lever is not important. More precisely, the \texttt{translatePNet} method takes a pNet as an input and examines all its sub-nets. For each sub-pLTS it invokes the Fiacre generator and for each sub-pNet it invokes itself. Finally, the method triggers the translation of the synchronisation vectors into EXP and the creation of SVL scripts.

\paragraph{Fiacre generation.} All pLTSs are transformed into Fiacre in the same way regardless of what kind of process they encodes. Figure \ref{fig:vce-pnets-ac-fiacre} illustrates a pLTS of a body translated into Fiacre. 
 The transformation includes the following steps:

\begin{enumerate}
\item
Transition analysis: the translator analyses the pLTS and constructs three sets: \textit{Vars} stores the variables of the pLTS, \textit{Types} stores the data types involved in the actions, and \textit{Actions} keeps the actions.
\item
Each state of the pLTS is mapped to a Fiacre state characterised by a name.
\item
\textit{Types}, \textit{Actions}, state names and  \textit{Vars} are translated into Fiacre. 
\item
For each pLTS state the translator transforms its outgoing transitions into Fiacre.
\end{enumerate}

\begin{figure}
     \centering
     \includegraphics[width=12cm]{drawings/vce-pnets-body-fiacre.pdf}
     \caption{Fiacre code of a body}
     \label{fig:vce-pnets-ac-fiacre}
 \end{figure}

\paragraph{EXP generation.} The generated processes are then translated into the BCG format and composed by EXP. The translation of the synchronisation vectors into EXP is quite straightforward  if all synchronised actions have either no parameters or parameters of the same type. In this case, we translate only the action names into EXP gates and omit the offers.

When the parameters of the synchronised actions have different types, we cannot synchronise actions only by gates but we have to include offers. For this, we explicitly generate each  value of a parameter in the synchronisation vector.
% and rename the corresponding action in the .bcg file the pNet accordingly.
 Let us consider a simple example. Assume, we have two LTSs \textit{A} and \textit{B} that are synchronised by a vector $\langle a(x), b(y) \rangle \to c(x, y)$ where $x$ is of type boolean and $y$ can have a value from an integer interval from 2 to 3. If we try to express such synchronisation in EXP using only gates (i.e. $\langle a, b \rangle \to c$), CADP will not accept the vector because the synchronised actions have different offers (i.e. different types of parameters). As an overcome, we instantiate with all possible values all parameters whose types do not match and generate a synchronisation vector for each possible instantiation:

\centerline{\textsf{$\textless$ a !0, b !2 $\textgreater \rightarrow$ c !0 !2}}
\centerline{\textsf{$\textless$ a !1, b !2 $\textgreater \rightarrow$ c !1 !2}}
\centerline{\textsf{$\textless$ a !0, b !3 $\textgreater \rightarrow$ c !0 !3}}
\centerline{\textsf{$\textless$ a !1, b !3 $\textgreater \rightarrow$ c !1 !3}}

Now, the synchronised actions are specified as a gate followed by offers and such synchronisation can be done. 
\TODO{Insert a listing of .exp for Comp1}

Another limitation of EXP is that it does not support indexed actions which are used in several synchronisation vectors of the GCM pNets. For instance, an indexed action is needed when a server method of a primitive component accesses a proxy at index $p$ in order to retrieve the result of a remote method invocation:  $\langle \symb{GetValue}\_m(p,\symb{val}), p\to\symb{GetValue}\_m(\symb{val}) \rangle \to \symb{GetValue}\_m(p,\symb{val}) $. While constructing such kind of communications in EXP, we rely on the fact that the number of proxies is fixed at the time when .exp files are created, and that the generated EXP synchronisation vector includes as many proxies as there are in the family. Hence, we can explicitly instantiate the value of $p$ and synchronise the server method action with a proxy at each possible index depending on the value of $p$. For instance, if the family size is equal to two, our example synchronisation vector will be translated into two EXP vectors as follows:

\centerline{\textsf{$\textless$ GetValue\_m!0, GetValue, -- $\textgreater \rightarrow$ GetValue\_m!0}}
\centerline{\textsf{$\textless$ GetValue\_m!1, --, GetValue $\textgreater \rightarrow$ GetValue\_m!1}}

\paragraph{Scripts generation.} When all pieces of the behavioural model have been produced, VerCors generates scripts for the automata construction and state space reduction.  
We first discuss in general the algorithm encoded by the scripts, and then we give the technical details on what kind of scripts are generated. 

\begin{algorithm}
  \caption{Constructing CADP input}\label{alg:vce-mc-cadp-input}
  \begin{algorithmic}
    \Procedure{buildPNet}{$net$}
    \For {\textbf{each} $subnet$ in $net$}
    	\If {$subnet$ is not pLTS}
    		\State $subnet$.bcg $\gets$ buildPNet($subnet$)
    	\Else
    		\State $subnet$.bcg $\gets$ flac($subnet$.fiacre)
    		\State $subnet$.bcg $\gets$ minimise($subnet$.bcg)
    	\EndIf
    	\EndFor
    \State $net$.bcg $\gets$ generate\_and\_minimise($net$.exp)
   % \State $net$.bcg $\gets$ minimise($net$.bcg)
    \If {$net \neq root$}
        \State $net$\_complete.bcg $\gets$ $net$.bcg
        \State $net$.bcg $\gets$ hide\_and\_minimise($net$.bcg)
    \EndIf
    
  \Return $net$.bcg
  \EndProcedure
\end{algorithmic}
\end{algorithm}  

The scripts encode the Algorithm~\ref{alg:vce-mc-cadp-input} which is based on the bottom-up approach for behavioural model construction. The \texttt{buildPNet} procedure described by the algorithm constructs the \texttt{.bcg} file for a pNet given as an input and the \texttt{.bcg} files for all its sub-nodes as follows. First, it iterates over all sub-nets and constructs their \texttt{.bcg} files. For this, the procedure invokes \texttt{buildPNet} for each the sub-pNet which is not a pLTS; for each sub-pLTS it invokes the Flac compiler which translates the pLTS into BCG format and the bcg\_min tool that minimises the state-space of the constructed \texttt{.bcg} file. After all pieces of the pNet have been constructed, \texttt{buildPNet} invokes EXP.OPEN which assembles the sub-nets with respect to the synchronisation vectors and creates a  minimised \texttt{.bcg} file of the resulting automaton. If the generated pNet does not model the root component, the scripts make a copy of its \texttt{.bcg} file, hide some of its labels, and minimise the state space. Copying the initial file could be optional, it is needed in order to allow the user to model-check the original behaviour of a sub-component that is not affected by its container. 
%For the sake of simplicity, we omit the renaming instructions in the algorithm.

\begin{lstlisting}[language=XML, numbers=left, rulecolor=\color{black}, caption={An example of SVL script}, label = {list:svl-example}, basicstyle=\scriptsize]
"Application_Comp1_complete.bcg"=  branching reduction of "Application_Comp1_SV.exp";
"Application_Comp1.bcg" = branching reduction of gate hide
	 "CALL_SET_MAX", "R_GET_MAX", ...
in "Application_Comp1_complete.bcg"
\end{lstlisting}

More technically, for each component being translated into input for the model-checker (i.e. the root component and its sub-components at all levels of hierarchy), VerCors generates a \texttt{.svl} file. Listing~\ref{list:svl-example} presents an example of the script generated for one of the participants of the leader election use-case from Figure~\ref{fig:peterson-components}. 
The SVL script 
%does the necessary renaming and then 
constructs the \texttt{.bcg} file modelling the component behaviour (line~1). Next, if the component is not the root (which is the case for our example) the script hides some of the internal communications (for instance, the access to attribute controllers), minimises the state space and produces the final model (lines~2-4). As a result, two files are generated: \texttt{Application\_Comp1\_complete.bcg} which represents the full behavioural graph of the component, and \texttt{Application\_Comp1.bcg} in which the internal communications are hidden.

Additionally, VerCors generates one \texttt{.sh} file for the whole model being constructed. The script invokes the Flac compiler on a \texttt{.fiacre} file of each pLTS included in the tree of the generated system. Then, it triggers execution of all produced SVL scripts starting from the components at the lowest levels of hierarchy and finishing by the root.
 
%
% Let us assume the system being built is a GCM application with N levels of hierarchy where the 1st level corresponds to the root component. First, the pLTSs from the lowest level of hierarchy are translated from Fiacre to BCG by the flac compiler. Then, the obtained automata are minimised by branching bisimulation. Next, CADP constructs the behaviour of the components at the lowest level of hierarchy EXP specification


\subsection{Model-checking with CADP}
Following the procedure described in the previous section, VerCors produces an input for the CADP model-checker. We experimented with generating and model-checking the behaviour of our use-case example of the leader election algorithm illustrated in Figure~\ref{fig:peterson-components}. Table
\ref{tab:bcg_files} presents size information for some of the intermediate
behaviour graphs. 
% File \texttt{Comp4.bcg} 
% stores the behavior of \texttt{Comp4}; \texttt{Comp4\_opt.bcg} is an
% optimized version of \texttt{Comp4.bcg} where the internal
% communications in the primitive (e.g. calls to attribute controllers)
% are hidden, and the graph minimised by branching
% bisimulation.
The last line is for the hierarchical construction of the full model of the application (including the scenario which triggers the election process once), and the time
includes the whole model-generation workflow.
% \texttt{Application} is synchronized with a
% \texttt{Scenario} where the election algorithm is triggered (see Section
% \ref{sec:behav-spec}).
The time needed to generate .fiacre, .exp files and scripts from VerCors is
neglectible.

\begin{table}
\caption{Behaviour graph files (all with Queue size of 3)}
\label{tab:bcg_files}  
    \begin{tabular}{|  p{5.7cm} |  p{1.4cm} |  p{1.7cm} | p{2.65cm} |}
    \hline
    Graph & States & Transitions & Computation time \\ \hline
%    Comp4.bcg 
%Behaviour of \texttt{Comp4}& 41.700.426 & 585.479.859 & 18m51.493s\\ \hline
Behaviour of \texttt{Comp4}&  3.217.983 & 45.055.266 & 2m48.520s\\ \hline
%    Comp4\_opt.bcg 
Comp4  (internal communication hidden, minimized by branching simulation)& 
%930.052 & 13.399.302 & 59m6.418s \\ \hline
90.821 & 1.306.138 & 5m23.030s \\ \hline
%    Application.bcg 
full application
& 296 & 661 & 47m1.673s \\ \hline
%    Application.bcg Q=4 & 276 & 598 & 29m24.847s \\ \hline  
    \end{tabular}
\end{table}

We use the Model Checking Language (MCL)~\cite{mateescu:inria-00315312} to express the 
behavioural properties we want to prove on our system. MCL is the input language of CADP allowing one to express various temporal logic formulas (CTL, ACTL, PDL) together with a predicate logic on data values, which will be verified on an input LTS. We make a short overview of the part of MCL syntax used in this thesis and then give examples of properties that we have checked on our use-case example. 
  
\paragraph{MCL.} Basic MCL allows specifying expressions, action formulas, regular formulas, and state formulas. An action in an MCL formula is a transition label of the model-checked LTS which represents a gate (action name) and possibly a list of offers (action parameters).
An \textit{expression} can be constructed from literals, data variables, unary and binary operators. \textit{Action formulas} are logical formulas built from action predicates, regular expressions, and boolean operators. \textit{Action predicates} provide a mechanism for specifying transition labels whose offers match a certain specification. 

Action formulas can be assembled into \textit{Regular formulas} which rely on the following elements:
\begin{itemize}
\item[$\bullet$]
 A single action formula \texttt{A} is a regular formula which is satisfied by a single transition whose label satisfies \texttt{A}. For example, a regular formula \texttt{"Call\_setValue.*"} is satisfied by all transitions whose labels start by \texttt{"Call\_setValue"}. 

\item[$\bullet$]
 Regular formulas can be concatenated using \texttt{"."} operator: a formula \texttt{R1.R2} is satisfied by a sequence of transitions which consists of two concatenated sub-sequences where the first sub-sequence satisfies \texttt{R1} and the second one satisfies \texttt{R2}.
 
\item[$\bullet$]
The operators \texttt{"*", "+", "?"} are used to express a repetition (a sequence satisfying the formula is repeated zero or more times), a strict repetition (a sequence satisfying the formula is repeated one or more times), and an option formula (a sequence satisfying the formula occurs once or does not occur at all) correspondingly. 
\end{itemize}

Finally, expressions, action formulas, and regular formulas can be assembled into \textit{state formulas} which rely on various operators and modalities and allow one to define predicates over the set of states of an LTS. In this thesis we will rely on the formulas using the following modalities and operators:

\begin{itemize}
\item[$\bullet$]
the possibility modality \texttt{"$\textless$" R "$\textgreater$" F} which is satisfied iff there exists a transition sequence (a path) in the input LTS which goes from a state satisfying a regular formula \texttt{R} to the state satisfying a state formula \texttt{F};
\item[$\bullet$]
the necessity modality \texttt{"[" R "]" F} which is satisfied iff all paths going from a state satisfying a regular formula \texttt{R} lead to a state satisfying \texttt{F};
\item[$\bullet$]
classical logical operators like "or", "xor", "end";
\item[$\bullet$]
\texttt{"true"} formula which is satisfied by any state and \texttt{"false"} formula which cannot be satisfied by any state.   
\end{itemize}

MCL allows one to define custom macros and libraries of operators parameterised by action and state formulas. Moreover, CADP includes a set of libraries with MCL operators which can be used from the user-defined formulas. Examples of such operators are given below:

\begin{itemize}
\item[$\bullet$]
\texttt{Absence\_Before(R1, R2)} is satisfied iff a sequence of transitions satisfying \texttt{R1} never occurs before a sequence of transitions satisfying \texttt{R2};
\item[$\bullet$]
\texttt{Inev(R)} is true iff all transition sequences lead to a transition sequence satisfying \texttt{R};
\end{itemize}
  

%
% MCL is a very
%expressive logic including first order predicates for the data part,
%and the alternation free $\mu$-calculus for branching time logics.
%On top of MCL, we use \emph{Specification Patterns}
%\cite{DwyerEtAl1999} for easier
%expression of some usual temporal logic properties, as in the examples below.
%We give examples of properties we proved for
%our use-case model below.
\paragraph{Examples of properties.} We used MCL in order to specify properties of our use-case example in the context of a scenario where the election algorithm is triggered only once.

An important parameter of the state-space generation is the size of
the request queues of our components. Indeed, the size of an LTS
encoding explicitly the states of a queue of max length L, receiving
N possible different values is in the order of $O(N^L)$, so it is
important to use small sizes both for the domain of request
parameters, and for the length of the queue. However, if we use a length
too small, then it is possible that the queue saturates during the
normal activity of the application. We encode a formula checking that the queue of a component cannot be saturated. Remark that this check involves the exhaustive analysis of the behaviour and interactions of the components, and this can be done only by the model-checker. To be able to detect saturation in
the model-checker,
our Queue model includes a specific event \textsf{'Comp\_ErrorQueue'}.

An example of such a formula for \texttt{Comp1} is given below:

\centerline{\textsf{\textless true* . 'Comp1\_ErrorQueue.*' \textgreater true}}
This formula means that the \texttt{Comp1\_ErrorQueue} action is reachable in the
behaviour graph. If we set the length of \texttt{Comp1} queue to 2, the
model-checker answers \texttt{true}: the queue can saturate. If we set
it to 3, the result is \texttt{false}, we are safe.

Now we do a similar query for the whole application. If the queue size of all the components is set to 3 the formula evaluates to \texttt{true}. The
model-checker gives us a diagnostic in the form of a path in the
global graph: each primitive component can drop a request in the
application queue, corresponding to calls on the client interface
\texttt{LeaderItf}. If 3 requests are not served before the $4^{th}$
arrives, then the queue saturates. If we set the composite queue
length to 4, the queue never saturates.
%\fi
%----------------------------------------------------------- END COMMENT
 


%% An equivalent formula for the queue of Application will be evaluated to \textbf{true} if the queue size if less or equal to three. As a result of evaluation, CADP provides a path in the behavior graph that leads to the Application queue saturation. According to it, if each primitive sends client request saying if it is the leader or not to outside of the composite, the requests will be dropped in the queue of Application, and if the first three requests are not forwarded before the fourth one arrives, then the queue will be saturated. We experimented also with Application queue of size four and according to CADP evaluation result in this case the queue cannot be saturated.

%% Next, in the case when queue size is equal to three for any component
%% we prove inevitability properties like the one given below. It ensures
%% that after elections were started either the leader will be elected or
%% the Application queue will be saturated. The property is evaluated to
%% \textbf{true}. \NOTE{if q=3 true, if q=2 - false}

%%Next in the configuration when queues do not saturate 
%\TODO{Eric, could you please add a ref to the specification patterns?}



Now that we have proved that our model is not limited by the size of queues, we can prove some of its functional properties. We check that after a call to \texttt{runPeterson()}, it is inevitable
(under fairness hypothesis) that either the
leader is elected or one of the queues is saturated. The model-checker
answers \texttt{true}: the election terminates.
We also proved that with adequate queue size, they never saturate.\\
%
%look at the functional correctness of the algorithm:
\centerline{\textsf{
%\noindent\textsf{[true* . 'Call\_RunPeterson' . (not 'Q\_IAmTheLeader.*')*]}\\
['Call\_RunPeterson'] Inev ('Q\_IamTheLeader.*' or 'ErrorQueue.*')
}}
%\centerline{
%\noindent\textsf{\textless(not 'Q\_IAmTheLeader.*')*. 
%('Q\_IAmTheLeader.*') \textgreater true}
%}
%
%This means that 

Then, we prove that the event \texttt{Q\_IamTheLeader} is 
emitted only once (i.e. only one leader is elected):\\
\centerline{\textsf{Absence\_Before ('Q\_IamTheLeader.*', 'Q\_IamTheLeader.*')"}} 
%%[ true* .  'Q\_IAmTheLeader.*'] not (\textless true* . %'Q\_IAmTheLeader.*'\textgreater 
%%%true)}}
%The property checks that no component can claim itself as the leader before one of the 
%components declares itself as the leader.

Recall that in order to illustrate the future-based communications, we extended our use-case with the methods \texttt{requestKey} and \texttt{encrypt}. The former is a client method invoked by the leader component in order to obtain an encryption key. Since it is a client method, the variable which should store the received result in the leader component is a future. Hence, the leader component should not get blocked after the method invocation but continue performing computations which do not involve the encryption key. In particular, just after asking for the encryption key, the leader component invokes the \texttt{IAmTheLeader} method on its client interface in order to report that it is the leader.
In order to check that the communications in the generated graph are
indeed implementing futures properly, 
%we have to prove that the  \texttt{IAmTheLeader} method can be invoked even if the key has not been received yet. 
we verify the following formula
which states that a key must be always received before IamTheLeader() is
invoked:\\ 
%\centerline{\textsf{\textless'Q\_requestKey.*'.(not 'R\_requestKey.*')* .'Q\_IamTheLeader.*' . %'R\_requestKey.*'\textgreater true}}
\centerline{\textsf{Existence\_Between('R\_RequestKey.*', 'Q\_requestKey.*', 'Q\_IamTheLeader.*')}}

The model-checker answers \texttt{false} and provides an example of system behaviour where \texttt{IamTheLeader()} method is invoked before the key is received. This proves that a component is not blocked if the key is not needed.


