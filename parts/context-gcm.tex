\section{The Grid Component Model}
\label{sec:context-gcm}
The approach presented in this work relies on the Grid Component Model (GCM).
GCM was designed by the CoreGrid European network of Excellence \cite{BCDGHP:Telecom08} as an extension of the Fractal model~\cite{Bruneton:2006:FCM:1152333.1152345} dedicated to distributed systems, including a set of control capabilities. The framework targets the design, implementation, execution, and deployment of hierarchical reconfigurable large-scale component-based applications.    
%
%There are several points that make the GCM model 
%
In this section, first, we provide an overview of GCM. Then, we describe an XML-based language for the GCM architecture specification. Finally, we introduce the ProActive middleware providing an implementation of GCM components based on Active Objects.

\subsection{GCM overview}

A GCM-based application consists of components, interfaces and bindings. We describe each of the elements below and illustrate them in Figure \ref{fig:context-gcm}. We do not explain the graphical notations here as they will be presented in details in Section~\ref{sec:vce-graphical-formalism}.

\begin{figure}
     \centering
     \includegraphics[width=10cm]{drawings/context-gcm.pdf}
     \caption{A GCM application}
     \label{fig:context-gcm}
 \end{figure}

\paragraph{Hierarchical components.} There exist two types of components in GCM depending on the level of observation: primitive and composite components, we will call them "primitives" and "composites" correspondingly. A \textit{composite} encompasses inner components which are called \textit{sub-components}. A composite (\texttt{Application} in Figure \ref{fig:context-gcm}) is separated into two parts: a \textit{membrane} (the grey part) containing all the subcomponents dealing with the application management and control, and a \textit{content} (the white part) which comprises the subcomponents implementing the business logic. Another component type - \textit{primitive} (\texttt{TaskDistributor, Worker1} in Figure \ref{fig:context-gcm}) - could be seen as a black-box view on a component that encapsulates the implementation code and provides some functionality. Each primitive also has a componentised membrane responsible for control and management. A primitive can comprise attributes of primitive type that will be accessible from outside of the component.
Each component is characterised by a name and a set of interfaces.

\paragraph{Interfaces.} The GCM interfaces are the communication points for the components. The communication between components is performed in the form of method invocation. An interface able to invoke methods and to receive the invocation results is called a \textit{client} interface (e.g. \texttt{C1}), and interfaces that accept method invocations and send back the results are called \textit{server} interfaces (e.g. \texttt{S1}). The interfaces that call or serve methods implementing the business logic are called \textit{functional} while the ones dealing with the application control are called \textit{non-functional}. The interfaces that communicate are connected by \textit{bindings} (the black arrow from C1 to S1). 
An interface accessible from outside of a component is said to be \textit{external}. An \textit{internal} interface is reachable only from inside of a component. 

\paragraph{Collective communications.} In order to facilitate parallel programming, GCM defines the \textit{cardinality} of an interface which can be \textit{singleton}, \textit{multicast} or \textit{gathercast}. 
%
A \textit{singleton} is the simplest interface used for one-to-one communications. A client singleton can be connected to only one target interface at each time in order to ensure the deterministic behaviour. However, a singleton server interface can be bound to several client interfaces, but a request from each client is processed separately. 
%
 A \textit{multicast} provides an abstraction mechanism for the one-to-N communications. It is a client interface that transforms a single method invocation into several requests and sends them to multiple target interfaces at the same time. If the method is supposed to return a result, the replies from the target components are collected in a single structure and given back to the caller.
 %
 An abstraction for N-to-one communication is provided by the gathercast interfaces. A \textit{gathercast} is a server interface that can receive calls from several clients at the same time, the calls are assembled in a single request that is processed by the gathercast interface owner.

\paragraph{The separation of concerns.}

The functional part of a GCM
  application can be separated from the control part thanks to the
  separation of a composite component into a membrane and a content
  and to the usage of functional and non-functional interfaces. This separation
  ensures, as much as possible, the independence of the business code from the management code. 

\paragraph{The non-functional aspect.} 
%By the non-functional aspect, we understand all system functionalities that are not related to the business logic, such as management, monitoring, control. 

The GCM was design to support wide range of non-functional capabilities which can be implemented thanks to the three core elements: predefined controllers, non-functional components, and reconfiguration mechanisms. 

The GCM specification describes a set of predefined entities used for an application management including:

\begin{itemize}
\item[$\bullet$]
A \textit{Lifecycle controller} is included in every component and serves to stop and start a component.
\item[$\bullet$]
A \textit{Attribute controller} is used to configure the values of the component attributes. 
\item[$\bullet$]
A \textit{Binding controller} is used to bind or unbind singleton interfaces.
\item[$\bullet$]
\textit{Multicast and gathercast controllers} are used to reconfigure (bind/unbind) multicast and gathercast interfaces.
\item[$\bullet$]
\textit{Content and membrane controllers} are used to add subcomponents in a content or a membrane correspondingly.
\end{itemize}

Additionally, the user can define his custom component-controllers (also referred as non-functional components) and place them in the membrane of the managed component (e.g. \texttt{Controller} in Figure \ref{fig:context-gcm}). The component-controllers can be primitives or composites; they have access to the predefined controllers (Lifecycle controller, Binding controller, etc) and to the host component. The programmer can make a component-controller accessible from outside of the composite through non-functional interfaces, or use interfaces  and bindings in order to reach the content subcomponents from the component-controller. In fact, specification of the non-functional part of a composite is as flexible as the design of the business logic. 

Thanks to the set of features described above, a GCM application architecture can evolve at runtime; this includes binding reconfiguration, component start and stop, adding or removing subcomponents at different levels of hierarchy. The reconfiguration capabilities provide a mechanism for application adaptation: a system can analyse the current state and change its structure depending on the current needs 
as demonstrated in \cite{DBLP:journals/spe/BaudeHR15}.

\paragraph{Interceptors.}
Sometimes, the information should be shared
  between the functional part and the controllers of the application.
  Interceptors are specific components inside the membrane that can
  intercept the flow of functional calls in order to trigger reaction
  from the non-functional aspects.
   For example, in the application from Figure \ref{fig:context-gcm},
  the \texttt{Monitor} component monitors the number of
  requests sent to the \texttt{TaskDistributor} and forwards the information to the \texttt{Controller}. The controller can, for example, add more workers to the system if the amount of requests is greater than a given threshold. It illustrates very well what is
  an interceptor.
Several interceptors can sequentially intercept the same functional call. The interceptors are discussed in details in Section~\ref{sec:formalisation-interceptors}.
%  
%  
%  In the membrane, functional
%  interfaces are directly connected, %(see the interface \textbf{G1} for example), 
%  however one or several interceptors can be inserted within such a
%  binding and interact with other non-functional components (see the
%  non-functional binding between \texttt{Monitor} and \texttt{Collector} in
%  the example).
%\newline
%conclusion and properties of GCM

%\subsection{GCM ADL}
%
%The architecture of a GCM application can be described in an XML-based language called \textit{Architecture Description Language} (ADL). 

\subsection{GCM/ADL}

A GCM-based application architecture can be specified in an XML-based format called architecture description language (ADL). Listing \ref{lish:adl-example} demonstrates an ADL file of the application depicted on the Figure \ref{fig:context-gcm}. Its root element \textit{definition} represents the  
root component of the modelled system and in our example corresponds to a composite. The specification of a composite includes the external functional interfaces (line 2), the subcomponents of the content (lines 3-14), the bindings of the content (lines 22-23) and the description of the membrane (lines 15-21) which includes the non-functional part. The definition of a primitive is demonstrated by lines 3-11. It is very similar to a composite except that instead of components nested in a content it has a reference to  implementation class (line 6). The specification of a functional interface should include a reference to all interceptors that monitor its calls if there are any. The component attributes reachable from outside should be declared in ADL (lines 7-9). The definition of subcomponents can be alternatively given in a separate ADL file and referenced from the root component.
Lines~2,~4-7 include references to Java classes and interfaces which will be detailed in the next section.
% as they are specific to the ADL implementation in the concrete middleware.

\begin{lstlisting}[language=XML, numbers=left, rulecolor=\color{black}, caption={ADL example}, label = {lish:adl-example}, basicstyle=\scriptsize]
<definition name="Application">
    <interface name="s_it1" role="server" signature = "ServerInterface" interceptors="Monitor.MS"/>
    <component name="TaskDistributor">
    	<interface name="S1" role="server" signature = "ServerInterface"/>
        <interface name="C1" role="client" signature = "ClientInterface"/>
        <content class="TaskDistributorClass"/>
        <attributes signature="TDAttributeController">
            <attribute name="_myId" value="1"/>
        </attributes>
        <controller desc="primitive"/>
    </component>
    <component name="Worker1">
    	...
    </component>
    <controller desc="composite">
    	<interface name="s_it2-controller" role="server".../>
    	<component name="Controller">
    	...
    	</component>
    	<binding client="this.s_it2-controller" server="Controller.s_it1"/>
    </controller">   
    <binding client="TaskDistributor" server="TaskDistributor.s_it2"/>
    ... other bindings
</definition>
\end{lstlisting}

\subsection{GCM/ProActive}
\label{subsec:context-proactive}
%There exist several implementations of GCM. Among them, we chose the one provided in the ProActive platform which fully implements the specification of GCM components including reconfiguration c

The ProActive platform \cite{ProActive} is a Java middleware for programming distributed and concurrent applications. The core notions of this framework are the active objects and communication paradigm based on request-reply by futures \cite{DBLP:journals/spe/BaudeHR15}; we discuss both of them below. The ProActive platform is important for this work because it provides a reference implementation of GCM which is used in order to run the executable the code generated by VerCors.

\paragraph{Active objects and request-reply by futures.} An active object~\cite{Lavender:1996:AOO:231958.232967} is a unit of distribution in ProActive and represents a normal Java object that additionally has a queue of pending requests, a body that has the ability to decide in which order the requests are served, and a control thread. It is an independent activity that can be addressed remotely and encapsulates its state: only the unique thread of the active object can modify its state. The active objects communicate asynchronously and we describe in details the communication paradigm below. 

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/context-futures-call.pdf}
  \caption{Remote method invocation}
  \label{fig:context-futures-call}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/context-futures-execute.pdf}
  \caption{Asynchronous execution}
  \label{fig:context-futures-execute}
\end{subfigure}\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/context-futures-block.pdf}
  \caption{Wait-by-necessity}
  \label{fig:context-futures-block}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{drawings/context-futures-continue.pdf}
  \caption{Result return}
  \label{fig:context-futures-continue}
\end{subfigure}
\caption{Request-reply by futures}
\label{fig:context-futures}
\end{figure}

%Now, let us assume that \texttt{TaskDistributor} and \texttt{Worker} are active objects. 
We rely on an example shown in Figure \ref{fig:context-futures} which illustrates the behaviour of two active objects. Here, \texttt{TaskDistributor} and \texttt{Worker} are active objects but not primitives as in the previous example. Whenever TaskDistributor invokes a method on Worker (Figure \ref{fig:context-futures-call}), a short rendez-vous occurs and the caller gets blocked until the corresponding request is dropped in the queue of the callee. This rendez-vouz is used to ensure a causal ordering of requests, The calls between active objects are asynchronous meaning that the requester can continue its execution. If the method is supposed to return a result, the requester creates a so-called \textit{future} object that does not store any value initially but is ready to receive the result of the remote method invocation. The requester continues its execution as long as it does not need the value of the future (Figure \ref{fig:context-futures-continue}). Once the value is required, the requester checks whether the result was obtained or not. If the result is available, the caller can use it and proceed the execution. Otherwise, the requester gets blocked waiting for the actual value (Figure \ref{fig:context-futures-block}); such mechanism is called \textit{wait-by-necessity}. If an active object calls one of its own methods (a \textit{local} method), the invocation is synchronous.

In fact, the communications in GCM/ProActive can rely on so-called \textit{first-class futures}. A first-class future is a future object which can be passed as an argument of a remote method call even if its value is not known. In this thesis we do not consider the first-class futures.
%In this thesis we rely only on the mono-threaded active objects, but multi-threaded active objects are also implemented in ProActive~\TODO{cite something}.

\paragraph{From active objects to components.} From the high-level point of view, a GCM/ProActive primitive is implemented as an active object that comprises all the features discussed above. By default, the incoming requests are served in FIFO order, but this can be modified by the programmer. Figure \ref{fig:context-gcm-pa} illustrates treatment of requests by the GCM/ProActive components. The body of a primitive selects a request from the queue and forwards it to its Java class that implements the corresponding behaviour of the primitive. When the request has been processed, the body selects the next request from the queue. If a primitive invokes a remote method, the call is forwarded from the Java class to the client interface and then to the target component. If the method is supposed to return a result, the corresponding future object is created.  
%
A composite is also an active object and it has an implementation class that can be used for configuring some specific parameters, but it does not implement any logic. All requests to the external server interfaces are first dropped in the queue of the composite and then the body forwards them to the subcomponents that are bound to the corresponding server interface. The client requests from the subcomponents to outside of the composite are also dropped in the queue of the composite and then distributed among the corresponding client interfaces.

\begin{figure}
     \centering
     \includegraphics[width=12cm]{drawings/context-gcm-pa.pdf}
     \caption{Request treatment by GCM/ProActive components}
     \label{fig:context-gcm-pa}
 \end{figure}

More technically, in order to wrap an active object into a GCM primitive, the programmer has to decide which methods of the active object can be called externally, or, in terms of GCM, which methods of the primitive will be accessible on the external server interfaces. 
Those methods are then composed into Java interfaces. Each Java interface will be later associated to possibly multiple GCM interfaces and there is no correlation between their names.
Then, the programmer writes a Java class implementing the behaviour of the primitive. The class is supposed to implement the Java interfaces associated with the GCM server interfaces of the primitive and a set of auxiliary interfaces. 
An example of a possible implementation of the \texttt{TaskDistributor} primitive from Figure~\ref{fig:context-gcm} is given in the Listing \ref{list:prim-example}. The class should at minimum include the following definitions:

\begin{itemize}
\item[$\bullet$]
the references to the client interfaces (line 7);
\item[$\bullet$]
the local variables;
\item[$\bullet$]
the attributes that will be exposed by the Attribute Controllers (line 5);
\item[$\bullet$]
the methods to access the attributes (lines 35-36);
\item[$\bullet$]
the methods that are exposed on the server interfaces (lines 10-15);
\item[$\bullet$] 
the auxiliary methods dealing with the client interfaces and bindings (lines 17-32);
\item[$\bullet$]
the local methods
\end{itemize}
%
%\begin{lstlisting}[float,language=Java, numbers=left, rulecolor=\color{black}, caption={A GCM/ProActive Java interface}, label = {list:prim-example}, basicstyle=\scriptsize]
%public interface ServerInterface{	
%	public int run();
%}
%\end{lstlisting}

\begin{lstlisting}[float,language=Java, numbers=left, rulecolor=\color{black}, caption={A Java class of a GCM/ProActive primitive}, label = {list:prim-example}, basicstyle=\scriptsize]
public class TaskDistributorClass implements  Serializable, BindingController,
 TDAttributeController, ServerInterface{
	
	//an attribute accessible from outside
	public int myId;	
	//client interfaces
	public ClientInterface C1;
	
	//methods of the server interfaces
	public int run() {
		IntWrapper x = C1.task1();
		//do somethong
		int y = x.getValue();
		return y;
	}
	
	//auxiliary methods for the management of bindings and interfaces
	public void bindFc(String myClientItf, Object serverItf) {
		switch(myClientItf) {
		case "C1": C1 = serverItf;
	...}
	public String[] listFc() {
		return new String[1]{"C1"};
	...}
	public Object lookupFc(String myClientItf) {
		switch(myClientItf) {
		case "C1": return C1;
	...}
	public void unbindFc(String myClientItf) throws NoSuchInterfaceException {
		switch(myClientItf) {
		case "C1": C1 = null;
	...}
	
	//methods to manage the attributes
	public void set_myId(int newId) {...}
	public int get_myId() {...}
}
\end{lstlisting}

\paragraph{Multi-threaded active objects and primitive components.}
Significant work has been done on the theoretical and practical aspects of the multi-threaded active objects \cite{Henrio2013, henrio:hal-00916293} which are already implemented in ProActive and can serve as the basis for the multi-threaded GCM primitives. The work presented in this thesis supports only single-threaded primitives, but specification and analysis of the multi-threaded primitives is kept for the future work.

\paragraph{Constructing and using components.} There are two ways to construct a GCM application in ProActive: either manually or using a factory. In the first case, the programmer invokes the GCM/ProActive API to create components with all necessary characteristics, to add subcomponents in the composites and to connect interfaces with bindings. This approach is not applied in this thesis. Instead, we rely on the second method where an application is constructed by a dedicated \textit{factory}. The factory takes an ADL file with the system architecture, a set of Java classes implementing primitives' behaviour, a set of Java interfaces declaring the methods of the GCM interfaces and creates automatically all necessary elements including components at different levels of hierarchy controllers and bindings. 

When we explained the structure of an ADL file, we did not mention how it is linked to the actual implementation because this is related to the underlying middleware. In the case of a GCM/ProActive ADL (example in the Listing \ref{lish:adl-example}), the programmer has to associate a primitive and a Java class implementing its behaviour (line 6), a GCM interface and a Java interface with its method signatures (lines 4, 5), a list of attributes of a primitive with a Java interface providing access to them (line 7). 
%
%The XML-based description of a GCM system is called an \textit{ADL} (for Architecture Description Language) file. Listing \ref{lish:adl-example} demonstrates the ADL file of the application depicted on the Figure \ref{fig:context-gcm}. Its root element \textit{definition} represents the  
%root component of the modelled application and in our example corresponds to a composite. The specification of a composite includes the external functional interfaces (line 2), the subcomponents of the content (lines 3-14), the bindings of the content (lines 24-25) and the description of the membrane (lines 16-22) which includes the non-functional part. \textit{Signature} of an interface links Java and GCM interfaces. The definition of a primitive is demonstrated by lines 3-11. It is very similar to a composite except that instead of components nested in a content it has a reference to the implementation class (line 6). The component attributes reachable from outside should be declared in ADL (lines 7-9). \textbf{TDAttributeController} is a user-defined interface that provides access to the attributes of a primitive.    
%
%\\\\\\\\\\\\\\\\\\\\\\\\\\\\

Given an ADL file, Java classes and interfaces, the GCM/ProActive factory returns an instance of the root component as illustrated in the Listing \ref{list:construct-use}. Here, line~1 gets an instance of the factory which will create the component. The factory is asked to construct a new component at line~5; it takes two arguments: the \texttt{context}  which stores the description of the deployment infrastructure (we omit the details here) and the name of the ADL file. Next, the constructed component should be ''started''. GCM/ProActive has an implementation of the Lifycycle controller able to start a component as illustrated at line~6.  
% \ref{list:construct-use} 
%at line 4 (the \textit{context} variable is related to the deployment configuration). 
The user can then get a server interface of the constructed component by name (line~7) and invoke its methods (line~8). 
Additionally, the user can use the standard API to modify the root component and bind it to the other components.
  
\begin{lstlisting}[float,language=Java, numbers=left, rulecolor=\color{black}, caption={A GCM/ProActive component construction and access}, label = {list:construct-use}, basicstyle=\scriptsize]
Factory f = FactoryFactory.getFactory();
String adl = "Application.adl";
Map<String, Object> context = new HashMap<String, Object>();
//fill the context with the deployment data
Component application = (Component) f.newComponent(adl, context);
GCM.getLifeCycleController(mainComponent).startFc();
RunItf itf = (RunItf)(component.getFcInterface("s_it1"));
itf.run();
\end{lstlisting}

\textit{Overall, specifying GCM/ProActive applications manually can sometimes require significant effort as the programmer has to take care of the coherency between the ADL description, Java classes and interfaces. In order to facilitate the development process, VerCors generates all elements of the application automatically from the user-defined graphical specification.}

\paragraph{Distributed deployment.} The GCM components can be deployed in a distributed manner where a primitive component is a unit of distribution. The underlying infrastructure is specified as a composition of \textit{virtual nodes} that express the abstract references to the resources where the components will be deployed. The virtual nodes are then associated to the exact physical infrastructure. The information about virtual nodes can be specified either with Java API or in the ADL file. The physical infrastructure should be provided in an XML-based file and
given to the GCM factory when the component is being constructed (lines 3-5, Listing \ref{list:construct-use}.

\paragraph{Collective communications.}
GCM/ProActive provides an API for multicast and  gathercast interfaces with the corresponding controllers and several policies on dispatching request arguments and assembling results. At the current stage, the VerCors platform deals \textit{only with the multicast interfaces for which an outgoing request is replicated and sent to all the bound targets and the results are collected in a list and given back to the requester}. Implementing other policies will be necessary in the future work.

\paragraph{Non-functional aspect.} The predefined controllers are implemented in GCM/ProActive and the user has access to them. For example, line~6 of the Listing \ref{list:construct-use} demonstrated invocation of a Lifecycle controller that starts a component. Except from that, the user has two ways to implement his customized controllers: as object-controllers or as component-controllers. Both of them should be inserted in the component membrane and specified in the ADL file. As opposed to the objects, the component-controllers can have hierarchical structure and communicate with the other subcomponents.  ProActive implements the reconfiguration primitives defined in the CM specification.