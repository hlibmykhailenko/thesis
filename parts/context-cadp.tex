\section{CADP - a toolbox for construction and analysis of distributed processes}

CADP (for Construction and Analysis of Distributed Processes)\cite{DBLP:journals/sttt/GaravelLMS13} is a toolsuite for the formal modelling, simulation and analysis of parallel asynchronous systems; it has been proven efficient by multiple case-studies \cite{abid:hal-00863262, 185156, 6176552}. The framework relies mainly on LOTOS \cite{Bolognesi:1987:IIS:44211.44214} and LotosNT \cite{LNT} \TODO{find a better ref} as system specification languages and 
%networks of communicating automata 
LTSs as the abstraction model. We describe below some of the tools included in CADP and we start by the modules applied in this work. 

%\subsection{The CADP tools applied in this work}

\paragraph{Caesar and Caesar.adt} are compilers that translate the behavioural and the data parts of a LOTOS specification correspondingly either into an LTS that can be verified or into a C program to be executed or simulated.   

\paragraph{BCG} (Binary-Coded-Graphs) is both a format for LTSs and a set of dedicated libraries. The advantage of the format is that compared to the LTS represented in ASCII, BCG graphs can take up to twenty times less space. Among variety of tools, the library includes:
\begin{itemize}
\item[$\bullet$]
\textbf{bcg\_draw} - a module for graphical representation of the graphs;
\item[$\bullet$]
\textbf{bcg\_min} - a tool for graph minimization by strong or branching bisimulation;
\item[$\bullet$]
\textbf{bcg\_labels} allows hiding and renaming labels;
\item[$\bullet$]
\textbf{bcg\_info} gathers information about a graph such as its size, the number of states and transitions, the list of labels, etc. and privides it to the user; 
\end{itemize}

\textbf{Exp.open} \cite{Lang2006} takes a \textbf{.exp} file encoding a network of communicating automata and constructs its global behaviour graph. The automata are represented as .bcg files and composed together  in parallel using different techniques including \textit{synchronisation vectors}, on which  we rely in this thesis. Listing \ref{list:exp-example} demonstrates an example of an .exp file with two synchronisation vectors (lines 1-2). Line 3 lists the names of the .bcg files of the operating in parallel LTSs that will be composed. A LOTOS action label is divided into two parts: \textit{gate} is the action name and \textit{offers} is the list of action parameters. 
The vector at line 1 should be interpreted as follows: if the LTS \texttt{A} performs an action with gate \texttt{a1}, it should synchronise with \texttt{B} performing an action with gate \texttt{b1} and the behaviour of \texttt{C} is not taken into account. This will result in a global action with gate \texttt{g1}. The offers of synchronised actions should have parameters with exactly the same types and values. Those parameters will be added to the global action.

\begin{lstlisting}[float,language=Java, numbers=left, rulecolor=\color{black}, caption={An example of synchronization vectors in .exp}, label = {list:exp-example}, basicstyle=\scriptsize]
   < a1, b1, - > -> g1,
   < -, -, c1 > -> g2
   A | B | C
\end{lstlisting}

\paragraph{The model checking language}
 (MCL) is a regular alternation-free $\mu$-calculus with actions, predicates and expressions over action sequences equipped with  parametrised fixed-point operators and regular expression. The formalism allows direct specification of branching-time logics formulas like ACTL \cite{DeNicola1990} and CTL \cite{Clarke:1986:AVF:5397.5399} and of regular logics formulas like PDL \cite{FISCHER1979194} interpreted over LTSs. A strong advantage of MCL is the ability to encode manipulations with data variables. Three types of formulas can be encoded with the language. First, action formulas are built upon action predicates and boolean operators; second, regular formulas are constructed from action formulas and regular expression operators (such as choice, counting, repetition and others). Finally, state formulas are built from boolean operators, modalities, fixed point operators, and data-handling constructs. In \cite{Mateescu2003255} the authors demonstrate the expressiveness of the formalism by encoding safety, liveness (potential reachability, inevitability, deadlock freedom), fairness properties.
In order to facilitate formula specification, MCL is equipped with a library of property patterns \cite{Dwyer:1999:PPS:302405.302672} such as absence, precedence, bounded existence, and others. The language allows one to construct his own reusable libraries of temporal operators. 
 
\paragraph{Evaluator} \cite{Mateescu2003255} is an on-the-fly model-checker and it is the core CADP engine used in this work. The tool takes as input an LTS expressed in various formats including binary graphs, LOTOS, LotosNT, EXP, 
%a network of communicating automata expressed as a binary graphs (so-called \textbf{.bcg} files  
% 
and a temporal logic property to be checked in MCL \cite{mateescu:inria-00315312}. Then, the input is translated into a boolean equation system \cite{ANDERSEN19943} which is solved using algorithms explained in \cite{Mateescu2003255}. 

Evaluator answers either TRUE or FALSE depending on whether the property is satisfied and possibly provides  diagnostics which can be either an example or a counterexample.    
%The input automata can be composed 

\paragraph{SVL} (Script Verification Language)~\cite{Garavel2002} is a high-level scripting language aiming at facilitating program verification with CADP. The language includes operators for model-checking, label hiding, renaming, and state-space reduction.

\paragraph{Ocis} is an interactive graphical simulator. It takes an automaton as an input and visualises the execution tree of a system, the execution traces and the communication between the parallel processes. The user can manually step-by-step navigate through the execution scenario and save the simulation results as a .bcg graph.

\paragraph{State-space reduction} is implemented in CADP  at two levels and employs the notion of so-called  "tau-transitions" or "hidden transitions", i.e. the transitions that exist in the behaviour graph but do not need to be observed during model-checking. The first approach called \textit{partial reduction} simply compresses and merges such transitions. The second approach - \textit{total reduction} consists in merging several states of a partially reduced LTS into one state by applying bisimulation techniques.
% Another technique implemented in CADP is state-space minimisation by branching bisimulation 
Such minimisation approach is particularly useful for hierarchical systems like GCM applications because it allows one to benefit from hiding some details of the behaviour of the processes at the low-levels of hierarchy. Also, as we will explain later, many processes inside GCM components have bisimular behaviour that can be merged during state space minimisation. 

\paragraph{The application of CADP tools in this work.}
To wrap-up, in this work we will apply the discussed CADP tools as follows. We will express LTSs in \texttt{.bcg} and synchronisation vectors in \texttt{.exp}, we will apply \textit{exp.open} for the composition of automata. We will use the \textit{bcg\_min} to minimise the state space as we will hide a lot of communications that should not be observed during verification. We will specify system properties in \textit{MCL} and model-check them with \textit{evaluator}. Finally, we will take advantage of the user-friendly \textit{svl} script for invoking various CADP modules and debug our systems with \textit{bcg\_info}, \textit{bcg\_draw}, \textit{bcg\_labels}, and \textit{ocis}.   

%\subsection{Other CADP tools}
\paragraph{}
The following two modules are not applied in this thesis but we plan experimenting with them in the future work.

\textbf{Distributor}~\cite{Garavel2013145} is a tool for distributed state-space construction, it splits the generation over N machines and each of them builds a fragment of the resulting LTS. The fragments are then assembled by \textbf{bcg\_merge}. 

\paragraph{Projector} abstracts the behaviour of a graph by synchronizing it with the restricted version of the behaviour of its interfaces. 



\label{sec:context-cadp}