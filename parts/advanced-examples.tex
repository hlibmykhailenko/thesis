\section{Examples}
\label{sec:advanced-examples}

In this section we introduce the examples of two projects created in the VerCors platform. In the first example we present a hierarchical software system with component-controllers that reconfigure multicast interfaces and with attribute controllers. The second example illustrates the usage of interceptors.

\subsection{Composite pattern}

\paragraph{The problem statement.} In order to test our approach, we designed, model-checked, and generated the code of a refined version of an application which was proposed as a challenge problem at the SAVCBS\footnote{http://www.eecs.ucf.edu/~leavens/SAVCBS/2008/challenge.shtml} workshop on specification and verification of component-based systems in 2008. Later, the problem statement was published in~\cite{vacid} as a part of a set of benchmarks for verification tools. The problem is dedicated to the \textit{composite pattern} which is very common in component- and object-oriented programming. According to the challenge, there exists a tree of components (in GCM terms we call it a "hierarchy of components"), and a client has a uniformed interface to access any sub-tree. Each composite in the tree has a counter \texttt{childrenNum} which stores the number of its sub-components at all levels of hierarchy. The client can add a sub-component anywhere in the system, and this should increment the value of \texttt{childrenNum} for all its ancestors. The challenge is to keep the value of \texttt{childrenNum} up-to-date.

We modelled the discussed application in VerCors. Since we plan to apply a finite state-space model-checker, we cannot allow the client to add an infinite number of sub-components. Instead, we model a system with three levels of hierarchy where each composite (except from the leaves) can have three functional sub-components and one non-functional sub-component responsible for the reconfiguration and for storing the \texttt{childrenNum} variable.  

\paragraph{Graphical design.}

Figure~\ref{fig:composite-vce} illustrates the component diagram of our use-case example; for the sake of simplicity we hide the internal structure of most of the components. The root component \texttt{Comp1} has three composite sub-components with an identical structure: \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13}. Each of them also has three sub-components with an identical structure. Each composite component (except from the ones at the lowest level of hierarchy) has an internal multicast interface \texttt{C1} which can be bound to the three functional sub-components. All the bindings going from the multicast interfaces are indexed. One can notice that the bindings are dashed which means that they do not exist when the application is launched but they can be added at run-time. In fact, the current version of VerCors does not allow modelling applications with dynamically created components. Instead, we consider a component to be "added" in the tree when it is bound to the multicast interface \texttt{C1} of its container. Sub-components cannot be added to the composites at the lowest levels of hierarchy (i.e. to \texttt{Comp111}, \texttt{Comp112}, etc).

\begin{figure}
\centering
    \includegraphics[width=\linewidth]{drawings/composite-vce.pdf}
 \caption{VerCors model of the composite pattern}
 \label{fig:composite-vce}
\end{figure}

Each composite has a component-controller responsible for adding sub-components. The behaviour of the component-controllers at the lowest levels of hierarchy is modelled by the class \texttt{LeafClass}. The behaviour of the component-controllers of Comp11, Comp12, and comp13 is modelled by \texttt{NodeClass}. The root controller of the root component is implemented by \texttt{RootClass} which extends \texttt{NodeClass}; all the classes are illustrated in Figure~\ref{fig:composite-classs}. 
\texttt{NodeClass} has three attributes: \texttt{myId} is the unique identifier, \texttt{childrenNum} is the number of all functional sub-components at the lower levels of hierarchy, and \texttt{isBound} represents the current status of the internal multicast interface \texttt{C1} of the encompassing composite. The latter attribute is an array of three boolean values. The value at a given index is \texttt{true} if the outgoing binding with the corresponding index is bound. Otherwise, it is \texttt{false}. Since no outgoing binding is initially bound to the multicast interfaces, \texttt{isBound} is initially equal to \texttt{[false, false, false]} for all the components.

\begin{figure}[t]
\centering
    \includegraphics[width=10cm]{drawings/composite-classes.pdf}
 \caption{The class diagram of the composite pattern}
 \label{fig:composite-classs}
\end{figure}


Each component-controller has a server method \texttt{addSubcomp} accessible from outside of its composite. The method is responsible for adding a sub-component and it takes one input parameter \texttt{parentId} which represents the identifier of the component where a sub-component should be added. If, for instance, the client would like to bind a functional sub-component inside \texttt{Comp12}, he would invoke the method with the argument equal to one. \texttt{addSubcomp} returns \texttt{true} if a sub-component has been successfully added in the component on which the method was invoked or in any of its sub-components.
The method is implemented differently for \texttt{NodeClass} and for \texttt{LeafClass} because the former cannot add any sub-component to the leaves of the hierarchy. 
%even if the value of the input parameter is equal to the identifier of the component-container.  

Figure~\ref{fig:composite-addsubcomp-sm} illustrates a state machine modelling  \texttt{addSubcomp} of \texttt{NodeClass}. First, in state \texttt{Choice6} it checks if the received \texttt{parentId} equals its own identifier. Then, two options are possible:

\begin{figure}[t]
\centering
    \includegraphics[width=\linewidth]{drawings/composite-addsubcomp-sm.pdf}
 \caption{addSubcomp method}
 \label{fig:composite-addsubcomp-sm}
\end{figure}

\begin{itemize}
\item[$\bullet$]
If the two values differ, \texttt{addSubcomp} request is sent to the sub-components through the multicast interface \texttt{C1}: \texttt{ar:=addSubComp(parentId}. This invokes \texttt{addSubcomp} on all the sub-components bound to \texttt{C1}. Then, we iterate over the array of received responses in order to check whether one of them is equal to \texttt{true}. If such a response is found, the \texttt{childrenNum} value of the current component is incremented, and the method returns a positive reply. Otherwise, it returns \texttt{false}.
%The iteration is possible thanks to the construct \texttt{ar.length} which provides the length of the 
\item[$\bullet$]
If the input parameter is equal to the identifier of the current component, it means that a new sub-component should be added inside the encompassing composite. However, before doing the reconfiguration, we have to check that adding a sub-component is still possible. Recall that our application can have only finite number of components, and we modelled our system so that not more than three sub-components can be added inside a composite. The check is done by \texttt{addAnyUnbound} local method which is invoked by the transition \texttt{/isBound:=this.addAnyUnbound()} and illustrated in Figure~\ref{fig:addAnyUnbound}. The method iterates over the \texttt{isBound} array in order to find the index of a binding which has not been bound yet. If such an index exists, the method binds the binding at the corresponding index (\texttt{/parent.bind(Interfaces.C1, index)}, sets the value of \texttt{isBound} at the index to \texttt{true} in order to remember that the binding has been added, and returns \texttt{true} to \texttt{addSubcomp} in order to report that a sub-component has been successfully bound. \texttt{addSubcomp}, in its turn, increments the value of \texttt{childrenNum} and returns \texttt{true}. If all three sub-components have been bound before, the method returns \texttt{false} and does not modify the counter of the number of children.   
\end{itemize}


\begin{figure}[t]
\centering
    \includegraphics[width=11cm]{drawings/composite-bindany-sm.pdf}
 \caption{addAnyUnbound method}
 \label{fig:addAnyUnbound}
\end{figure}

A sub-component cannot be added at the lowest levels of hierarchy. Hence, \texttt{addSubcomp} of \texttt{LeadClass} always returns \texttt{false} and does not perform any additional actions.

The \texttt{addSubcomp} method of the server interface \texttt{S1} of \texttt{Comp1} is the entry point of the modelled application. If the user wants to add a sub-component somewhere in the hierarchy, he should invoke this method with the identifier of the target component. In addition, each component-controller has a method \texttt{foo}. The method does not encapsulate any logics, and we will use it later for the debugging purposes. Whenever it is possible, the \texttt{foo} method invokes the \texttt{foo} method on the internal multicast interface of the composite-container, thus propagating the call to the sub-components.

Finally, the root composite has an interface \texttt{S2} with the \texttt{computeChildren} method which returns the value of \texttt{childrenNum} stored by its component-controller.

\paragraph{Model-checking and executable code generation.}

We used VerCors in order to generate the pNets of the modelled application and to translate them into the input for CADP. To reduce the global state-space of the system, we synchronised it with the scenario illustrated in Figure~\ref{fig:composite-scenario-sm}. It adds random number of sub-components to random composites in the application (there are no guards on the transitions going from the choice state, hence, each time the next transition is chosen non-deterministically). Then, the scenario requests the value of \texttt{childrenNum} of the root component and invokes the \texttt{foo} method on the system.

\begin{figure}
\centering 
    \includegraphics[width=10cm]{drawings/composite-scenario.pdf}
 \caption{Scenario for the composite pattern application}
 \label{fig:composite-scenario-sm}
\end{figure}

We started building the model following the bottom-up approach: first, we constructed the components at the lower levels of hierarchy. 
From the very beginning we realised that considering the reconfiguration, the variety of possible requests and the values of the parameters, the state-space was going to be large. Hence, we decided to start by generating the necessary files and by building the state-space for \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13}. Then, we generated the remaining processes for \texttt{Comp1} and constructed the final state-space using the obtained models for the three sub-components.  
%
%
%, we observed that the state space of \texttt{P11} was already quite large, \texttt{2.7M} states. We realised that the interleaving of the variety of requests and different values of parameters will have significant impact on the state space. 

\paragraph{Building the state-space for Comp11, Comp12, and Comp13}
Recall that when the user launches the state-space generation, he has to choose the root component of the application and optionally the state machine modelling the behaviour of the environment (i.e. the scenario).
By default, VerCors constructs the state-space starting from the processes at the lowest levels of hierarchy like if they are not affected by the scenario, and then synchronises the root component with the scenario. The reason is that in the current version we do not have tools to compute automatically the impact of the scenario on the sub-components while building their state-space. However, the scenario of our use-case example is quite generic and it is not difficult to infer from the business logic of the application which requests from the environment modelled in Figure~\ref{fig:scenario-sm} can eventually reach \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13}. In particular, we know that the method call \texttt{S1.addSubcomp(0)} is not forwarded to these three components because \texttt{0} is the identifier of the root component, hence, \texttt{Comp1} will try to add a sub-component inside its content instead of forwarding the call. Also, \texttt{S2.computeChildren()} cannot be invoked on the sub-components simply because they do not serve this method. Hence, we were able to create a state machine that models the scenario for  \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13}: it is similar to the scenario for the root component but it does not have the two transitions with the instructions \texttt{S1.addSubcomp(0)} and \texttt{S2.computeChildren()}.
Then, we automatically generated from VerCors the state-space for \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13} synchronised their scenario. 

The time to generate .fiacre, .exp files and the auxiliary scripts from VerCors is negligible. The overall time to construct \texttt{Comp11} was \texttt{30 minutes}. This includes the time to run the Flac compiler, to build the sub-components of \texttt{Comp11}, and to construct the final product synchronised with the scenario. 
Eventually the model of \texttt{Comp11} was only \texttt{994} states. The reason why constructing so few states took 30 minutes is that, again, when the sub-processes of \texttt{Comp11} are being constructed, they are not synchronised with the scenario. It means that building the queue of the component and its sub-component \texttt{P11} takes a lot of time, because all possible interleaving of the incoming requests have to be taken into consideration.

Since \texttt{Comp12} and \texttt{Comp13} have exactly the same behaviour and structur as \texttt{Comp11}, constructing their state-spaces took the same time. At this point we were wondering how much time we would be able to gain, if we could analyse automatically the modelled system in VerCors and predict that the behaviour of the three components is similar. The following experiment was done manually on the .fiacre, .exp. .svl files generated from VerCors and .bcg files constructed by Flac and CADP.
 Recall that the only difference between \texttt{Comp11}, \texttt{Comp12}, \texttt{Comp13} is the default value of \texttt{myId} stored by their component-controllers. In order to build the state-space of \texttt{Comp12} based on the .bcg files generated for \texttt{Comp11}, we did the following. We modified the default value of the \texttt{myId} attribute in the .fiacre file of the attribute controller generated for \texttt{P11}. We ran Flac compiler to create the .bcg file of the modified pLTS. The obtained process, in fact, represented the attribute controller for \texttt{P12}. The rest of the processes included in \texttt{P11} and \texttt{P12} are similar. Hence, we could synchronise the queue, the body, and the other sub-nets of \texttt{P11} with the myId attribute controller of \texttt{P12} and obtain the model of \texttt{P12}. Then, we synchronised it with the rest of the sub-nets of \texttt{Comp11} and obtained the .bcg file of \texttt{Comp12}. Overall, we had to modify one .fiacre file and two .exp files, to run Flac compiler once, and Exp Open twice. Running the tools took us 2 minutes only. Hence, doing automatically such kind of optimisation would save us 28 minutes for building the state-space of \texttt{Comp12}.
 
\paragraph{Building the final product}
We used VerCors to generate automatically all the files necessary to build the state-space of our use-case example synchronised with the scenario illustrated in Figure~\ref{fig:scenario-sm}. We modified the generated \texttt{.sh} script so that it does not lunch the constructions of \texttt{Comp11}, \texttt{Comp12} and \texttt{Comp13} because we had already built them. 

The final non-reduced state-space of \texttt{Comp1} synchronised with the scenario was \texttt{143.689.330} states, and the minimiser of CADP managed to reduce it to \texttt{31.699.470} states. 
Its construction and minimisation took us almost \texttt{11 hours}. This includes running Flac compiler for all the pLTSs of \texttt{Comp1} and for the scenario, building the state-space of \texttt{P1}, and synchronising all this with \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13}. Most of the time we were constructing the queue and synchronising the final product. Running the Flac compiler for the queue of the \texttt{Comp1} took us almost \texttt{6 hours}. The reason is that at this point the queue is not affected by the scenario and it should be able to handle all possible interleaving of the request both from outside of the composite and from its internal components. 

To sum-up, building the state-space of the whole system took us almost \texttt{13 hours} and the obtained state-space is \texttt{31.699.470} states. 

We also modelled in VerCors and generated the state-space for a similar scenario and a similar system but with only two functional sub-components  at each level of hierarchy and \texttt{myId} ranging from \texttt{0} to \texttt{2}. Then, we model-checked several properties on both examples.

\paragraph{Checking properties}
First, we checked that it is possible to add a sub-component at the two higher levels of hierarchy, i.e. that there exist paths in the behavioural graph where a call to \texttt{addSubcomp} with \texttt{parentId} equal to 0 (the id of \texttt{Comp1}) or to 1 (the id of \texttt{Comp11}) returns \texttt{true}. The reply from the model-checker after the verification of the following formula was TRUE.

%\centerline{\textsf{Absence\_Before ('Q\_IamTheLeader.*', 'Q\_IamTheLeader.*')"}}  

\centerline{\textsf{\textless true* . 'Scenario\_S1\_addSubcomp !POS (0)' 
. (not 'Scenario\_S1\_addSubcomp.*')* }}
\centerline{\textsf{
. 'R\_S1\_addSubcomp !POS(1)'\textgreater true }}
\centerline{\textsf{
and}}
\centerline{\textsf{
'\textless true* . 'Scenario\_S1\_addSubcomp !POS (1)' . (not 'Scenario\_S1\_addSubcomp.*')* }}
\centerline{\textsf{ 
. 'R\_S1\_addSubcomp !POS(1)'\textgreater true}}

Then, we checked that if a component has not been bound in the system, it cannot be accessed. More precisely, we checked that \texttt{Comp11}, \texttt{Comp12}, and \texttt{Comp13} cannot be accessed unless a sub-component is added to \texttt{Comp1}.

\centerline{\textsf{Absence\_Before ('Comp1[1$\vert$ 2$\vert$ 3].*', 'Scenario\_S1\_addSubcomp !POS (0)')}}

Next, we checked that sub-components cannot be added inside a composite more than twice. For this we used an MCL pattern \texttt{Bounded\_Existence\_Globally} which verifies that a given action predicate is satisfied in the model exactly two times. The following formula checks this property for \texttt{Comp12}:

\centerline{\textsf{Bounded\_Existence\_Globally ('Comp12\_P12\_Bind\_C1.*')}}

For the use-case with two sub-components at each level the model-checker answers TRUE. However, in the case of three sub-components it answers FALSE and provides an example of a path where three sub-components are added in the content of \texttt{Comp12}. We checked a similar formula for all the composites in both examples.

Finally, we checked the main property of the system stating that the value of \texttt{childrenNum} variable of the root component is equal to the number of sub-components added at all levels of hierarchy. In order to obtain the value of \texttt{childrenNum}, the scenario invokes the \texttt{computeChildren} method on the \texttt{S2} interface of the root component. In order to count the number of components which are active in the application, we invoke the \texttt{foo} function on the root composite. The call is propagated to the sub-components at all levels of hierarchy, and we count how many components have processed the method invocation. If the value returned by \texttt{computeChildren} is equal to \textit{x}, we expect the number of components that received the \texttt{foo} request to be \textit{x+1} because the root component should also receive it. The following MCL formula corresponds to the property:

\centerline{\textsf{[true* . {R\_S2\_ComputeChildren ?x:Nat} . (not 'R\_S1\_addSubcomp.*')* . "Scenario\_\_S1\_foo"]}} \centerline{\textsf{\textless('.*Serve\_S1\_foo.*' . (not ('R\_S1\_addSubcomp.*' or "Scenario\_S1\_foo"))*)\{x+1\}\textgreater true}}
 
The first line of the formula explores all paths in the behaviour graph where the number of children \texttt{x} has been returned by the \textsf{R\_S2\_ComputeChildren} action, a new component has not been added to the system (\textsf{(not 'R\_S1\_addSubcomp.*')*}), and after the \texttt{foo} method invocation was sent (\texttt{Scenario\_S1\_foo}). Then the formula states that all such paths are followed by a path where the \texttt{foo} request has been served \texttt{x+1} times, no component has been added in the meanwhile and the root component has not received any other \texttt{foo} method invocation. The model-checker answers TRUE. If in the second line we replace \texttt{x+1} by \texttt{x+2}, which means that we expect the \texttt{foo} method invocation to be served at least \texttt{x+2} times, the model-checker answers FALSE, because the number of components in the system is equal to \textit{x+1}.

We have also generated and executed the code of the modelled application on GCM/ProActive. 
%\TODO{what can we tell about code execution?}

\smallskip
The modelled application involves all the advances features discussed in this chapter except from the interceptors. It has a non-functional part specified for the composite components, the attribute controllers providing access to the attributes (\texttt{myId}, \texttt{isBound}, etc), multicast interfaces in the composite components which are reconfigured by the component-controllers. 

\subsection{Springoo}
In this section we present an example of an application using interceptors which were not included in the previous use-case.
We assisted our colleagues from Telecom ParisTech in modelling and generating of their application called Springoo in VerCors. The details of the use-case example were published in~\cite{aubonnet:hal-01180627}. Its component diagram is illustrated in Figure~\ref{fig:springoo-vce}.   

\begin{figure}
\centering
    \includegraphics[width=\linewidth]{drawings/springoo.pdf}
 \caption{Springoo application modelled in VerCors}
 \label{fig:springoo-vce}
\end{figure}

Springoo is a web application that conforms to the
three-tier Java Enterprise Edition (JEE) platform architecture, providing typical commercial web services
through an Apache/Jonas/MySQL architecture. The business logic of the application is performed by the \texttt{Apache-SCC} and \texttt{Jonas-SCC} components. Both components are "self-controlled"  in the sense that all functional requests going to and from these components are monitored by the interceptors \texttt{InM} and \texttt{OutM} located in their membranes. The information gathered by the monitors is then forwarded to the \texttt{Qos} component which analyses it and provides it to an external component through its server interface. The behaviour of the \texttt{Apache-SCC} and \texttt{Jonas-SCC} is additionally controlled by the membrane of the encompassing composite \texttt{Composite}. Its \texttt{Qos} component-controller  gathers and analyses the information from the  \texttt{Qos} components of \texttt{Apache-SCC} and \texttt{Jonas-SCC}, and from the interceptors in the membrane of \texttt{Composite}. Finally, a so-called MAPE sequence of components in the membrane of the root composite \texttt{Springoo} uses the information gathered by the \texttt{Qos} components in order to reconfigure system properties. More precisely, the  \texttt{InOutMonitor} intercepts the incoming requests to the application. The \texttt{Qos} component, as usual, takes the gathered metrics, processes them, and provides them to the \texttt{Analyser}. The latter additionally requests the information from the \texttt{Composite} and analyses it. The analysis result is used by the \texttt{Planner} in order to plan system reconfiguration if it is necessary. Finally, the reconfiguration is performed by the \texttt{Executor} component. The reconfiguration was not modelled in VerCors because for this, the authors used so-called GCM-script~\cite{ibanez:hal-01302467} instead of the standard GCM/ProActive API.

The behaviour of the system was not model-checked in VerCors mainly because Springoo takes the reconfiguration decisions based on the metrics related to the time, and the timed systems cannot be analysed with our platform.   
Still, we validated  its static correctness with respect to the properties formalised in Chapter~\ref{chap:formalisation} and generated the ADL file and the skeletons of the Java classes and interfaces including the component-controllers and intercepts. The authors integrated their implementation of the business logic in the generated code and ran it on GCM/ProActive.