


\section{Partition quality} 

Different partitioners provide different partitions, however it is not obvious which partition is better. %That is why, there are metrics which measure partitioning quality. 
Partition quality can be evaluated in two ways. One way is to evaluate the effect of the partition on the algorithm we want to execute on the graph, e.g., in terms of the total execution time or total communication overhead. While this analysis provides the definitive answer, it is clear that we would like to choose the best partition before running an algorithm. The second way uses metrics which can be computed directly from the partition without the need to run the target algorithm. We call the first set of metrics execution metrics and the second one partition metrics. 
%partitioning itself, another way is to evaluating effect of partitioning on execution time. In the next chapter, we will show how partitioning metrics were actually used in literature.

\subsection{Execution metrics}

A partition is better than another if it reduces the execution time of the specific algorithm we want to run on the graph. In some cases the partitioning time itself may not be negligible and then we want to take it into account while comparing the total execution times.

%Execution metrics measure final effect of partitioning to algorithm that are executed on partitioned graph. Final effect may be measured in time and in network traffic.

\textit{Partitioning time}: it is time spent to partition a graph using a particular computing resources. This metric shows how fast a partitioner works and not the final effect on the partitioned graph.

\textit{Execution time}: this metric measures the execution time of a graph processing algorithm (such as \CC, \PR, \SSS, etc.  ) on a partitioned graph using some specific computing resources.

Some other metrics may be used as proxies for these quantities. 

\textit{Network communication}: it measures in bytes of traffic or in number of logical messages, how much information has been exchanged 
during the partitioning or the execution of graph processing algorithms.


\textit{Rounds}: this metric indicates the number of rounds (iterations) performed by the partitioner. It can only be applied  to iterative partitioners. This metric is useful to compare the impact of a modification of a partitioner and to evaluate its convergence speed. This metric can provide an indication of the total partitioning time. 


\subsection{Partition metrics} \label{sec:metrics}

Partition metrics can be split into two groups. In the first group there are the metrics that quantify \textit{balance}, i.e., how homogeneous the partitions' sizes are. The underlying idea is that if one component is much larger that the others, the computational load on the corresponding machine is higher and then this machine can slow down the whole execution. The metrics in the second group quantify \textit{communication}, i.e., how much overlap there is among the different components, i.e., how many vertices appear in multiple components. This overlap is a reasonable proxy for the amount of inter-machine communication that will be required to merge the results of the local computations. The first two metrics below are \emph{balance metrics}, all the other ones are \emph{communication metrics}. 
% \TODO{do you use this information later? The metrics should be organized by type or something, it looks like a random list}
%\subsubsection{Computational balance}

\textit{Balance} (denoted as~\acrshort{bal}): it is the ratio of the maximum number of edges in a component to the average number of edges across all the components:
$$ \textrm{BAL}=\frac{\max\limits_{i=1,\dots N}|E_i|}{|E|/ N}.$$
\textit{Standard deviation of partition size} (denoted as~\acrshort{std}): it is the normalized standard deviation of the number of edges in each component:
%\TODO{what more information does it bring compared to balance? Hlib: it was used in some papers}
$$ \textrm{STD} = \sqrt{\frac{1}{N} \sum\limits_{i=1}^N \left(\frac{|E_i|}{|E|/N}-1\right)^2 }$$
\textit{Replication factor} (denoted as~\acrshort{rf}): it is the ratio of the number of vertices in all the components to the number of 
vertices in the original graph. It measures the overhead, in terms of vertices, induced by the partitioning.  %\TODO{I have added this, is it a good idea to have a plain english description?} 
%\fab{explain this is become you cut vertices}
The overhead appears due to the fact that some vertices get cut by graph partitioning algorithm.
$$ \textrm{RF} = \sum\limits_{i=1}^N |V(E_i)| \frac{1}{|V|}$$
\textit{Communication cost} (denoted as~\acrshort{cc}): it is defined as the total number of frontier vertices and measures communication overhead due to the fact that framework will synchronize states of vertices that were cut:
$$ \textrm{CC} = \sum\limits_{i=1}^N |F (E_i)| $$
\textit{Vertex-cut} (denoted as~\acrshort{vc}): this metric measures how many times vertices were cut. For example, if a vertex is cut in 5 pieces, then its contribution to this metric is 4. 
$$ \textrm{VC} = \sum\limits_{i=1}^N F(E_i) + \sum\limits_{i=1}^N \bar{F}(E_i) - |V| $$
\textit{Normalized vertex-cut}: it is a ratio of the  \textit{vertex-cut} metric of the partitioned graph to the expected \textit{vertex-cut} of a randomly partitioned graph.

\textit{Expansion}: it was originally introduced in ~\cite{expansion2, expansion1} in the context of vertex partitioning. In~\cite{sbvcut} \textit{expansion} was adapted to edge partitioning approach. It measures the largest portion of frontier vertices across all the components. 
%$ \textrm{Expansion} = \max_{i=1,\dots N} \left( \frac{| F(E_i) \cap F(\bigcup_{j=1}^{N}E_j)               |}   {\min ( |E_i| , | \bigcup_{j=1}^{N}E_j - E_i    | )} \right)   $
$$  \textrm{Expansion} = \max\limits_{i=1,\dots N} \frac{|F(E_i)|}{|V(E_i)|} $$
\textit{Modularity}: as \textit{expansion}, \textit{modularity} was proposed in ~\cite{expansion2, expansion1} and later adapted to edge partitioning approach in~\cite{sbvcut} as follows\footnote{
	Both expansion and modularity could also be defined normalized to the number of edges rather than to the number of vertices. 
}:
$$ \textrm{Modularity } = \sum\limits_{i=1}^N \left( \frac{|V(E_i)|}{|V|} - \left(  \sum\limits_{j \neq i} \frac{|F(E_i) \cap F(E_j)|}{|V|}         \right)^2   \right) $$

The \textit{modularity} measures density of the components --- how many vertices are inside component comparing to those that are between components. The higher it is, the better is partitioning.

It should be noticed that RF, CC, and VC metrics are linear combinations of each others:
\begin{align*}
\textrm{RF} &= \sum\limits_{i=1}^N |V(E_i)|\frac{1}{|V|} \\&= \sum\limits_{i=1}^N (|\bar{F}(E_i)| + |F(E_i)|) \frac{1 }{|V|} \\&= 1 - \frac{\textrm{VC}}{|V|} + \frac{\textrm{CC}}{|V|}
\end{align*}
%For this reason it is sufficient to consider only two of them. We selected VC and CC.
%~\footnote{We decided to sacrifice RF because including RF leads easily to negative coefficients in the linear regression model.}

In the next chapter we evaluate if these metrics are indeed good proxies for the final execution time of different algorithms.


