%!TEX root = ../Hlib_thesis.tex

\subsection{Distributed implementation} \label{sec:gibbs:impl}

Our new algorithm borrows from \cite{gibbs} the idea to maintain potentially different opinions at each instance, and to use the probabilities in \eqref{formula:gibbs} to accept the opinions of other instances. At the same time, instance $i$ updates its subvector $\mathbf c^i_i$ according to Metropolis-Hasting sampling described in Section~\ref{sec:algorithm}, because it is computationally simpler than Gibbs sampling. Moreover, the stationary distribution corresponding to the local Metropolis-Hasting sampling steps is~\eqref{e:gibbs_probability}. Hence a ``large'' number of Metropolist-Hasting steps correspond to draw a single Gibbs sample. We suspect that the results in \cite{gibbs} hold also when a finite number of Metropolis-Hasting sampling steps are performed, but we do not have yet a formal proof. At the same time, implementing a simulated annealing algorithm based on the asynchronous Gibbs sampler in \cite{gibbs} requires minor changes to the Spark implementation of our algorithm described in what follows, as far as Gibbs samples can be drawn efficiently.

We assume that the energy can be decomposed as sum of vertex energies that depend only on the number of edges of each color the vertex has:
$$ \mathcal{E}(\mathbf{c}) = \sum\limits_{v \in V} \varepsilon_v(\mathbf{c})= \sum\limits_{v \in V} \varepsilon_v(\{n_v(c), c \in C \}). $$
This is for example the case of both $\mathcal E_{comm}$ and $\mathcal E_{cc}$. 
This assumption allows us to simplify the computational requirement of the algorithm, but it is not necessary. 
It will be convenient to define the energy associated to a subset $A$ of the vertices:
$$ \mathcal{E}_A(\mathbf{c}) \triangleq \sum\limits_{v \in A} \varepsilon_v(\mathbf{c}). $$


%We start introducing some additional notation. 
Remember that we have $N$ instances of the partitioner algorithm, where each instance has its own subset of edges $E_i$, whose set of vertices is $V(E_i)$. Let $E_{-i}$ denote all the edges except those in $E_i$
% and $\tilde{E_i}$ denotes the set of edges just after instances of the algorithm have updated its own opinions. %Instance change Edge which colors was changed by an instance $i$ changed opinion about Improved edges is denoted as $\tilde{E_i}$. 
Each edge has two values attached to it: the index of the instance to whom this edges belongs, and an array of colors of length $N$: $ c_k^0, c_k^1, ... , c_k^N $ which represents the colors of edge $k$ according to the $N$ different instances. The different opinions the instances have about what the color of edges $k$ should be. When an instance $i$ changes the color of edge $k$, it changes $c_k^i$. 
%Each color in the discussed array represents an \textit{opinion} of some instance of the partitioner about the color of a particular edge. 
A value attached to a vertex is an array which contains $N$ different maps. Each of these maps contains an information about the colors of the edges attached to this vertex according to some instance opinion, e.g., a map says there are $n_0$ edges of color $c_0$,  $n_1$ edges of color $c_1$, etc. 

Each iteration of this distributed algorithm consists of two steps: a local step and global step. Each instance updates its opinion during the local step performing $L$ attempts to swap/change colors as in the first version. No communication between instances is required.  Then during a global step, it performs a \textit{shuffle}, which propagates vertex values to all the instances, and after it computes the $N(N-1)$ values of $\alpha_{i,j}$, which are used to accept the opinions of the other instances. 
A naive computation of the probabilities $\alpha_{i,j}$ requires computing four times the energy of the whole graph in our case. Computing the energy of the whole graph in GraphX, requires using all $N$ instances, because each instance has only a subset of the graph. Thus, each instance $i$ needs to compute the $N-1$ acceptance probabilities $\alpha_{k,i} $ for $k$ different from $i$. To compute all $\alpha $ for each instance, we will need to compute sequentially $4N(N-1)$ times energy of different graphs.

In order to speed up the computation of the probabilities $\alpha_{i,j}$, we propose the following procedure that makes possible to calculate the four energies appearing in~\eqref{formula:gibbs} as sum of two differences, where each difference can be computed locally by instance $i$, thanks to our assumption on the energy function.
%
%
%where $ \mathbf{c}$ represents vector of colors of edges. $ \mathbf{c}_i^i$ denotes an opinion of instance $i$ about component $i$. 
%
%$$ \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i, \mathbf{c}_{-i}^j) \triangleq \mathcal{E}_{V(E_i)} (\{ c_l^i, l \in E_i \} \cup \{ c_l^j, l \in E(V(E_i) / E_i      \})$$

%An instance $j$ updates its opinion about component $i$, according to what $i$ thinks about it with probability $\alpha_{i,j}$:
%\begin{align}
%\label{formula:gibbs}
% \alpha_{i,j} = \min \left(1, \exp \left( - \frac{1}{T}( \mathcal{E}(\mathbf{c}_i^i,  \mathbf{c}_{-i}^j)  - \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j) + \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}(\mathbf{c}_i^i , \mathbf{c}_{-i}^i)       ) \right) \right)
%\end{align}


First we consider the difference between the third and forth energy terms in~\eqref{formula:gibbs}:
$$ \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}(\mathbf{c}_i^i, \mathbf{c}_{-i}^i). $$
Both these energies are computed as function of the vertices of the whole graph, but these graphs are different. 
The two energies are different because $i$ and $j$ have in general different opinions about what colors assign to the edges $E_i$: $\mathbf{c}_{i}^i \neq\mathbf{c}_{i}^j$.
It is important to notice that $ \mathbf{c}_i^j $ did not change during the local step: instance $j$ may only have changed $\mathbf c_j^j$. Instance $i$ has then all the information required to compute this difference. Moreover, because of our assumption on the energy function, the difference of the two energies can be reduced to computing the difference between energies attached to $V(E_i)$ according to $j$ and $i$ opinions: 
\begin{align}
\label{formula:gibbs-diff2}
\mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}(\mathbf{c}_i^i, \mathbf{c}_{-i}^i)= \mathcal{E}_{V(E_i)}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i, \mathbf{c}_{-i}^i).
\end{align}
As we said above, this difference can be computed immediately after the local step, without any need for the nodes to communicate.


Now, we consider the difference between the first two energies appearing in the formula~\eqref{formula:gibbs}: 
$$ \mathcal{E}(\mathbf{c}_i^i, \mathbf{c}_{-i}^j) - \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j) $$
 However, $\mathbf{c}_{j}^j \subset \mathbf{c}_{-i}^j$ and the updated values of $\mathbf{c}_{j}^j $ are located on instance $j$. That it why, it is necessary to perform a shuffle first to propate the new set of opinions. We observe that, because of our assumption on the energy function:
\begin{align}
\label{formula:gibbs-diff1}
\mathcal{E}(\mathbf{c}_i^i, \mathbf{c}_{-i}^j) - \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j)= \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i, \mathbf{c}_{-i}^j)  - \mathcal{E}_{V(E_i)}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j).
\end{align}
%$ \mathcal{E}_{V(E_i)}(\underline{\mathbf{c}}_i^i,  \underline{\mathbf{c}}_{-i}^j) $ is an energy of vertices $V(E_i)$, where vertex data attached to these vertices forms from $\underline{\mathbf{c}}_i^i,  line{\mathbf{c}}_{-i}^j$ vector. 
Moreover,  we need to know only the colors of $E(V(E_i))$ (all neighbors edges of $V(E_i))$). We calculate $ \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i, \mathbf{c}_{-i}^j) $ in the following manner. First, instance $i$ collects all the triplets (edges with their vertices) that correspond to $ E_i $. Next, it takes all vertices attached to these triplets and reduce it to a set of vertices (because some vertices may be repeated). After, instance $i$ takes vertex values (mapping from color to the number of times edges of this color are connected to this vertex) according to $j$ opinion. Then, it traverses all triplets and compare color of the edges according to $i$ and $j$. In case the colors of edge $e=(u,v)$ are different for $i$ and $j$ we modify vertex values of $u$ and $v$ (by removing from both vertex data one appearance of color $c_e^j$ and adding one appearance of color $c_e^i$). 

Formulas \eqref{formula:gibbs-diff1} and~\eqref{formula:gibbs-diff2} simplify the computation of $\alpha_{i,j}$: instead of computing four times an energy of the four different graphs (which cannot be done in local way), it is necessary to compute locally difference in energy related to the local vertices between what the owner instance thinks and remote instance thinks. Finally, computing $\alpha_{i,j}$ is reduced to~\eqref{formula:gibbs-final}:
\begin{align}
\begin{split}
\label{formula:gibbs-final}
 \alpha_{i,j} = \min \bigg(1, \exp \Big( - \frac{1}{T}( \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i,  \mathbf{c}_{-i}^j) - \mathcal{E}_{V(E_i)}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j) \\ + \mathcal{E}_{V(E_i)}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i, \mathbf{c}_{-i}^i)) \Big) \bigg)
\end{split}
\end{align}
The probability $ \alpha_{i,j} $ (the probability to assign $\mathbf c_i^j:=\mathbf c_i^i$) can be computed then by instance $i$ in two phases requiring only one shuffle. Once $\alpha_{i,j}$ has been computed for all $j$ different from $i$, instance $i$ can generate the corresponding Bernoulli random variable and decide if $\mathbf c_i^j$ should be updated to $\mathbf c_i^i$. Note that the values  $\mathbf c_i^j$ are associated the links $E_i$ and are then stored at instance $i$. Hence, no communication is required for this update.
Finally, instance $i$ computes for each vertex in $V(E_i)$ how many edges in $E_i$ of a given color it has according to the different $N$ opinion. A second shuffle is needed to aggregate this information and update the array of maps associated to each vertex.

%During a local step, each instance of the partitioner improves coloring of the local subset of edges $E_i$ using simulated annealing algorithm as discussed in the previous chapter. Each instance changes only its own opinion about own edges. After instance made $L$ attempts to swap/change colors of the edges, it computes locally~\ref{formula:gibbs-diff1}.  
%
%After a local step has finished, a global step starts. First it performs global shuffle where all the values attached to vertices are updated. Second, in parallel, each instance computes~\eqref{formula:gibbs-diff2} and then $\alpha_{i,j}$ is computed using~\eqref{formula:gibbs-diff1}. %-final}. 
%For each of $N$ instances we know $N-1$ probabilities (alphas). Each $\alpha_{i,j}$ determines the probability for an instance $i$ to change its own opinion about $E_j$ according to its owner $j$. The next part is to apply current probabilities, i.e., to accept or not other opinions. The last part, again, propagates updated edges values in order to update all values attached to the vertices. Overall the global step requires sending $N$ opinions about each edge.  


%One may think that we no need update $ \tilde{\mathcal{E}(V(E_i^j))} $ this opinions, because $j$, during its local step, cannot change the opinion about edges in $i$. The last statement is true, however, $j$ can change some colors of edges in $j$ which share vertices with some edge in $i$, hence we need to update all values attached to vertices. 

Algorithm~\ref{alg:gibbs} shows the pseudo-code of the distributed implementation of our algorithm.



\begin{algorithm}
\caption{Asynchronous \SA for GraphX}\label{alg:gibbs}
\begin{algorithmic}[1]
\Procedure{SimulatedAnnealing}{}
\State $ G \gets randomlyAssignColors(G) $
\State $ G \gets partitionRandomly(G) $
\State $ round \gets 0 $
\While{ $T > 0 $ }  
		\State $ G \gets propagateValues(G) $  
		\For { $ component \gets components  $} \Comment{Executes locally on each component}
			\For { $ i < L $} 
				\If{$ tossCoin(probabillityToSwap) == "head" $} \Comment{swap}			
					\State $ e \gets randomEdge(component) $
					\State $ e^\prime \gets randomEdge(component) $
					\State  $ \Delta \mathcal{E} \gets computeDelta(e, e^\prime) $ 
					\If{$ \Delta \mathcal{E} < 0 $}
						\State $ swapColors(e, e^\prime) $
					\Else 
						\State $ swapColorsWithProb(e, e^\prime, e^{-\frac{\Delta\mathcal{E}}{T}})$
					\EndIf	
				 \Else  \Comment{change}
					\State $ e \gets randomEdge(component) $
				 	\State $ c^\prime \gets anotherColor(c) $ 
				 	\State $ \Delta \mathcal{E} \gets computeDelta(e, c^\prime) $ 
			 		\If{$ \Delta \mathcal{E} < 0 $}
						\State $ changeColor(e, c^\prime) $
					\Else 
						\State $ changeColorWithProb(e, c^\prime, e^{-\frac{\Delta\mathcal{E}}{T}})$
					\EndIf
				 \EndIf
			\State $ i++ $
			\EndFor 
			\State $ D2 = computeDifferenceD2(component) $ \Comment{See~\ref{formula:gibbs-diff2}}
		\EndFor
		\State $ G \gets propagateValues(G) $  
		\For {$ component \gets components $} \Comment{Executes locally on each component}
			\State $ D1 = computeDifferenceD1(component) $ \Comment{See~\ref{formula:gibbs-diff1}}
			\State $ \alpha_{i,j} = computeAlpha(D1, D2) $
			\State $ propagateWithPtob(i, j, \alpha_{i,j}) $ 
		\EndFor 		
		
		\State $ round++ $
		\State $T \gets T_{init} - round * \Delta T$  
\EndWhile
\State $ G = partitionBasedOnColor(G) $
\EndProcedure
\end{algorithmic}
\end{algorithm}

