\section{A general \SA~framework for edge partitioning} \label{sec:algorithm}

Let us consider a general optimization problem
\begin{equation}
\begin{aligned}
\label{problem}
& \underset{\mathbf c \in  C^{|E|}}{\textrm{minimize} }
& & \mathcal E(\mathbf{c}) \\
& \textrm{subject to } 
& & \mathbf{c} \in D,\\
\end{aligned}
\end{equation}
where $D$ is a generic set of constraints. We can solve this problem with \SA~as follows.
\begin{itemize}
\item given the current solution $\mathbf{c}$, select a possible alternative $\mathbf{c}' \in D$ with probability $q_{\mathbf{c},\mathbf{c}'}$
\item if $\mathcal E(\mathbf{c}')\le \mathcal E(\mathbf{c})$ accept the change, otherwise accept it with probability $\exp\left(\frac{\mathcal E(\mathbf{c})-\mathcal E(\mathbf{c}')}{T}\right)<1$. The probability to accept the change can be also expressed as $\beta_{\mathbf c, \mathbf c'} = \min \left(1, \exp\left(\frac{\mathcal E(\mathbf{c})-\mathcal E(\mathbf{c}')}{T}\right)\right)$
\end{itemize}
If the selection probabilities $q_{\mathbf c,\mathbf c'}$ are symmetric ($q_{\mathbf c,\mathbf c'}=q_{\mathbf c',\mathbf c}$) and if the temperature decreases as $T_0/\log(1+k)$, where $k\ge0$ is the iteration number and the initial temperature $T_0$ is large enough, then this algorithm is guaranteed to converge to the optimal solution of the above problem~\cite[Chapter~7]{bremaud2013markov}. \J does not satisfy any of these conditions, and then it is not guaranteed to converge to the optimal solution.


At a given step the transition probability from state $\mathbf c$ to state $\mathbf c'$ is $p_{\mathbf{c},\mathbf{c}'} = q_{\mathbf{c},\mathbf{c}'} \beta_{\mathbf c, \mathbf c'}$. The choices for $q_{\mathbf{c},\mathbf{c}'} \beta_{\mathbf c, \mathbf c'}$ and $\beta_{\mathbf c, \mathbf c'}$ correspond to what is called a Metropolis-Hasting sampler. In Section~\ref{sec:asy}, we will discuss a different possibility: the Gibbs sampler.

This algorithm is very general and can be applied to any energy function $\mathcal E$ including the metrics described in Section~\ref{sec:notation}.
A practical limit is that the algorithm may not be easy to distribute for a generic function $\mathcal E(\mathbf c)$, because of its dependency on the whole vector $\mathbf c$. Nevertheless, we can observe that 
a \SA~algorithm needs to evaluate only the energy differences $\mathcal E(\mathbf c')-\mathcal E(\mathbf c)$. Then, as far as such differences depend only on a few elements of the vectors $\mathbf c$ and $\mathbf c'$, the algorithm has still a possibility to be implemented in a distributed way. 
%For example, this is the case of \J, that at each step considers two states that differ only for the colors of two edges $(u,v)$ and $(u', v')$, and whose energy difference depends only on the number of edges of those colors that $u$, $v$, $u'$ and $v'$ have.

If the function $\mathcal E$ can be expressed as a sum of  potentials of the cliques of order not larger than $r$,\footnote{
	Cliques of order $1$ are nodes, cliques of order $2$ are edges, etc..
} evaluating the energy difference requires only to evaluate the value of the potentials for the corresponding cliques (see ~\cite[Chapter~7]{bremaud2013markov}). The energy function $\mathcal E_{comm}$ considered by \J falls in this category and in fact at each step the energy difference between two states 
%that differ only for the colors of the edges $(u,v)$ and $(u', v')$ 
requires to count only the edges of those colors that $u$, $v$, $u'$ and $v'$ have \eqref{e:jabejavc}. As we said, our framework is more general and can accommodate any function that can be expressed as sum of clique potentials. For example in what follows we consider the following function 
\begin{align}
\mathcal{E} &= \mathcal{E}_{comm} + \alpha \mathcal{E}_{bal} \label{eq:e}\\
\mathcal{E}_{bal} &= \frac{1}{|E|^2 (1 - \frac{1}{N})^2} \sum\limits_{\mathbf{c} \in C} \left( |E(\mathbf{c})| - \frac{|E|}{N} 
\right)^2 \label{eq:bal},
\end{align}
% & \mathcal{E}_{bal} \in [0; \alpha]
that allows to trade off the communication requirements associated to a partition, captured by $\mathcal E_{comm}$, and the computational balance, captured by $\mathcal E_{bal}$.\footnote{
	In Chapter~\ref{chap:lrm} we have shown that linear combinations of similar metrics can be good predictors for the final computation time.
} The term $\mathcal E_{bal}$ indeed ranges from $0$ for a perfectly balanced partition to $1$ for a partition where all the edges have been assigned the same color.
The parameter $\alpha>0$ allows the user to tune the relative importance of the two terms. 

The function $\mathcal E$ can be optimized according to the general  framework we described above as follows. We select an edge uniformly at random (say it is $(u,v)$ with color $c$) from $E$ and decide probabilistically if we want to swap its color with another edge ($(u',v')$ with color $c'$) or change the color $c$ to another color $c''$ without affecting other edges. Both the edge $(u',v')$ and the color $c''$ are selected uniformly at random from the corresponding sets.
For a color swapping operation the difference of energy is equal to:
\begin{multline}
 \Delta \mathcal{E}^{sw} = \frac{1}{|E|(1 - \frac{1}{N})}   \Big( g(u, v, c) - g(u, v, c^\prime) + g(u^\prime, v^\prime, c^\prime) \\
 - g(u^\prime, v^\prime, c) - \frac{1}{d_u} - \frac{1}{d_v} - \frac{1}{d_{u^\prime}} - \frac{1}{d_{u^\prime}} \Big),
\end{multline}
similarly to the condition~\eqref{e:jabejavc} for \J.
For a simple color change operation, the change of energy is:
%\begin{align}
% \Delta \mathcal{E} = \frac{1}{|E|(1 - \frac{1}{N})} \left( A(u, c, c^\prime) + A(v, c, c^\prime)  \right) 
%  \end{align}
%  \begin{align*}
% + \alpha \frac{2}{|E|^2(1 - \frac{1}{N})^2} \left( |E(c^\prime)| - |E(c)| + 1 \right)   
% \end{align*}
%
%$ \mathcal{E} \in [0, 1 + \alpha] $
%
\begin{multline}
\Delta \mathcal{E}^{ch} = \frac{1}{|E|(1 - \frac{1}{N})} \left( g(u, v, c)- g(u, v, c'') - \frac{1}{d_u} - \frac{1}{d_v} \right)  \\
+ \alpha \frac{2}{|E|^2(1 - \frac{1}{N})^2} \Big(n_u(c'') + n_v(c'')- n_u(c)- n_v(c) + 1 \Big).
\end{multline}
Another metrics that can be expressed as sum of clique potentials is the communication cost. Using the notation, in this chapter the communication cost (CC) metric can be defined as:
$$ CC = \sum\limits_{v \in V} \sum\limits_{c \in C} \mathds{1}(0 < n_v(c) < d_v)$$
In what follows we will consider the equivalent metric:
$$  \mathcal{E}_{cc} = \frac{ \sum\limits_{v \in V} \sum\limits_{c \in C} \mathds{1}(0 < n_v(c) < d_v)} {|V|N}, $$
that has the advantage of being between 0 and 1. 
Let us consider the color change from $c$ to $c'$ for edge $(u,v)$. The number of colors change as follows:
\begin{align*}
 \hat n_u(c) &= n_u(c) - 1 &  \hat n_v(c) &= n_v(c) - 1\\
 \hat n_u(c') &= n_u(c') + 1 &  \hat n_v(c') &= n_v(c') + 1
\end{align*}
The change of the energy of the graph is then equal to:
$$  \Delta \mathcal{E}_{cc}^{ch}(u, v) = \widehat{\varepsilon}_{cc}^{ch} - \varepsilon_{cc}^{ch},  $$
where $ \varepsilon_{cc}^{ch} $ and  $ \widehat{\varepsilon}_{cc}^{ch} $ denote the contribution from the vertices $u$ and $v$ to the total energy of the graph, before and after we have changed the color of the edge respectively. $\widehat{\varepsilon}_{cc}^{ch} $ and $ \varepsilon_{cc}^{ch} $ are defined as follows:
\begin{align*}
  \begin{split}
\widehat{\varepsilon}_{cc}^{ch} &= \frac{1}{|V|N} ( \mathds{1}(0 < \hat n_u(c) < d_u) + \mathds{1}(0 < \hat n_v(c) < d_v)  \\ 
&  + \mathds{1}(0 < \hat n_u(c') < d_u) + \mathds{1}(0 < \hat n_v(c') < d_v)  ) \\
&= \frac{1}{|V|N}(\mathds{1}(0 < n_u(c) - 1 < d_u) + \mathds{1}(0 < n_v(c) -1  < d_v)\\ 
&+ \mathds{1}(0 < n_u(c') + 1 < d_u) + \mathds{1}(0 < n_v(c') + 1 < d_v)) \\ \\ 
\varepsilon_{cc}^{ch} &= \frac{1}{|V|N} (\mathds{1}(0 < n_u(c) < d_u) + \mathds{1}(0 < n_v(c) < d_v)\\ 
&+ \mathds{1}(0 < n_u(c') < d_u) + \mathds{1}(0 < n_v(c') < d_v))\\
  \end{split}
\end{align*}
Then $ \Delta \mathcal{E}_{cc}^{ch} $ takes values $ \frac{-2}{|V|N}, \frac{-1}{|V|N}, 0, \frac{1}{|V|N}, \frac{2}{|V|N}$.
In case of color swapping operation, we swaps the colors $c$ and $c'$ of edges $(u,v)$ and $(u',v')$ respectively. Then the energy change is equal to:
$$  \Delta \mathcal{E}_{cc}^{sw}(u,v,u',v') = \widehat{\varepsilon}_{cc}^{sw} - \varepsilon_{cc}^{sw}  $$
where $ \varepsilon_{cc}^{sw} $ and  $ \widehat{\varepsilon}_{cc}^{sw} $ denote contribution from the vertices $u$, $v$, $u'$, and  $v'$ to the total energy of the graph, before and after we have swapped the color of the edge respectively.
\begin{align*}
\widehat{\varepsilon}_{cc}^{sw} &= \frac{1}{|V|N} (\mathds{1}(0 < \hat n_u(c) < d_u) + \mathds{1}(0 < \hat n_v(c) < d_v) + \mathds{1}(0 < \hat n_u(c') < d_u)\\ 
&+ \mathds{1}(0 < \hat n_v(c') < d_v) + \mathds{1}(0 < \hat n_{u'}(c) < d_{u'}) + \mathds{1}(0 < \hat n_{v'}(c) < d_{v'})\\ 
&+ \mathds{1}(0 < \hat n_{u'}(c') < d_{u'}) + \mathds{1}(0 < \hat n_{v'}(c') < d_{v'}))\\
&= \frac{1}{|V|N} (\mathds{1}(0 < n_u(c) - 1 < d_u) + \mathds{1}(0 < n_v(c) - 1 < d_v) + \mathds{1}(0 < n_u(c') + 1 < d_u)\\ 
&+ \mathds{1}(0 < n_v(c') + 1 < d_v) + \mathds{1}(0 < n_{u'}(c) + 1 < d_{u'}) + \mathds{1}(0 < n_{v'}(c) + 1 < d_{v'})\\ 
&+ \mathds{1}(0 < n_{u'}(c') - 1 < d_{u'}) + \mathds{1}(0 < n_{v'}(c') - 1 < d_{v'})) 
\end{align*}


\begin{align*}
\varepsilon_{cc}^{sw}  &= \frac{1}{|V|N} (\mathds{1}(0 < n_u(c) < d_u) + \mathds{1}(0 < n_v(c) < d_v) + \mathds{1}(0 < n_u(c') < d_u)\\ 
&+ \mathds{1}(0 < n_v(c') < d_v) + \mathds{1}(0 < n_{u'}(c) < d_{u'}) + \mathds{1}(0 < n_{v'}(c) < d_{v'})\\ 
&+ \mathds{1}(0 < n_{u'}(c') < d_{u'}) + \mathds{1}(0 < n_{v'}(c') < d_{v'}))
\end{align*}

To calculate energy change (due to the change of color of the edge or to the swap of colors of edges) of functions $\mathcal{E}$ and $\mathcal{E}_{cc}$ , only information about the nodes involved and their neighborhood is required as it was the case for \J. At the same time, the same difficulty noted in Section~\ref{sec:notation} holds. In an edge-centric distributed framework, in general the edges for a node are processed by different instances. First, we have implemented a naive version of our \SA algorithm of \J which requires each instance to propagate color updates (e.g., the new values of $n_u(c)$, $n_v(c)$, etc.) to other instances at each iteration, however it  leads to an unacceptable partitioning time. We have thus developed two versions of  \SA framework which overcome this issue.

\section{The \SA framework} \label{sec:first}

In order to reduce partitioning time, we implemented the following distributed version of the algorithm. First, edges are 
randomly distributed among the instances, and  each of them executes the general \SA algorithm described above on the 
local 
set of edges for $L$ swaps. No communication can take place among instances during this phase. After this phase is 
finished, communication 
is allowed, the correct values are computed and all the edges are again distributed at random among the instances. This 
reshuffle guarantees that any pair of edges has a probability to be considered for color swapping. 

The larger $L$, the larger risk that workers operate with stale information. We want then to fix $L$, so that it is very unlikely that two workers change the color of two different edges of the same vertex. To calculate the upper-bound for $L$ we considered the following situation. When we change the color of an edge in component $j$ ($P_j$), in average there are $2\bar{d}$ (where $\bar d$ is the average degree in the graph) edges attached to this edge. Hence, the probability that all these $2\bar{d}$ edges will not be situated in $P_i$ is the following:
\begin{equation*}
\left( 1 - \frac{1}{N} \right)^{2\bar d}
\end{equation*}
Then, the probability that at least one edge from these $2\bar{d}$ edges will be in $P_i$ is:
\begin{equation*}
1 - \left( 1 - \frac{1}{N}\right)^{2\bar d}
\end{equation*}
The maximum number of vertices in $P_i$ affected by performing swaps in all other $N-1$ components is the following:
\begin{equation}
\label{eq:maxnumvert}
4\bar d(N - 1)\left( 1 - \left( 1 - \frac{1}{N}\right)^{2\bar d} \right)
\end{equation}
In case we perform $L$ swaps during a \textit{local step}, then in $P_i$ we have two sets of vertices: $S_{direct}$, $S_{indirect}$. $S_{direct}$ consists of vertices that are directly affected by the $L$ swaps performed in current component, $|S_{direct}| <= 4L $. $S_{indirect}$ is the set of vertices that are affected by the swaps in other components; its size is at most the value in~(\ref{eq:maxnumvert}).

Hence, we need to be sure that these two sets ($S_{direct}$, $S_{indirect}$) are significantly smaller than $\frac{|V|}{N}$:
\begin{equation}
max\left(4L, 4L\bar d(N - 1)\left( 1 - \left(1 - \frac{1}{N}\right)^{2\bar d}\right) \right) < 4L\left( 1 + 4\bar{d}Ne^{-2\bar d}\right) << \frac{|V|}{N}
\end{equation}
\begin{equation}
L << \frac{|V|}{4N\left(  1 + 4\bar{d}Ne^{-2\bar d}        \right)}
\end{equation}


%We observe that during a local phase, an executor may  compute erroneously the energy differences, if other executors are changing the color of local edges involving the same nodes. While the algorithm is intrinsically robust to such ``errors,'' they can still prevent the convergence to the global optimum if they happen too often. It is then important to limit the number $L$ of iterations during the local phase so that such errors do not happen to often.

%In~\cite{tech-report} we show that the number of nodes potentially affected by this problem during a local phase is bounded by $L 4(1 + 4 \bar d Ne^{-2 \bar d})$, where $\bar d$ is the average degree in the graph. Imposing that this number is negligible in comparison to the total number of vertices, we obtain the configuration rule:
%\begin{align}
% L << \frac{|V|}{4N(1 + 4 \bar d Ne^{-2 \bar d})}.
%\end{align} 


Given the same number of potential changes considered, this implementation requires $L$ times less synchronization phases among the instances. Due to the large time required for the synchronization phase in comparison to the time required for the \textit{local} step, one can expect the total partitioning time to be reduced by roughly the same factor. Our experiments show that this is the case. In the setting described in the following section, with $L=200$, there is no difference between the final components produced by the two implementations, but the partitioning time for the naive one is $100$ times larger.

The detailed steps are described in Algorithm~\ref{alg:SA}.


%
%This distributed implementation will work almost the same as 
%
%At the end of this phase, the cor
%
%considering only the edges it received and with. A number $L$ of potential 
%
%
%Our implementation of \SA algorithm (Algorithm~\ref{alg:SA}) works in the following manner. First it assigns random colors to edges and then 
%propagates the color of 
%each edge to the attached vertices. This allows us to compute, for each vertex, the number of edges of a given color. 
%Then each partition executes a \textit{local step} which only works on edges inside this partition. With some 
%probability, it tries to either swap colors of two edges or just change the color of an edge to another one. 
%%find pair of edges  $ e(u,v), e^\prime(u^\prime, v^\prime) $, and considers changing of energy function. If the 
%%difference of energies: 
%
%
%%$ \Delta \mathcal{E} = \frac{1}{|E|(1 - \frac{1}{N})} \left( \frac{n_u(c) - n_u(c^\prime) - 1}{d_u} + \frac{n_v(c) - n_v(c^\prime) - 1}{d_v} + \frac{n_{u^\prime}(c^\prime) - n_{u^\prime}(c) - 1}{{d_u}^\prime} + \frac{n_{v^\prime}(c^\prime) - n_{v^\prime}(c) - 1}{{d_v}^\prime}  \right) $
%
%If $ \Delta \mathcal{E} $ is negative than the swap is performed. If not, then the swap is performed with probability $ 
%e^{-\frac{\Delta\mathcal{E}}{T}} $.
%
%%\begin{align*}
%%  
%%\end{align*}
%
%After this local step, the algorithm propagates values of the edges and vertices and executes a random repartitioning, 
%in order to have a positive probability of swaping colors of two random edges.
%
%However, it might be inefficient to propagate values and repartition after each local step, when we changed/swapped 
%only one edge/pair of edges. That is why, we keep at each iteration the number of swaps/changes limited to let each executor work independently as much as possible without interacting with each other. For this purpose, algorithm calculates a constant $ L $ for a given graph (the calculations are in the companion technical report~\cite{tech-report}):
%
%\begin{align}
% L << \frac{|V|}{2(1 + 2<d>Ne^{-2<d>})} ,
%\end{align} 
%where $<d>$ is a degree of the graph.
%
%
%, this information is not necessary available at the executor, because it may not be responsible for all the edges of node $u$.
%
%As we discussed above in Sec.~\ref{sec:notation}
%
%%The function in \eqref{eq:e} is the one with consider in our later experiments with $\alpha = 0.5$
%
%
%%GIOVANNI: ARRIVED HERE. THERE IS STILL A LOT OF WORK TO DO TO MAKE IT PRESENTABLE. HERE IT IS WHAT I SUGGEST AS ORGANIZATION.
%%\begin{itemize}
%%\item PRESENT OUR ENERGY FUNCTION AS AN EXAMPLE OF AN ENERGY FUNCTION MORE GENERAL THAN THOSE OF \J AND THAT ALSO HAS THE ADVANTAGE TO TRADE OFF BALANCE AND COMMUNICATION THAT WE HAVE SHOWN ADVANTAGEOUS IN THE OTHER PAPER
%%\item BRIEFLY PRESENT WHY THIS ALGO IS DISTRIBUTED AS MUCH AS \J IS
%%\item SAY THAT WE ARE GOING TO CONSIDER THIS FUNCTION IN WHAT FOLLOWS
%%\item DISCUSS THE CHANGE TO THE BASIC ALGORITHM TO SPEED-UP CONVERGENCE IN SPARK, WE KEEP AT EACH ITERATION THE NUMBER OF SWAPS LIMITED TO LET EACH EXECUTOR WORK INDIPENDENTLY AS MUCH AS POSSIBLE WITHOUT INTERACTING WITH THE OTHER. JUSTIFY THE EXPRESSION FOR L. SAY SPEED-UP ACHIEVED FOR JABEJAVC
%%\end{itemize}
%
%%\TODO{In general, if we consider that the information about graph cliques of order not larger than $r$ is local, then  }
%
%%\TODO{More, in general, it is possible to prove   $\mathcal E(\mathbf c')-\mathcal E(\mathbf c)$}
%
%%\TODO{Whatever it is the function $\mathcal E$, }
%
%
%
%%We proposed a new energy function~(\ref{eq:e}), which is the example of an energy function that is more general than those of \J, and, on the contrary to \J, it does not consider balance property as more important than communication property. We think that improving 
%%communication at the cost of balance will lead to better performance. That is why, our energy function has the advantage to trade of balance and communication property.
%
%
%\begin{comment}
%$
% \mathcal{E} = \frac{1}{2|E|(1 - \frac{1}{N})} \left( 2|E| - \sum\limits_{v}^V \sum\limits_{c}^{C}\left(\frac{n_v(c)^2}{d_v}\right)  \right) + \alpha \frac{1}{|E|^2 (1 - \frac{1}{N})^2} \sum\limits_{c}^C \left( |E(c)| - \frac{|E|}{N} \right)^2 
%$
%\end{comment}
%
%
%%$ \mathcal{E}_{comm}$ is basically the normalized function of \J~and represents communication. It takes values in $ [0;  1] $. 
%
%The $ \alpha $ parameter  sets the importance of balance vs communication properties. We set $ 
%\alpha $ to $ 0.5 $ which approximately corresponds 
%to experiments we had in~\cite{ownpaper}.
%
%The \SA algorithm also can be implemented in a distributed way. Because, like for \J, at each step it considers two states that differ only for the colors of two edges $(u,v)$ and $(u', v')$ (whose energy difference depends only on the number of edges of those colors that $u$, $v$, $u'$ and $v'$ have) and balance state which we take the same for beginning of each \textit{local step}.
%
%%Because of the particularities of Spark (especially RDDs), it is not possible to simply port \J~on GraphX. \TODO{explain why}
%%Hence we had to adapt it. We started by coloring edges with random colors and then we partitioned the graph randomly, 
%%irrespectively of the assigned colors. Then we used GraphX functions (\texttt{mapReduceTriplets} and 
%%\texttt{outerJoinVertices}) to propagate the color to the respective vertices. \TODO{And then? What about the iterative 
%%	part}
%%We have implemented for GraphX~\cite{graphx} simulated annealing algorithm which minimizes energy function: \TODO{Not 
%%clear, is this \J or another algorithm?}
%
%%First part of $ \mathcal{E}  $ correspond to communication property 
%%\end{equation}
%
%
%
%%Due to space constraints, the calculations are in the companion technical report \COPY{CITE TECHNICAL REPORT}. $ L $ limits the number of swaps/changes in a local step to be relatively sure that local changes performed by different partitions are not intersect.
%
%
%% $ \textrm{subject to}  |E(c)| = constant $
% 
%
%
%
%



\begin{algorithm}
\caption{Implementation of \SA for GraphX}\label{alg:SA}
\begin{algorithmic}[1]
\Procedure{SimulatedAnnealing}{}
\State $ G \gets randomlyAssignColors(G) $
\State $ round \gets 0 $
\While{ $T > 0 $ }  
		\State $ G \gets partitionRandomly(G) $
		\State $ G \gets propagateValues(G) $  
		\For { $ component \gets components  $} \Comment{Executes locally on each component}
			\For { $ i < L $} 
				\If{$ tossCoin(2/3) == "head" $} \Comment{swapping with probability 2/3...}			
				\State $ e \gets randomEdge(component) $
				\State $ e^\prime \gets randomEdge(component) $
				\State  $ \Delta \mathcal{E} \gets computeDelta(e, e^\prime) $ 
				\If{$ \Delta \mathcal{E} < 0 $}
					\State $ swapColors(e, e^\prime) $
				\Else 
					\State $ swapColorsWithProb(e, e^\prime, e^{-\frac{\Delta\mathcal{E}}{T}})$
				\EndIf	
			 \Else  \Comment{changing...}			
				\State $ e \gets randomEdge(component) $
			 	\State $ c^\prime \gets anotherColor(c) $ 
			 	\State $ \Delta \mathcal{E} \gets computeDelta(e, c^\prime) $ 
			 	\If{$ \Delta \mathcal{E} < 0 $}
					\State $ changeColor(e, c^\prime) $
				\Else 
					\State $ changeColorWithProb(e, c^\prime, e^{-\frac{\Delta\mathcal{E}}{T}})$
				\EndIf
			 \EndIf
		\State $ i++ $
		\EndFor 
		\EndFor 
		\State $ round++ $
		\State $T \gets T_{init} - round * \Delta T$  
\EndWhile
\State $ G = partitionBasedOnColor(G) $
\EndProcedure
\end{algorithmic}
\end{algorithm}