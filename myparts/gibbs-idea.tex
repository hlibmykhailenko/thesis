%!TEX root = ../Hlib_thesis.tex

%\subsection{An asynchronous Gibbs sampler} \label{sec:gibbs:idea}

A Gibbs sampler is another possible set of rules to determine the next state, i.e., the next color assignment in our case. Under Gibbs sampler, a subvector $\mathbf c_s$ of $\mathbf c$\footnote{$\mathbf c$ is the solution for~\eqref{problem}} is selected with probability $q_{\mathbf c_s}$. Let $\mathbf c_{-s}$ denote the other elements of $\mathbf c$, so that $\mathbf c= (\mathbf c_s, \mathbf c_{-s})$. A new random subvector $\mathbf c'_s$ is randomly generated so that the probability to move to $\mathbf c'= (\mathbf c'_s, \mathbf c_{-s})$ is: 
\begin{equation}
\label{e:prob_gibbs}
\frac{e^{- \frac{\mathcal E(\mathbf c'_s, \mathbf c_{-s})}{T}}}{\sum_{\mathbf d_s \in C^{|\mathbf c_s|}} e^{-\frac{\mathcal E(\mathbf d_s, \mathbf c_{-s})}{T}}}.
\end{equation}

Gibbs samplers can be considered as a special case of Metropolis-Hasting samplers where the new candidate states are selected with probabilities  $q_{\mathbf c, \mathbf c'}$ as in \eqref{e:prob_gibbs} and the acceptance probabilities $\beta_{\mathbf c, \mathbf c'}$ are all equal to one.  Practically speaking one talks about a Metropolis-Hasting  sampler, if the probabilities $q_{\mathbf c, \mathbf c'}$ are fixed (independently from the energy function) and the acceptance probabilities $\beta_{\mathbf c, \mathbf c'}$ can be smaller than 1. The Gibbs sampler has the advantage that no change is refused (but it may be $\mathbf c'_s= \mathbf c$) but it requires to be able to sample according to \eqref{e:prob_gibbs}, that corresponds, in general, to knowing the full conditional distribution. This may limit the size of the state vector that can be modified at a given time. Often, a single vector element (the color of a single edge in our case) is modified by a Gibbs sampler.

Also for the Gibbs sampler, if the temperature decreases logarithmically to $0$, the state will converge almost surely to a global minimizer of the function $\mathcal E$. Sometimes the expression annealed Gibbs sampler is used for this specific SA algorithm.

In a distributed setting, one would split the vector $\mathbf c$ among the $N$ workers: $\mathbf c= (\mathbf c_1, \mathbf c_2, \dots \mathbf c_N)$. While every instance, say it $i$, could independently modify its subvector $\mathbf c_i$ (i.e., the colors of its component), it would need to know the current value of all the other variables $\mathbf c_{-i}$. The same difficulties we exposed above hold then. Recently, the authors of \cite{gibbs} have shown that it is possible to circumvent this problem. Their asynchronous Gibbs sampler allow workers to proceed in parallel without synchronization or locking. We briefly describe the algorithm using our notation.

The algorithm requires each instance to maintain an \textit{opinion} about the color assignment for all the edges. We use the superscript $i$ to denote the opinion of instance $i$, that we can write as  $(\mathbf c_1^i, \mathbf c_2^i, \dots \mathbf c_N^i)$. Each iteration of the proposed algorithm consists of two steps: local and global. During a local step each instance of the algorithm improves its own component of the graph. Instance $i$ selects a subset $\mathbf c_{i,s}^i$ of its subvector $\mathbf c_{i}^i$ and generate $ \tilde {\mathbf  c}_{i,s}^{i} $ with probability
\begin{equation}
	\label{e:gibbs_probability}	
\frac{e^{- \frac{\mathcal E(\tilde {\mathbf  c}_{i,s}^i, \mathbf c_{-i}^i)}{T}}}{\sum_{\mathbf d_{i,s} \in C^{|\mathbf c_{i,s}^i|}} e^{-\frac{\mathcal E(\mathbf d_{i,s}, \mathbf c^i_{-s})}{T}}},
\end{equation}
that depends only on the current opinion of instance $i$. 
Then, during the global step, each instance, decides whether to update its opinion according to what other instances think about their components or not.  
In particular an instance $j$ updates its opinion about component $i$, according to what $i$ thinks about it with probability $\alpha_{i,j}$:
\begin{align}
\label{formula:gibbs}
 \alpha_{i,j} = \min \bigg(1, \exp \Big( - \frac{1}{T}( \mathcal{E}(\mathbf{c}_i^i,  \mathbf{c}_{-i}^j)  - \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j) + \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}(\mathbf{c}_i^i , \mathbf{c}_{-i}^i)       ) \Big) \bigg)
\end{align}

In \cite{gibbs} it is proven that the probability distribution of each opinion $\mathbf c^i$ converges to the same distribution the centralized Gibbs sampler would converge. It follows then that if we gradually decrease the temperature according to the usual law, each opinion will converge almost surely to a global minimizer of the energy function.

The next section describes how we have modified our original simulated algorithm according to the results in \cite{gibbs} and how we have implemented it in GraphX.


%We need to mention that, each instance has its own \textit{opinion} about the whole graph. An \textit{opinion} about the graph, in our case, is color assignment to all the edges of the graph. During a global step, each instance, decides whether to update its opinion according to what other instances think about their components or not. 
%It was proven that every opinion will be spawn according to the correct distribution.
%
%The generalized energy function is defined as follows:
%$$ \mathcal{E}(\mathbf{c}) \triangleq \mathcal{E}_V(\mathbf{c}) \triangleq \sum\limits_{v \in V} \varepsilon_v(\mathbf{c}), $$
%where $ \mathbf{c}$ represents vector of colors of edges. $ \mathbf{c}_i^i$ denotes an opinion of instance $i$ about component $i$. 
%
%$$ \mathcal{E}_{V(E_i)}(\mathbf{c}_i^i, \mathbf{c}_{-i}^j) \triangleq \mathcal{E}_{V(E_i)} (\{ c_l^i, l \in E_i \} \cup \{ c_l^j, l \in E(V(E_i) / E_i      \})$$
%
%An instance $j$ updates its opinion about component $i$, according to what $i$ thinks about it with probability $\alpha_{i,j}$:
%\begin{align}
%\label{formula:gibbs}
% \alpha_{i,j} = \min \left(1, \exp \left( - \frac{1}{T}( \mathcal{E}(\mathbf{c}_i^i,  \mathbf{c}_{-i}^j)  - \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^j) + \mathcal{E}(\mathbf{c}_i^j, \mathbf{c}_{-i}^i) - \mathcal{E}(\mathbf{c}_i^i , \mathbf{c}_{-i}^i)       ) \right) \right)
%\end{align}
