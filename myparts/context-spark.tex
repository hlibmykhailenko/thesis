\section{Pregel model}

We will briefly overview the Pregel model~\cite{pregel} which is illustrated in Figure~\ref{fig:pregel2}. Pregel introduces a computational model where a graph processing algorithm is executed in an iterative way. Each iteration is called a \textit{superstep}. Vertices can be either active or inactive. At \textit{superstep} 0 all vertices are active. Each active vertex computes some function during a superstep and then becomes inactive. Each vertex has the possibility to send a message through outgoing edges to neighbor vertices and this message will be available only in the next superstep. All vertices that received some messages become active until they explicitly decides to turn to inactive. After all messages are received, a new \textit{superstep} can start: all vertices that received messages will perform some local computations. All messages sent to a vertex are aggregated into a single message using some commutative function. 

The Pregel API consists of three basic functions, which developer should implement: \textit{sendMessage}, \textit{mergeMessage} and \textit{applyMessage}. \textit{sendMessage} takes an edge and produces messages to none, one, or both of the adjacent vertices. All messages dedicated to one vertex are merged in a single message by \textit{mergeMessage}. Finally, \textit{applyMessage} uses a merged message and a vertex value to produce a new vertex value which will replace the previous one.



\begin{figure}
\centering
\includegraphics[width=5in]{figures/pregel2.png}
\caption{Pregel model~\cite{pregel}}
\label{fig:pregel2}
\end{figure}


\section{Apache Spark}

Apache Spark~\cite{spark} is a large-scale distributed framework
for general purpose data processing. It relies on the concept of Resilient Distributed Dataset~\cite{rdd} (see Section~\ref{subsec:rdd}) and Bulk Synchronous Parallel (see Section~\ref{subsec:bsp}) model. Spark basic functionality (Spark Core) covers: scheduling, distributing, and monitoring jobs, memory management, cache management, fault recovery, and communication with data source.

Spark itself is implemented on a JVM~\cite{jvm} program, using the Scala~\cite{scala} programming language. 

Spark can use three cluster managers: \textit{Standalone} (a native part of Spark), \textit{Apache Mesos},~\cite{mesos} and \textit{Hadoop YARN}~\cite{yarn}. All these cluster managers consist of one \textit{master} program and several \textit{worker} programs. Several Spark applications can be executed by one cluster manager simultaneously. A Spark application launched, using one of the scripts provided by Spark, on the \textit{driver} machine, is called a \textit{driver} program (see Figure~\ref{fig:cluster}). The \textit{driver} program connects to a \textit{master} program, which allocates resources on \textit{worker} machines and instantiates one \textit{executor} program on each \textit{worker} machine. Each \textit{executor} may operate several components using several cores. \textit{Driver}, \textit{master}, and \textit{worker} programs are implemented as Akka actors~\cite{akka}.

Fault tolerance in Spark is implemented in the following manner. In case a task failed during execution but the executor is still work, then this task is rescheduled on the same executor. Spark limits (by the property \textit{spark.task.maxFailures}) how many times a task can be rescheduled  before finally giving up on the whole job. In case the cluster manager loses a worker (basically the machine stops responding) then the cluster manager reschedules tasks previously assigned to the working machine. It is easy to reschedule a failed task thanks to the fact that all RDDs (see next section) are computed as ancestor of other RDDs, according to the principle  of RDD lineage~\cite{lineage}. 

\begin{figure}
\centering
\includegraphics[width=5in]{figures/master.png}
\caption{Spark standalone cluster architecture~\cite{scmo}}
\label{fig:cluster}
\end{figure}

\subsection{Resilient Distributed Dataset} \label{subsec:rdd}

A Resilient Distributed Dataset (~\acrshort{rdd}) is an immutable, distributed, lazy-evaluated collection with predefined set of operations. An RDD can be created from either raw data or another RDD (called parent RDD). An RDD is partitioned collection, and its components are distributed among the machines of the computational cluster. Remember that both an RDD and a partitioned graph are distributed data structures, which are partitioned into components. Note that the number of components $N$ can be different from the number $N_m$ of machines, i.e multiple components can be assigned to one machine . The set of operations which can be applied to an RDD came from the functional programming paradigm. The advantage is that the developer should only correctly utilize RDD operations but should not take care of synchronization and information distribution over the cluster, which come out-of-the-box. This advantage comes at the price of a more restricted set of operations available. 
%Obviously, RDD operations impose some restrictions comparing to the systems which do not have predefined set of operations. 

One of the main ideas behind RDD is to reduce slow\footnote{Approximately, sequential access to a hard-disk is about 6 times slower and random access 100,000 slower, than access to RAM, according to~\cite{ramslow}.} interaction with hard-disk by keeping as much as possible in RAM memory.
Usually, the whole sequence of RDDs is stored in RAM memory without saving it to the file system. Nevertheless, it can be impossible to avoid using hard-disk, despite of the fact that Spark uses memory serialization (to reduce memory occupation) and lazy-evaluation.

There are two kinds of operations that can be executed on RDDs: transformations and actions. All transformations are lazy evaluated operations which create a new RDD from an existing RDD. There are two types of transformations: \textit{narrow} and \textit{wide} (see Figure~\ref{fig:narrow-wide}). 

\textit{Narrow} transformations preserve partitioning between parent and child RDD and do not require inter-node communication/synchronization. \textit{Filter},  \textit{map}, \textit{flatMap} are the examples of \textit{narrow} transformations (all of them are related to functions in functional programming). Let us consider the \textit{filter} transformation: it is a higher-order function, which requires a function that takes an item of an RDD and returns a boolean value (basically it is a predicate). After we apply a \textit{filter} transformation to an RDD $r1$, we get a new RDD $r2$, which contains only those items from $r1$, on which the predicate given to \textit{filter} returned the $true$ value. 

A \textit{wide} transformation requires inter-node communication (so-called \textit{shuffles}). Examples of wide transformations are \textit{groupByKey}, \textit{reduceByKey}, \textit{sortByKey}, \textit{join} and so on. Let us consider the  \textit{groupByKey} transformation. It requires that items of the RDD (on which it will be applied) are represented as key-value pairs. For example, we have the following component: 
$$ component = [ (k1, v1), (k2, v2), (k2, v3), (k1, v4), (k1, v5), (k3, v6), (k3, v7), \dots ] $$
Initially, each component contains some part of the RDD, \textit{groupByKey} first groups key-value pairs with similar ids in each component independently (this preparation require inter-node communication):
\begin{align*}
k1 &\Rightarrow [v1, v4, v5]\\
k2 &\Rightarrow [v2, v3]\\
k3 &\Rightarrow [v6, v7] \\
&\dots
\end{align*}
Then each component sends all its groups to the dedicated components according to the key of the group. Each key is related to one component by using \textit{HashPartitioner}.\footnote{It partitions based on key by using hash funciton.} In this way, a \textit{groupByKey} transformation can compute to which component each key belongs. After it has computed all $N - 1$ (remember that $N$ is number of components) messages to others components (all groups dedicated to one component form single message), it can start to send them as following:
\begin{align*}
messageForComponent0 &\Rightarrow [k3 \Rightarrow [v6, v7], k2 \Rightarrow [v2, v3]]\\
messageForComponent1 &\Rightarrow [k1 \Rightarrow [v1, v4, v5]] \\
&\dots
\end{align*}
After a component has received all messages dedicated to it, it can finally merge all the groups of key-values.% (see Figure~\ref{fig:underthehood}).
\begin{figure}
\centering
\includegraphics[width=5in]{figures/narrowwide.png}
\caption{Narrow and wide transformations~\cite{narrow-wide}}
\label{fig:narrow-wide}
\end{figure}

An action returns some value from an existing RDD, e.g., \textit{first}, \textit{collect}, \textit{reduce} and so on. Only when an action is called on an RDD, it is actually evaluated. An invoked action submits a job to the DAGScheduler, the class responsible for scheduling in Spark. A submitted job represents a logical computation plan made from a sequence of RDD transformations with an action at the end. The DAGScheduler creates a physical computation plan which is represented as a directed acyclic graph (\acrshort{dag}) of stages (see Figure~\ref{fig:underthehood}). There are two types of stages: ResultStage and ShuffleMapStage. The DAG is ended with a ResultStage. Each ShuffleMapStage stage consists of sequence of narrow transformations followed by a wide transformation. Each stage is split in $N$ tasks.

In-memory caching in Spark follows Least Recently Used (\acrshort{lru}) policy~\cite{lru}. Spark does not support automatic caching. Instead, the Spark user has to explicitly specify which RDD he/she would like to cache, and then the framework may cache it. If there is not enough space to cache, Spark applies LRU policy and removes some other RDDs from the cache. The user also can directly request to uncache an RDD.


To recap, Spark is based on several trending ideas: 
\begin{itemize}
\item immutable collections (RDD), which simplify coding process;
\item limited number of operations (transformations and actions) --- users are restricted to use pure functions~\cite{pure};
\item use of~\acrshort{ram} instead of disk, which accelerate computations. 
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=5in]{figures/spark-workflow.png}
\caption{Under The Hood: DAG Scheduler~\cite{underthehood}, where from \textbf{A}, \textbf{B}, \textbf{C}, \textbf{D}, \textbf{E}, \textbf{F} RDDs created \textbf{G} RDD.}
\label{fig:underthehood}
\end{figure}

\begin{comment}
\centering
\includegraphics[width=5in]{figures/dag.jpg}
\caption{Scheduling in Spark~\cite{schedulingspark}}
\label{fig:schedulingspark}
\end{comment}

\begin{comment}
\begin{figure}
\centering
\includegraphics[width=5in]{figures/dag2.jpg}
\caption{Scheduling in Spark TODO ~\cite{schedulingspark2}}
\label{fig:schedulingspark2}
\end{figure}
\end{comment}


\subsection{Bulk Synchronous Parallel model} \label{subsec:bsp}

The Bulk Synchronous Parallel model(~\acrshort{bsp})~\cite{bsp} is an abstract model, which is built on iterations. Each iteration consists of two parts: a local computation and a global information exchange. Each executor computes its own component of distributed data structure independently and after sends messages to other executors. A new iteration does not start until all components have received all messages dedicated to them (see Figure~\ref{fig:bspmodel}). This way, BSP implements  a barrier synchronization mechanism~\cite[Chapter~17]{herlihy2011art}.

The total time of an iteration consist of three parts: the time to perform local computation by the slowest machine, the time to exchange messages, and, finally, the time to process barrier synchronization.

In order to illustrate the application of BSP model in Spark, we consider the following sequence of transformations applied to RDD: \textit{filter}, \textit{map}, \textit{reduceByKey}. The first two transformations are \textit{narrow} one, thus they will be executed during a local computation step on each executor without synchronization (basically sequences of \textit{narrow} transformations are squeezed in one transformation). Next the \textit{reduceByKey} will be executed during a global information exchange. 

\begin{figure}
\centering
\includegraphics[width=3.5in]{figures/bsp.png}
\caption{Bulk synchronous parallel model~\cite{bspmodel}}
\label{fig:bspmodel}
\end{figure}



\subsection{GraphX}

In this section we focus on the GraphX framework and the reasons why we selected it for our computations. In Section~\ref{sec:others} we present the other graph processing frameworks which we considered while choosing the framework for our experiments.

%To work with graph partitioners we need a graph processing system to execute partitioners and graph processing algorithms. We compare existing graph processing systems in order to find the most suitable for our needs. 

%We selected GraphX. 
GraphX~\cite{graphx} is an Apache Spark's API for graph data processing. %Pros: built on Spark, pure functions, pre-built algorithms.  Based on BSP model, implements Pregel API, why did we choose GraphX
GraphX programs are executed as Spark jobs. %Because of the distributed nature of the execution, one has to partition the graph as a collection of edges. 
The framework relies on a vertex-cut partitioning approach and implements Pregel API as well. %Recent study ~\cite{bourse2014balanced} provided analytical support for the empirical evidence 
%\TODO {This new approach is motivated by the observation that standard tools, e.g., [13, 24] for constructing a balanced vertex partition perform poorly on power-law graphs [1, 17, 18].  }  
%that balance \textbf{edge-cut} partitioning works poorly on power-law graphs~\cite{abou2006multilevel} \cite{lang2004finding} \cite{leskovec2009community}.
%that \textbf{vertex-cut} approach works better on a power-law graphs than \textbf{edge-cut} approach. 
%There exist many different algorithms for choosing where to map a given edge. We discuss in Section~\ref{sec:GraphXpartitioning} six such algorithms implemented in GraphX. 
% and the metrics, which were proposed \cite{VC-SOA}~\cite{JA-BE-JA-VC} to evaluate the quality of graph partitioning.
%Moreover, it is possible to implement algorithm using the Pregel API in GraphX. 


%In this work we rely on the first approach because modifying an input graph can be done automatically while changing an algorithm requires more effort and can lead to introducing errors in the algorithm.

%\subsection{GraphX execution model}

A graph is stored in GraphX as two RDDs: a vertex RDD and an edge RDD. A vertex RDD stores the ids of the vertices and their values. An edge RDD stores source and destination ids, and the values assigned to the edges. Each of these RDDs is split in $N$ components, each assigned to a different executor.\footnote{
	It is also possible to assign multiple partitions to the same executor, but we do not consider this possibility.
}  A vertex RDD is always partitioned by a hash function based on the vertex ids, while the edge RDD is partitioned using a user-specified partitioner, i.e., usually graphs have much less vertices than edges, thus effect of partitioning of the vertex RDD is negligible. GraphX distributes the $N$ components among the machines in a round robin fashion.
%Usually graph algorithms need to work with triplets, which include an edge and the values of the two vertices connected by this edge. Hence, to have triplets GraphX needs to join vertex RDD and edge RDD. To do this join, GraphX sends messages from each vertex RDD partition to each edge RDD partition. 

%In our experiments we used GraphX because it does edge partitioning and is based on Spark. We selected Spark because it is still quite new but stable enough, it is currently under intensive development. 
%And somehow, it looks like successor of Hadoop. 

By default, GraphX considers all input graphs as directed ones, but an algorithm can work on its input as if it was an undirected graph. This is, for example, the case of the \CC algorithm built-in in GraphX. Other algorithms, like the GraphX implementation of \PR~\cite{page1999pagerank}, instead assume the input is directed. In this case, if one wants to process an undirected graph, he/she can pre-process the input and replace each undirected edge by two edges with opposite directions. An alternative approach is to modify the algorithm which is executed on a graph. For instance, the \texttt{PageRank} algorithm can be easily modified so that it sends messages in both directions of an edge. 

We have selected GraphX as a system on which we perform our experiments due to multiple reasons:
\begin{itemize}
\item GraphX is free to use;
\item it supports edge partitioning;
\item it is still under development and maintenance;
\item it is built on top of Spark. Most of the effective solutions are based on Hadoop or Spark. As we discuss in the following section, we believe that Spark is more promising than Hadoop.
\end{itemize}


\begin{comment}
\centering
\includegraphics[width=5in]{figures/rdds.png}
\caption{Property Graph~\cite{propertygraph}}
\label{fig:propertygraph}
\end{comment}
